<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mobi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<o-17vyY<fgd@`rDXVLxG1Qa8!~p5$M(m-W{g6zb@f3o-GkU8/,Rqg+&#<$o<fkl' );
define( 'SECURE_AUTH_KEY',  'a*%<UOkZ{Yn?o<d:n[ZZF%d#>`snwfQ<4+ ,&RG%RBEwI8F/:2R|SD>JFdU/+PJv' );
define( 'LOGGED_IN_KEY',    'yV9h`V CL :] sY=NR6X,*byw@Zm^AA9@)[.aZF]}A:hi8Ccj%(v#RJ%n8;{,MZ=' );
define( 'NONCE_KEY',        'tMu,/il=E`ga6%pV~x<B[M`5R]v&v1[IITSJ<Sr85euS.Nu>:$jcmN@k)I%cC>@Z' );
define( 'AUTH_SALT',        'r?>`-k4~pU~LYCbVZ9^@[}4F/{4l@n`4gBXMxqMD!y:nIst4COIehFfRJx~Fhk.N' );
define( 'SECURE_AUTH_SALT', 'T+y=~4ALd*[G`Aq*qKt0Gb{>dG?ps<l2&km4y{JTn`gzp,1u4 !R~svS|Ds-=RF,' );
define( 'LOGGED_IN_SALT',   'eN^h_h!D*2KJ3V.|JKLT,(Jc1?OIc(HSk#P$Ge]Qi`,qbt&=neLsii)O) 6gV62E' );
define( 'NONCE_SALT',       ' &Y<}+/~wP>2`+$!9J}X6h:d8)sJfDT6:<(@j)C!.Oe)DA)x4PC)k`KVZRs[}ZR$' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', false );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

