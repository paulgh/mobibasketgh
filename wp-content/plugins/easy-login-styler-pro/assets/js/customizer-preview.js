( function( $ ){

	wp.customize.bind( 'preview-ready', function() {
		wp.customize.preview.bind( 'easyloginstyler-url-switcher', function( data ) {
			if ( true === data.expanded ) {
				wp.customize.preview.send( 'url', easy_login_styler.permalink );
			}
		} );

		wp.customize.preview.bind( 'easyloginstyler-back-to-home', function( data ) {
			wp.customize.preview.send( 'url', data.home_url );
		} );

		wp.customize.preview.bind( 'easy_login_styler_templates', function( data ) {
			if ( true === data.expanded ) {
				$( 'body' ).addClass( 'easyloginstyler-template-section-opened' );
			} else {
				$( 'body' ).removeClass( 'easyloginstyler-template-section-opened' );
			}
		} );
	});

    wp.customize( 'easy_login_styler_customizer[template]', function( value ) {
        value.bind( function( to ){
            console.log( to );
            $('body').removeClass( 'easy-login-template-default easy-login-template-1 easy-login-template-2 easy-login-template-3 easy-login-template-4 easy-login-template-5 easy-login-template-6 easy-login-template-7 easy-login-template-8 easy-login-template-9 easy-login-template-10 easy-login-template-11 easy-login-template-12' );
            $('body').addClass( 'easy-login-' + to );
        });
    });
	
	wp.customize( 'easy_login_styler_customizer[layout]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-default-left easy-login-default-middle easy-login-default-right' );
            $('body').addClass( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[loginwidth]', function( value ) {
        value.bind( function( to ){
            $('body #login').css( { 'width': to + 'px' } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[hideback]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-hideback-true easy-login-hideback-false' );
            $('body').addClass( 'easy-login-hideback-' + to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logoshow]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-logo-true easy-login-logo-false' );
            $('body').addClass(  'easy-login-logo-' + to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logo]', function( value ) {
        value.bind( function( to ){
            $('.login h1 a').css( { 'background-image': 'url('+ to +')' } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[url]', function( value ) {
        value.bind( function( to ){
            $('.login h1 a').attr( 'href' , to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logostyle]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-logo-circular easy-login-logo-square easy-login-logo-auto' );
            $('body').addClass( 'easy-login-logo-' + to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logoalign]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-logo-left easy-login-logo-center easy-login-logo-right' );
            $('body').addClass( 'easy-login-logo-'+ to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logowidth]', function( value ) {
        value.bind( function( to ){
            $('.login h1 a').css( { 'width' : to + 'px', 'background-size': to + 'px ' +  wp.customize.value( 'easy_login_styler_customizer[logoheight]' )() + 'px'  } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[logoheight]', function( value ) {
        value.bind( function( to ){
            $('.login h1 a').css( { 'height' : to + 'px', 'background-size': wp.customize.value( 'easy_login_styler_customizer[logowidth]' )() + 'px ' + to + 'px'  } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtalign]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-text-left easy-login-text-center easy-login-text-right' );
            $('body').addClass( 'easy-login-text-'+ to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[title]', function( value ) {
        value.bind( function( to ){
            $('.easy-login-styler-title').text( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[desc]', function( value ) {
        value.bind( function( to ){
        	$('.easy-login-styler-desc').text( to );
        	if( to.length > 0 ){
        		$('.easy-login-styler-desc').show();
        	}else{
        		$('.easy-login-styler-desc').hide();
        	}
        });
    });

    wp.customize( 'easy_login_styler_customizer[username]', function( value ) {
        value.bind( function( to ){
            $('label[for="user_login"] span').text( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[password]', function( value ) {
        value.bind( function( to ){
            $('label[for="user_pass"] span').text( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[remember]', function( value ) {
        value.bind( function( to ){
            $('label[for="rememberme"] span').text( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[regbtn]', function( value ) {
        value.bind( function( to ){
            if( $('p#nav a').length > 1 ){
                $('p#nav a:eq(0)').text( to );
            }
        });
    });

    wp.customize( 'easy_login_styler_customizer[buttonsize]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-btn-sm easy-login-btn-m easy-login-btn-l' );
            $('body').addClass( 'easy-login-btn-'+ to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[loginbtn]', function( value ) {
        value.bind( function( to ){
            $('#wp-submit').val( to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[lostpass]', function( value ) {
        value.bind( function( to ){
            if( $('p#nav a').length > 1 ){
                $('p#nav a:eq(1)').text( to );
            }else{
                $('p#nav a:eq(0)').text( to );
            }
        });
    });

    wp.customize( 'easy_login_styler_customizer[loginbg]', function( value ) {
        value.bind( function( to ){
            $( '#login, .login form' ).css( { 'background-color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtcolor]', function( value ) {
        value.bind( function( to ){
            $( '#login, #login h2, #login p:not(.message), .login label, #login a' ).css( { 'color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtfld]', function( value ) {
        value.bind( function( to ){
            $('body').removeClass( 'easy-login-txtfld-boxed easy-login-txtfld-linear' );
            $('body').addClass( 'easy-login-txtfld-' + to );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtfldcolor]', function( value ) {
        value.bind( function( to ){
            $( '#login form .input' ).css( { 'color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtfldbr]', function( value ) {
        value.bind( function( to ){
            $( '#login form .input, #rememberme' ).css( { 'border-color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[txtfldbg]', function( value ) {
        value.bind( function( to ){
            $( '#login form .input, #rememberme' ).css( { 'background-color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[buttonbg]', function( value ) {
        value.bind( function( to ){
            $( '.submit .button' ).css( { 'background-color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[buttonbr]', function( value ) {
        value.bind( function( to ){
            $( '.submit .button' ).css( { 'border-color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[buttoncr]', function( value ) {
        value.bind( function( to ){
            $( '.submit .button' ).css( { 'color' : to } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[buttonrd]', function( value ) {
        value.bind( function( to ){
            $( '.submit .button' ).css( { 'border-radius' : to + 'px' } );
        });
    });

    wp.customize( 'easy_login_styler_customizer[css]', function( value ) {
        value.bind( function( to ){
        	$( '#login .easy_login_styler_customizer-css' ).remove();
            $( '#login' ).append('<div class="easy_login_styler_customizer-css"><style>'+ to +'</style></div>');
        });
    });

    wp.customize( 'easy_login_styler_customizer[font]', function( value ) {
        value.bind( function( to ){
            $( 'head link[rel="stylesheet"]' ).last().after("<link rel='stylesheet' href='//fonts.googleapis.com/css?family="+ to +":100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' type='text/css' media='screen'>");
       		$( 'body' ).css({ 'font-family': to  });
        });
    });

    wp.customize( 'easy_login_styler_customizer[images]', function( value ) {
        value.bind( function( to ){
            if( to.length > 0 ){
                var images = to.split( ',' );
                var slider = '<div id="focus-preview"></div>';
                $('.easy-login-styler-bg').html( slider );
                if( images.length > 0 ){
                    var filtered = images.filter(function (el) {
                      return el != '';
                    });

                    if( filtered.length > 1 ){
                        $.each( filtered , function( index, value ) {
                            if( value != "" ){
                                $( '#focus-preview' ).append( '<li><img src="'+ value +'"></li>' );
                            }
                        });
                        $( '#focus-preview' ).focuswp({
                            'timeout'       : 3500,
                            'nav'           : false,
                            'pager'         : false,
                            'autoplay'      : true,
                            'pauseControls' : false,
                            'pause'         : false
                        });
                    }else{
                        $( '.easy-login-styler-bg' ).css( 'background-image', 'url('+ filtered[0] +')' );
                    }
                }
            }else{
               $('.easy-login-styler-bg').html(''); 
               $('.easy-login-styler-bg').css( 'background-image', '' );
            }
        });
    });

    // wp.customize( 'easy_login_styler_customizer[license]', function( value ) {
    //     value.bind( function( to ){
    //         var postData = {
    //             'action': easy_login_styler.ajax_action,
    //             'license-data':   to,
    //         };

    //         jQuery.post( easy_login_styler.ajaxurl, postData )
    //             .always(function( a, status, b ) {
    //                 if( status == "success" ){
    //                     a = jQuery.parseJSON( a  );
    //                     if( 'undefined' !== a.response ){
    //                         if( 'undefined' !== a.response.license && a.response.license == 'valid' ){
    //                             console.log( a.response.license );
    //                             $( '#_customize-input-easy_login_styler_customizer[license]' ).hide();
    //                         }
    //                     }
    //                 }
    //             });
    //     });
    // } );

} )( jQuery );