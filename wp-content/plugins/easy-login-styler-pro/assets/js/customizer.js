( function( $, api ){

	/*
	 * Detect when the Login Customizer panel is expanded (or closed) so we can preview the login form easily.
	 * Source: Login Designer
	*/
	wp.customize.panel( 'easy_login_styler', function( section ) {
		section.expanded.bind( function( isExpanding ) {
			// Value of isExpanding will = true if you're entering the section, false if you're leaving it.
			if ( isExpanding ) {

				// Only send the previewer to the login designer page, if we're not already on it.
				var current_url = wp.customize.previewer.previewUrl();
					current_url = current_url.includes( easy_login_styler.permalink );

				if ( ! current_url ) {
					wp.customize.previewer.send( 'easyloginstyler-url-switcher', { expanded: isExpanding } );
				}

			} else {
				// Head back to the home page, if we leave the Login Designer panel.
				wp.customize.previewer.send( 'easyloginstyler-back-to-home', { home_url: wp.customize.settings.url.home } );
				url = wp.customize.settings.url.home;
			}
		});
	});

	// Detect when the templates section is expanded (or closed) so we can hide the templates shortcut when it's open.
	wp.customize.section( 'easy_login_styler_templates', function( section ) {
		section.expanded.bind( function( isExpanding ) {

			// Value of isExpanding will = true if you're entering the section, false if you're leaving it.
			if ( isExpanding ) {
				wp.customize.previewer.send( 'easy_login_styler_templates', { expanded: isExpanding } );
			} else {
				wp.customize.previewer.send( 'easy_login_styler_templates', { expanded: false } );
			}
		} );
	} );

	wp.customize.control( 'easy_login_styler_customizer[activate]', function( control ) {
		control.container.find( '.button' ).on( 'click', function() {
			var license = wp.customize.value( 'easy_login_styler_customizer[license]' )();
			control.container.prepend( '<span class="spinner" style="visibility: visible;"></span>' );
			if( license ){
				var postData = {
	                'action'		: easy_login_styler.ajax_action,
	                'nonce'			: easy_login_styler.nonce,
	                'method'		: 'activate',
	                'license-data'	: license,
	            };

	            jQuery.post( easy_login_styler.ajaxurl, postData )
	                .always(function( a, status, b ) {
	                    if( status == "success" ){
	                        a = jQuery.parseJSON( a  );
	                        if( 'undefined' !== a.response ){
	                            if( 'undefined' !== a.response.license && a.response.license == 'valid' ){
	                                control.container.prepend( '<div class="notice notice-success" style="padding: 10px 15px;">'+ easy_login_styler.activated +'</div>' );
	                                control.container.find( '.button, .notice-error' ).hide();
	                                control.container.parent().parent().find( '#customize-control-easy_login_styler_customizer-deactivate .button' ).removeClass( 'hidden' );
	                                control.container.parent().parent().find( '.easy_login_styler_activation-notice' ).hide();
	                                control.container.find( '.spinner' ).remove();
	                            }else{
	                            	control.container.prepend( '<div class="notice notice-error" style="padding: 10px 15px;">'+ easy_login_styler.invalid +'</div>' );
	                            	control.container.find( '.spinner' ).remove();
	                            }
	                        }
	                    }
	                });
			}
	    } );
	});

	wp.customize.control( 'easy_login_styler_customizer[deactivate]', function( control ) {
		control.container.find( '.button' ).on( 'click', function() {
			var license = wp.customize.value( 'easy_login_styler_customizer[license]' )();
			control.container.prepend( '<span class="spinner" style="visibility: visible;"></span>' );
			if( license ){
				var postData = {
	                'action'		: easy_login_styler.ajax_action,
	                'nonce'			: easy_login_styler.nonce,
	                'method'		: 'deactivate',
	                'license-data'	: license,
	            };

	            jQuery.post( easy_login_styler.ajaxurl, postData )
	                .always(function( a, status, b ) {
	                    if( status == "success" ){
	                        a = jQuery.parseJSON( a  );
	                        console.log( a );
	                        if( 'undefined' !== a.response ){
	                            if( 'undefined' !== a.response && a.response == 'deactivated' ){
	                                control.container.prepend( '<div class="notice notice-info" style="padding: 10px 15px;">'+ easy_login_styler.deactivated +'</div>' );
	                                control.container.find( '.button' ).hide();

	                                wp.customize( 'easy_login_styler_customizer[license]', function( setting ) {
			        					setting.set('');
									});

	                                control.container.parent().parent().find( '#customize-control-easy_login_styler_customizer-activate .notice' ).hide();
	                                control.container.parent().parent().find( '#customize-control-easy_login_styler_customizer-activate .button' ).show();
	                                control.container.parent().parent().find( '.easy_login_styler_activation-notice' ).show();
	                                control.container.find( '.spinner' ).remove();
	                            }
	                        }
	                    }
	                });
			}
	    } );
	});

	var easy_login_styler_defaults ={
		// 'txtalign' : 'center'
	};

	wp.customize( 'easy_login_styler_customizer[template]', function( value ) {
        value.bind( function( to ){
            var newTemplate = easyloginstyler_templates( to );
            if( newTemplate.length > 0 ){
            	$.each( newTemplate, function( k,v ){
            		$.each( v, function( i, j ){
        				wp.customize( 'easy_login_styler_customizer['+ i +']', function( setting ) {
        					setting.set( j );
						});
        			} );
            	} );   	
            }
        });
    });

    function easyloginstyler_templates( template ){
    	var templates = {};
    	
    	templates['template-default'] = [{
			'logoshow' 		: true,
			'layout'		: 'easy-login-default-left',
			'desc'			: '',
			'txtalign'		: 'center',
			'loginwidth'	: 420,
			'font'			: '',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#484848',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#32373c',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#DDDDDD',
			'buttonsize'	: 'l',
			'buttonbg'		: '',
			'buttonbr'		: '',
			'buttoncr'		: '',
			'buttonrd'		: 3,
			'hideback'		: false,
			'logowidth'		: 84,
			'logoheight'	: 84,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

    	templates['template-1'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'left',
			'loginwidth'	: 500,
			'font'			: 'Merriweather',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#000000',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#DDDDDD',
			'buttonsize'	: 'm',
			'buttonbg'		: '#000000',
			'buttonbr'		: '#000000',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-2'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 550,
			'font'			: 'Krub',
			'loginbg'		: '#212c38',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'linear',
			'txtfldcolor'	: '#FFFFFF',
			'txtfldbg'		: 'transparent',
			'txtfldbr'		: '#FFFFFF',
			'buttonsize'	: 'l',
			'buttonbg'		: '#e83787',
			'buttonbr'		: '#e83787',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: 25,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-3'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'left',
			'loginwidth'	: 400,
			'font'			: 'Fira Sans',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#000000',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#fcfcfc',
			'txtfldbr'		: '#e1e1e1',
			'buttonsize'	: 'l',
			'buttonbg'		: '#dc3d3a',
			'buttonbr'		: '#c92510',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];


		templates['template-4'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-right',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'left',
			'loginwidth'	: 400,
			'font'			: 'Frank Ruhl Libre',
			'loginbg'		: '#9cb8a0',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#fcfcfc',
			'txtfldbr'		: '#fefefe',
			'buttonsize'	: 'l',
			'buttonbg'		: '#df5e68',
			'buttonbr'		: '#bb4d54',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-5'] = [{
			'logoshow' 		: true,
			'layout'		: 'easy-login-default-right',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 400,
			'font'			: 'Open Sans',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#587482',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#587482',
			'txtfldbg'		: '#f3f6f7',
			'txtfldbr'		: '#f0f2f3',
			'buttonsize'	: 'l',
			'buttonbg'		: '#607D8B',
			'buttonbr'		: '#587482',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'hideback'		: true,
			'logowidth'		: 70,
			'logoheight'	: 70,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-6'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-middle',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 400,
			'font'			: 'Open Sans',
			'loginbg'		: '#29333f',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#29333f',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#FFFFFF',
			'buttonsize'	: 'l',
			'buttonbg'		: '#40989a',
			'buttonbr'		: '#40989a',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: 25,
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-7'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-right',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 800,
			'font'			: 'Merriweather',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#000000',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#dddddd',
			'buttonsize'	: 'm',
			'buttonbg'		: '#5e48a0',
			'buttonbr'		: '#5e48a0',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: 2,
			'hideback'		: false,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-8'] = [{
			'logoshow' 		: true,
			'layout'		: 'easy-login-default-middle',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 400,
			'font'			: 'Lora',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#2d2d2d',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#29333f',
			'txtfldbg'		: '#f3f6f7',
			'txtfldbr'		: '#f0f2f3',
			'buttonsize'	: 'm',
			'buttonbg'		: '#fdb714',
			'buttonbr'		: '#fdb714',
			'buttoncr'		: '#000000',
			'buttonrd'		: 2,
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-9'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 530,
			'font'			: 'Asap',
			'loginbg'		: '#5b6473',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#222222',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#FFFFFF',
			'buttonsize'	: 'm',
			'buttonbg'		: '#f89b72',
			'buttonbr'		: '#f89b72',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: 25,
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-10'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-right',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'left',
			'loginwidth'	: 380,
			'font'			: 'Archivo Narrow',
			'loginbg'		: '#20203a',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#20203a',
			'txtfldbg'		: '#FFFFFF',
			'txtfldbr'		: '#FFFFFF',
			'buttonsize'	: 'sm',
			'buttonbg'		: '#ffa660',
			'buttonbr'		: '#ffa660',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-11'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 400,
			'font'			: 'Fira Sans',
			'loginbg'		: '#FFFFFF',
			'txtcolor' 		: '#000000',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#f3f6f7',
			'txtfldbr'		: '#f0f2f3',
			'buttonsize'	: 'l',
			'buttonbg'		: '#007ed7',
			'buttonbr'		: '#0968a7',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: '2',
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

		templates['template-12'] = [{
			'logoshow' 		: false,
			'layout'		: 'easy-login-default-left',
			'desc'			: 'Welcome back! Please login to your account.',
			'txtalign'		: 'center',
			'loginwidth'	: 380,
			'font'			: 'Encode Sans Condensed',
			'loginbg'		: '#0c9cb2',
			'txtcolor' 		: '#FFFFFF',
			'txtfld'		: 'boxed',
			'txtfldcolor'	: '#000000',
			'txtfldbg'		: '#f3f6f7',
			'txtfldbr'		: '#f0f2f3',
			'buttonsize'	: 'm',
			'buttonbg'		: '#000000',
			'buttonbr'		: '#000000',
			'buttoncr'		: '#FFFFFF',
			'buttonrd'		: 25,
			'hideback'		: true,
			'noticecolor'	: '#111111',
			'errorcolor'	: '#111111',
		}];

    	return templates[template];
    }
} )( jQuery, wp.customize );