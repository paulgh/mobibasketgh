( function( $ ) {

        $(window).load(function(){
            var begin_attachment_string = $( '#images-input' ).val();
            var begin_attachment_array = begin_attachment_string.split( ',' );

            for(var i = 0; i < begin_attachment_array.length; i++){
                if(begin_attachment_array[i] != ""){
                    $(".images").append( "<li class='image-list'><a class='button-secondary easyloginstyler-remove-img'>Remove</a><img src='"+begin_attachment_array[i]+"'></li>" );
                }
            }
            $(".button-secondary.upload").click(function(){
                var custom_uploader = wp.media.frames.file_frame = wp.media({
                    multiple: true
                });

                custom_uploader.on('select', function() {
                    var selection = custom_uploader.state().get('selection');
                    var attachments = [];
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        $(".images").append( "<li class='image-list'><a class='button-secondary easyloginstyler-remove-img'>Remove</a><img src='"+attachment.url+"'></li>" );
                        attachments.push(attachment.url);
                        //
                     });
                     var attachment_string = $('#images-input').val() + attachments.join() + ',';
                     $('#images-input').val(attachment_string).trigger('change');
                 });
                 custom_uploader.open();
             });

             $( document ).on( 'click', '.easyloginstyler-remove-img', function(){
                var img_src = $( event.target ).parent().find( 'img' ).attr('src');

                $( event.target ).parent().parent().find( 'img[src="'+ img_src +'"]' ).closest('li').remove();
                var links = $('.image-list input[type="text"]').map(function() {
                    return this.value;
                }).get();

                var attachment_arr = $('.images .image-list img').map(function() {
                    return $( this ).attr( 'src' );
                }).get();

                if( attachment_arr.length > 0 ){
                    attachment_arr = attachment_arr + ',';
                }

                $( '#images-input' ).val( attachment_arr ).trigger('change');
                
             });

            $( document ).on( 'input', '.image-list input[type="text"]', function(){
                var links = $('.image-list input[type="text"]').map(function() {
                    return this.value;
                }).get();
            } );
         });

} )( jQuery );