<?php
/**
 * Create dashboard welcome page after activation
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * EasyLoginStyler_Pro_Dashboard_Welcome Class
 */
class EasyLoginStyler_Pro_Dashboard_Welcome {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'screen_page' ) );
		add_action( 'activated_plugin', array( $this, 'redirect' ) );
		add_action( 'admin_head', array( $this, 'remove_menu' ) );
	}

	/**
	 * Setup the admin menu.
	 */
	public function screen_page() {
		add_dashboard_page(
			__( 'Easy Login Styler Pro', 'easy-login-styler' ),
			__( 'Easy Login Styler Pro', 'easy-login-styler' ),
			apply_filters( 'easyloginstyler_welcome_screen_capability', 'manage_options' ),
			'easy-login-styler-pro--welcome',
			array( $this, 'content' )
		);
	}

	/**
	 * Remove the menu item from the admin.
	 */
	public function remove_menu() {
		remove_submenu_page( 'index.php', 'easy-login-styler-pro--welcome' );
	}

	/**
	 * Page header.
	 */
	public function header() {

		$selected = isset( $_GET['page'] ) ? $_GET['page'] : 'easy-login-styler-pro--welcome';
		?>
		<h1><?php echo esc_html__( 'Welcome to Easy Login Styler Pro', 'easy-login-styler' ); ?></h1>

		<?php
	}

	/**
	 * Page content.
	 */
	public function content() {
		$details = get_option( 'easy_login_styler_pro_license_active' );
        
	?>
		<div class="wrap about-wrap easy-login-styler--about-wrap">
			<?php $this->header(); ?>
			<div class="about-description">
				<p><?php 
					if ( is_object( $details ) && 'valid' === $details->license ) {
						echo sprintf( esc_html__( 'We recommend you to %swatch this instructions below%s to get started, then you will be up and running in no time.', 'easy-login-styler' ), '<strong>', '</strong>' );
					}else{
						echo sprintf( esc_html__( 'We recommend you to %sactivate your license%s and watch this instructions below to get started, then you will be up and running in no time.', 'easy-login-styler' ), '<strong>', '</strong>' );
					} ?>
				</p>
				
				<?php 
				if ( is_object( $details ) && 'valid' === $details->license ) {
		            //valid license
		        }else{ ?>
					<div class="license-activation">
						<form action="#" method="post" accept-charset="utf-8">
							<p><input type="text" id="easy-login-styler-pro-license-code" class="regular-text" placeholder="Enter 'NULLED' Here!"></p>
							<p><input type="submit" class="button button-primary" value="<?php echo esc_html__( 'Activate License', 'easy-login-styler' );?>" /> <span class="spinner" style="float: none;"></span> </p>
						</form>
					</div>
				<?php } ?>
				<div class="featured-video">
					<iframe width="500" height="290" src="https://www.youtube.com/embed/qnUBKKBanQU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<p class="cta-buttons">
					<a href="<?php echo esc_url( admin_url( 'options-general.php?page=easy-login-styler' ) ); ?>" class="button button-primary"><?php echo esc_html__( 'Style Your Login Page Now', 'easy-login-styler' ); ?></a>
					<a href="https://easyloginwp.com/setup-wordpress-login-page/?utm_source=welcome-page" class="button button-secondary" target="_blank"><?php echo esc_html__( 'View Full Tutorials', 'easy-login-styler' ); ?></a>
				</p>
			</div>
		</div>
		<style type="text/css" media="screen">
			.easy-login-styler--about-wrap h1{
		    	color: #000;
		    	text-align: center;
		    	font-size: 31px;
		    	margin: 50px 0px 30px;
		    }
		    .easy-login-styler--about-wrap .regular-text{
		    	max-width:  100%;
		    }
		    .easy-login-styler--about-wrap .about-description{
		    	font-size: 16px;
		    	color: #000;
		    	background: #fff;
			    border: 1px solid #e1e1e1;
			    padding: 40px;
			    box-shadow: 1px 5px 15px rgba(0,0,0,0.02);
			    border-radius: 2px;
			    text-align: center;
		    }
		    .easy-login-styler--about-wrap .about-description .featured-video{
		    	margin-top: 30px;
		    }
		    .easy-login-styler--about-wrap .about-description .cta-buttons .button{
		    	font-size: 16px;
		    	height: auto;
		    	width: auto;
		    	padding: 15px 30px;
		    	margin: 10px;
		    }
		    .easy-login-styler--about-wrap iframe{
		    	max-width: 100%;
		    }
		</style>
		<script>
			jQuery( document ).ready( function(){
				jQuery( '.easy-login-styler--about-wrap .license-activation form' ).on( 'submit', function(){
					var license = jQuery( '#easy-login-styler-pro-license-code' ).val();
					jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'visible' });
					if( license ){

						var postData = {
			                'action'		: 'easy_login_styler_pro_ajax_settings',
			                'nonce'			: '<?php echo wp_create_nonce( 'easy_login_styler_pro-settings-nonce' );?>',
			                'method'		: 'activate',
			                'license-data'	: license,
			            };

			            jQuery.post( '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', postData )
			                .always(function( a, status, b ) {
			                    if( status == "success" ){
			                        a = jQuery.parseJSON( a  );
			                        console.log( a );
			                        if( 'undefined' !== a.response ){
			                            if( 'undefined' !== a.response.license && a.response.license == 'valid' ){
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .notice-error' ).remove();
			                                jQuery( '.easy-login-styler--about-wrap .license-activation' ).prepend( '<div class="notice notice-success" style="display: block !important; width: auto; display: inline-block !important; padding: 5px 15px; font-size: 14px;"><?php echo esc_html__( 'License Successfully Activated. Please make sure to watch the video guide below. Thanks!', 'easy-login-styler' ); ?></div>' );
			                                jQuery( '.easy-login-styler--about-wrap .license-activation form' ).hide();
			                            }else{
			                            	jQuery( '.easy-login-styler--about-wrap .license-activation' ).prepend( '<div class="notice notice-error" style="display: block !important; width: auto; display: inline-block !important; padding: 5px 15px; font-size: 14px;"><?php echo esc_html__( 'Please provide valid license code. Thanks!', 'easy-login-styler' ); ?></div>' );
			                            }

			                            jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'hidden' });
			                        }
			                    }
			                });

						jQuery( '#easy-login-styler-pro-license-code' ).css({ 'border-color' : '' });
					}else{
						jQuery( '#easy-login-styler-pro-license-code' ).css({ 'border-color' : 'red' });
						jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'hidden' });
					}

					return false;
				} );
			} );
		</script>
	<?php
	}

	/**
	 * Redirect to the welcome page upon plugin activation.
	 */
	public function redirect( $plugin ) {
		if ( ( $plugin == 'easy-login-styler-pro/plugin.php' ) && ! isset( $_GET['activate-multi'] ) ) {
			wp_safe_redirect( admin_url( 'index.php?page=easy-login-styler-pro--welcome' ) );
			die();
		}
	}
}

return new EasyLoginStyler_Pro_Dashboard_Welcome();