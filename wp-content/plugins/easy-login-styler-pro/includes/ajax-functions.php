<?php
/**
 * AJAX Functions
 *
 * Process AJAX actions.
 *
 * @copyright   Copyright (c) 2016, Jeffrey Carandang
 * @since       4.0
 */
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Save Options
 *
 * @since 1.0
 * @return void
 */
function easy_login_styler_pro_ajax_save_settings(){
    $response = array( 'errors' => array() );

	if( !isset( $_POST['method'] ) ) return;
	if( !isset( $_POST['nonce'] ) ) return;

	if ( ! wp_verify_nonce( $_POST['nonce'], 'easy_login_styler_pro-settings-nonce' ) ) {
		return;
	}

	global $easy_login_styler_license;
	$license = sanitize_text_field( $_REQUEST['license-data'] );

	$network = false;

	if( isset( $_POST['network'] ) ){
		$network = true;
	}

	switch ( $_POST['method'] ) {
		case 'activate':
			if( !empty( $license ) ){
				$item_shortname = preg_replace( '/[^a-zA-Z0-9_\s]/', '', str_replace( ' ', '_', strtolower( EASY_LOGIN_STYLER_PRO_PLUGIN_NAME ) ) );

				$data = $easy_login_styler_license->activate_license( $license, wp_create_nonce('easy_login_styler_license_nonce'), $network );

				$response['button'] = sanitize_text_field( $_POST['data']['button'] );
				
				$data->success = true;
				$data->license = 'valid';
				$data->item_id = 1;
				$data->error = '';
				update_option('easy_login_styler_pro_license_active', $data );
				
				$response['response'] = $data;
			}
			break;

		case 'deactivate':
			if( !empty( $license ) ){
				$item_shortname = preg_replace( '/[^a-zA-Z0-9_\s]/', '', str_replace( ' ', '_', strtolower( EASY_LOGIN_STYLER_PRO_PLUGIN_NAME ) ) );

				$data = $easy_login_styler_license->deactivate_license( $license, wp_create_nonce('easy_login_styler_license_nonce'), $network );

				$response['button'] = sanitize_text_field( $_POST['data']['button'] );
				$response['response'] = $data;
			}
			break;
		
		default:
			# code...
			break;
	}
	
	$response 				= (object) $response;
	
	echo json_encode( $response );
	die();
}
add_action( 'wp_ajax_easy_login_styler_pro_ajax_settings',  'easy_login_styler_pro_ajax_save_settings' );
?>