<?php
/**
 * Install Function
 *
 * @copyright   Copyright (c) 2017, Jeffrey Carandang
 * @since       1.0
*/
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

//add settings link on plugin page
if( !function_exists( 'easy_login_styler_filter_plugin_actions' ) ){
  add_action( 'plugin_action_links_' . plugin_basename( EASY_LOGIN_STYLER_PRO_PLUGIN_FILE ) , 'easy_login_styler_filter_plugin_actions' );
  function easy_login_styler_filter_plugin_actions($links){
    $links[]  = '<a href="'. esc_url( admin_url( 'options-general.php?page=easy-login-styler' ) ) .'">' . __( 'Settings', 'easy-login-styler' ) . '</a>';
    return $links;
  }
}

//register default values
if( !function_exists( 'easyloginstyler_register_defaults' ) ){
	register_activation_hook( EASY_LOGIN_STYLER_PRO_PLUGIN_FILE, 'easyloginstyler_register_defaults' );
  	// add_action( 'plugins_loaded', 'easyloginstyler_register_defaults', 99 );
	function easyloginstyler_register_defaults(){
		if( is_admin() ){
			if( is_plugin_active( 'easy-login-styler/plugin.php' ) ){
				deactivate_plugins( '/easy-login-styler/plugin.php' );
			}

			if( !get_option( 'easy_login_styler_pro_installDate' ) ){

				$free_options = get_option( 'easy_login_styler_settings' );
				if( $free_options ){
					$easy_login_customizer = get_option( 'easy_login_styler_customizer' );
					
					if( empty( $easy_login_customizer ) ){
						$easy_login_customizer = array();
					}

					$easy_login_customizer['logo'] 		= ( isset( $free_options['logo'] ) ) ? $free_options['logo'] : '';
					$easy_login_customizer['images'] 	= ( isset( $free_options['background'] ) ) ? $free_options['background']  . ',' : '';
					$easy_login_customizer['css'] 		= ( isset( $free_options['css'] ) ) ? $free_options['css'] : '';

					//add default template settings
					$easy_login_customizer['logoshow'] 	= true;
					$easy_login_customizer['template']	= 'template-default';
					$easy_login_customizer['layout']	= 'easy-login-default-left';
					$easy_login_customizer['txtalign']	= 'center';
					$easy_login_customizer['loginwidth']= 420;
					$easy_login_customizer['loginbg'] 	= '#FFFFFF';
					$easy_login_customizer['txtcolor'] 	= '#484848';
					$easy_login_customizer['txtfld'] 	= 'boxed';
					$easy_login_customizer['txtfldcolor'] 	= '#32373c';
					$easy_login_customizer['txtfldbg'] 	= '#FFFFFF';
					$easy_login_customizer['txtfldbr'] 	= '#DDDDDD';
					$easy_login_customizer['buttonsize']= 'l';
					$easy_login_customizer['buttonrd'] 	= 3;
					$easy_login_customizer['hideback'] 	= false;
					$easy_login_customizer['logowidth'] = 84;
					$easy_login_customizer['logoheight']= 84;
					
					//update option with free ones
					update_option( 'easy_login_styler_customizer', $easy_login_customizer );
				}else{
					$easy_login_customizer = array();

					//add default pro template settings
					$easy_login_customizer['logoshow'] 	= false;
					$easy_login_customizer['template']	= 'template-1';
					$easy_login_customizer['desc']		= 'Welcome back! Please login to your account.';
					$easy_login_customizer['layout']	= 'easy-login-default-left';
					$easy_login_customizer['txtalign']	= 'left';
					$easy_login_customizer['loginwidth']= 500;
					$easy_login_customizer['font']		= 'Merriweather';
					$easy_login_customizer['loginbg'] 	= '#FFFFFF';
					$easy_login_customizer['txtcolor'] 	= '#000000';
					$easy_login_customizer['txtfld'] 	= 'boxed';
					$easy_login_customizer['txtfldcolor'] 	= '#000000';
					$easy_login_customizer['txtfldbg'] 	= '#FFFFFF';
					$easy_login_customizer['txtfldbr'] 	= '#DDDDDD';
					$easy_login_customizer['buttonsize']= 'm';
					$easy_login_customizer['buttonbg'] 	= '#000000';
					$easy_login_customizer['buttonbr'] 	= '#000000';
					$easy_login_customizer['buttoncr'] 	= '#FFFFFF';
					$easy_login_customizer['buttonrd'] 	= 2;
					$easy_login_customizer['hideback'] 	= false;
					$easy_login_customizer['noticecolor'] = '#111111';
					$easy_login_customizer['errorcolor']= '#111111';

					update_option( 'easy_login_styler_customizer', $easy_login_customizer );
				}

				add_option( 'easy_login_styler_pro_installDate', date( 'Y-m-d h:i:s' ) );
			}

			if( !get_option( 'easy_login_styler_pro_settings' ) ){
				easyloginstyler_create_login_handler();
			}

		}
	}
}

//create custom page
if( !function_exists( 'easyloginstyler_create_login_handler' ) ){
	function easyloginstyler_create_login_handler(){
		$login_page = wp_insert_post(
			array(
				'post_title'     => 'Easy Login Styler',
				'post_content'   => 'Automatically created for Easy Login Styler Plugin customizer support. Thanks!',
				'post_status'    => 'private',
				'post_author'    => 1,
				'post_type'      => 'page',
				'comment_status' => 'closed',
			)
		);
		$options = get_option( 'easy_login_styler_pro_settings' );
		if ( -1 !== $login_page ) {
			if( $options && is_array( $options ) ){
				$options['page'] = $login_page;
			}else{
				$options = array( 'page' => $login_page );
			}

			update_post_meta( $login_page, '_wp_page_template', 'easy-login-styler-page-template.php' );
		}

		update_option( 'easy_login_styler_pro_settings', $options );
	}
}

?>
