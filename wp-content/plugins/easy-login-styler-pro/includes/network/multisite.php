<?php
/*
 * Multisite Compatibility
 */

if( !function_exists( 'easy_login_styler_network_menu' ) ):
    add_action( 'network_admin_menu', 'easy_login_styler_network_menu' );
    function easy_login_styler_network_menu(){
        add_submenu_page( 
        	'settings.php',
	        esc_html__( 'Easy Login Styler Network Settings', 'easy-login-styler' ), 
	        esc_html__( 'Easy Login Styler', 'easy-login-styler' ), 
	        'capability', 
	        'easy_login_styler_network', 
	        'easy_login_styler_network_content' 
	    );
    }

    function easy_login_styler_network_content(){ 
    	$license_data = get_site_option( 'easy_login_styler_pro_license_active' );
    	$license = get_site_option( 'easy_login_styler_pro_license_data' );

    	if( !empty( $license_data ) && is_object( $license_data ) && isset( $license_data->license ) && $license_data->license == 'valid' ){
    		$hidden = 'deactivate';
    	}else{
    		$hidden = 'activate';
    	}
    	?>
    	<div class="wrap about-wrap easy-login-styler--about-wrap">
    		<h3><?php echo esc_html__( 'Easy Login Styler Network Settings', 'easy-login-styler' ); ?></h3>
			<div class="about-description">
				<p><?php echo sprintf( esc_html__( '%sNetwork Activate%s Easy Login Styler Pro license so you won\'t have to do it manually one by one.', 'easy-login-styler' ), '<strong>', '</strong>' );?></p>
				<div class="license-activation">
					<form action="#" method="post" accept-charset="utf-8">
						<input type="hidden" id="easy-login-styler-pro-license-method" value="<?php echo $hidden;?>">
						<p><input type="text" id="easy-login-styler-pro-license-code" class="regular-text" placeholder="<?php echo esc_html__( 'Add your license code here', 'easy-login-styler' );?>" value="<?php echo sanitize_text_field( $license ); ?>">
						<span style="color: #777; font-size: 12px;"><?php if( !empty( $license_data ) && is_object( $license_data ) && isset( $license_data->license ) && $license_data->license == 'valid' ){
								_e( 'Valid until ' . date( 'M d, Y', strtotime( $license_data->expires ) ), 'widget-options' );
							} ?></span>
						</p>
						<p>
							<input type="submit" class="button <?php echo ( $hidden == 'activate' ) ? 'button-primary' : 'button-secondary';?>" value="<?php echo ( $hidden == 'activate' ) ? esc_html__( 'Activate License', 'easy-login-styler' ) : esc_html__( 'Deactivate License', 'easy-login-styler' );?>" />
							 <span class="spinner" style="float: none;"></span> </p>
					</form>
				</div>
			</div>
		</div>
		<style type="text/css" media="screen">
			.easy-login-styler--about-wrap{
				text-align: center;
			}
			.easy-login-styler--about-wrap .regular-text{
		    	max-width:  100%;
		    }
		    .easy-login-styler--about-wrap .about-description{
		    	font-size: 16px;
		    	color: #000;
		    	background: #fff;
			    border: 1px solid #e1e1e1;
			    padding: 40px;
			    box-shadow: 1px 5px 15px rgba(0,0,0,0.02);
			    border-radius: 2px;
			    text-align: center;
		    }
		</style>
		<script>
			jQuery( document ).ready( function(){
				jQuery( '.easy-login-styler--about-wrap .license-activation form' ).on( 'submit', function(){
					var license = jQuery( '#easy-login-styler-pro-license-code' ).val();
					var method = jQuery( '#easy-login-styler-pro-license-method' ).val();
					jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'visible' });
					if( license && method ){

						var postData = {
			                'action'		: 'easy_login_styler_pro_ajax_settings',
			                'nonce'			: '<?php echo wp_create_nonce( 'easy_login_styler_pro-settings-nonce' );?>',
			                'method'		: method,
			                'license-data'	: license,
			                'network'		: true,
			            };

			            if( 'activate' == method ){
			            	jQuery.post( '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', postData )
			                .always(function( a, status, b ) {
			                    if( status == "success" ){
			                        a = jQuery.parseJSON( a  );
			                        
			                        if( 'undefined' !== a.response ){
			                            if( 'undefined' !== a.response.license && a.response.license == 'valid' ){
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .notice-error' ).remove();

			                                jQuery( '#easy-login-styler-pro-license-method' ).val('deactivate');
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .button' ).val('<?php echo esc_html__( 'Deactivate License', 'easy-login-styler' );?>');
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .button' ).removeClass('button-primary').addClass('button-secondary');

			                                jQuery( '.easy-login-styler--about-wrap .license-activation' ).prepend( '<div class="notice notice-success" style="display: block !important; width: auto; display: inline-block !important; padding: 5px 15px; font-size: 14px;"><?php echo esc_html__( 'License Successfully Activated. Thanks!', 'easy-login-styler' ); ?></div>' );
			                            }else{
			                            	jQuery( '.easy-login-styler--about-wrap .license-activation' ).prepend( '<div class="notice notice-error" style="display: block !important; width: auto; display: inline-block !important; padding: 5px 15px; font-size: 14px;"><?php echo esc_html__( 'Please provide valid license code for Multisite installation. Thanks!', 'easy-login-styler' ); ?></div>' );
			                            }

			                            jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'hidden' });
			                        }
			                    }
			                });
			            }else if( 'deactivate' == method ){
			            	jQuery.post( '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', postData )
			                .always(function( a, status, b ) {
			                    if( status == "success" ){
			                        a = jQuery.parseJSON( a  );
			                        if( 'undefined' !== a.response ){
			                            if( 'undefined' !== a.response && a.response == 'deactivated' ){
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .notice' ).remove();
			                                
			                                jQuery( '#easy-login-styler-pro-license-code' ).val('');
			                                jQuery( '#easy-login-styler-pro-license-method' ).val('activate');
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .button' ).val('<?php echo esc_html__( 'Activate License', 'easy-login-styler' );?>');
			                                jQuery( '.easy-login-styler--about-wrap .license-activation .button' ).removeClass('button-secondary').addClass('button-primary');

			                                jQuery( '.easy-login-styler--about-wrap .license-activation span' ).remove();
			                                jQuery( '.easy-login-styler--about-wrap .license-activation' ).prepend( '<div class="notice notice-info" style="display: block !important; width: auto; display: inline-block !important; padding: 5px 15px; font-size: 14px;"><?php echo esc_html__( 'License Successfully Deactivated. Thanks!', 'easy-login-styler' ); ?></div>' );
			                                jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'hidden' });
			                            }
			                        }
			                    }
			                });
			            }

						jQuery( '#easy-login-styler-pro-license-code' ).css({ 'border-color' : '' });
					}else{
						jQuery( '#easy-login-styler-pro-license-code' ).css({ 'border-color' : 'red' });
						jQuery( '.easy-login-styler--about-wrap .spinner' ).css({ 'visibility' : 'hidden' });
					}

					return false;
				} );
			} );
		</script>
    <?php }
endif;

?>