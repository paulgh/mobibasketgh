<?php
/**
 * Handles hooks and actions for wp-login.php
 *
 */

if( !function_exists( 'easy_login_styler_enqueue' ) ):
    add_action( 'login_enqueue_scripts', 'easy_login_styler_enqueue', 10 );
    function easy_login_styler_enqueue(){
        $options = get_option( 'easy_login_styler_customizer' );

        if( isset( $options['font'] ) && !empty( $options['font'] ) ){
            $gfonts      = str_replace( ' ', '+', $options['font'] ) . ':100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic';
            $query_args = array(
                'family' => $gfonts,
            );

            wp_register_style(
                'easy-login-styler-font',
                add_query_arg( $query_args, '//fonts.googleapis.com/css' ),
                array(),
                null
            );
            wp_enqueue_style( 'easy-login-styler-font' );
        }
            

        wp_enqueue_style( 'login-styler', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/css/easy-login-styler.css', false );
        wp_enqueue_style( 'login-styler-focuswp', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/css/focuswp.css', false );
        wp_enqueue_script( 'jquery-login-styler-focuswp', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/js/jquery.focus.min.js',  array( 'jquery' ), false, true );
    }
endif;

if( !function_exists( 'easyloginstyler_head' ) ):
    add_action( 'login_head', 'easyloginstyler_head' );
    function easyloginstyler_head(){
        $options = get_option( 'easy_login_styler_customizer' );
        if( !empty( $options ) ){ 
            if( ( !isset( $options['logowidth'] ) || empty( $options['logowidth'] ) ) ){
                $options['logowidth'] = 84;
            }
            if( ( !isset( $options['logoheight'] ) || empty( $options['logoheight'] ) ) ){
                $options['logoheight'] = 84;
            }
            ?>
            <style>
                <?php if( isset( $options['loginwidth'] ) && !empty( $options['loginwidth'] ) ): ?>
                    body #login{
                        width: <?php esc_html_e( $options['loginwidth'] ); ?>px;
                    }
                <?php endif; ?>

                <?php if( isset( $options['font'] ) && !empty( $options['font'] ) ): ?>
                    body{
                        font-family: '<?php esc_html_e( $options['font'] ); ?>';
                    }
                <?php endif; ?>

                <?php if( isset( $options['logo'] ) && !empty( $options['logo'] ) ): ?>
                    body #login h1 a{
                        background-image: url( <?php esc_html_e( $options['logo'] ); ?> );
                    }
                <?php endif; ?>

                body #login h1 a{
                    width: <?php esc_html_e( $options['logowidth'] ); ?>px;
                    height: <?php esc_html_e( $options['logoheight'] ); ?>px;
                    background-size:  <?php esc_html_e( $options['logowidth'] ); ?>px <?php esc_html_e( $options['logoheight'] ); ?>px;
                }

                <?php if( isset( $options['loginbg'] ) && !empty( $options['loginbg'] ) ): ?>
                    body #login, body #login form{
                        background-color: <?php esc_html_e( $options['loginbg'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['txtcolor'] ) && !empty( $options['txtcolor'] ) ): ?>
                    body #login, body.login #login h2, body.login #login p:not(.message), body.login #login form label, body.login #login a{
                        color: <?php esc_html_e( $options['txtcolor'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['txtfldcolor'] ) && !empty( $options['txtfldcolor'] ) ): ?>
                    body #login form .input{
                        color: <?php esc_html_e( $options['txtfldcolor'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['txtfldbg'] ) && !empty( $options['txtfldbg'] ) ): ?>
                    body #login form .input{
                        background-color: <?php esc_html_e( $options['txtfldbg'] ); ?>;
                    }
                <?php endif; ?>
                
                <?php if( isset( $options['txtfldbr'] ) && !empty( $options['txtfldbr'] ) ): ?>
                    body #login form .input{
                        border-color: <?php esc_html_e( $options['txtfldbr'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['buttonbg'] ) && !empty( $options['buttonbg'] ) ): ?>
                    body #login form .submit .button{
                        background-color: <?php esc_html_e( $options['buttonbg'] ); ?>;
                    }
                    body #login form .submit .button:hover{
                        opacity: 0.95;
                    }
                <?php endif; ?>

                <?php if( isset( $options['buttonbr'] ) && !empty( $options['buttonbr'] ) ): ?>
                    body #login form .submit .button{
                        border-color: <?php esc_html_e( $options['buttonbr'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['buttoncr'] ) && !empty( $options['buttoncr'] ) ): ?>
                    body #login form .submit .button{
                        color: <?php esc_html_e( $options['buttoncr'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['buttonrd'] ) && !empty( $options['buttonrd'] ) ): ?>
                    body #login form .submit .button{
                        border-radius: <?php esc_html_e( $options['buttonrd'] ); ?>px;
                        -moz-border-radius: <?php esc_html_e( $options['buttonrd'] ); ?>px;
                        -webkit-border-radius: <?php esc_html_e( $options['buttonrd'] ); ?>px;
                    }
                <?php endif; ?>

                <?php if( isset( $options['noticebg'] ) && !empty( $options['noticebg'] ) ): ?>
                    body #login .message{
                        background-color: <?php esc_html_e( $options['noticebg'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['noticecolor'] ) && !empty( $options['noticecolor'] ) ): ?>
                    body #login .message{
                        color: <?php esc_html_e( $options['noticecolor'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['errorbg'] ) && !empty( $options['errorbg'] ) ): ?>
                    body #login #login_error{
                        background-color: <?php esc_html_e( $options['errorbg'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['errorcolor'] ) && !empty( $options['errorcolor'] ) ): ?>
                    body #login #login_error{
                        color: <?php esc_html_e( $options['errorcolor'] ); ?>;
                    }
                <?php endif; ?>

                <?php if( isset( $options['css'] ) && !empty( $options['css'] ) ){
                    esc_html_e( $options['css'] );
                }?>
            </style>
        <?php }

        //labels
        add_filter( 'gettext', 'easyloginstyler_translations', 20, 3 ); 
    }
endif;

if( !function_exists( 'easyloginstyler_translations' ) ){
    function easyloginstyler_translations( $translated_text, $original, $domain ){
        $options = get_option( 'easy_login_styler_customizer' );
        if ( in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {

            $text = array();

            if( is_array( $options ) && isset( $options['username'] ) ){
                $text['Username or Email Address'] = sanitize_text_field( $options['username'] );
            }

            if( is_array( $options ) && isset( $options['password'] ) ){
                $text['Password'] = sanitize_text_field( $options['password'] );
            }

            if( is_array( $options ) && isset( $options['remember'] ) ){
                $text['Remember Me'] = sanitize_text_field( $options['remember'] );
            }

            if( is_array( $options ) && isset( $options['loginbtn'] ) ){
                $text['Log In'] = sanitize_text_field( $options['loginbtn'] );
                $text['Log in'] = sanitize_text_field( $options['loginbtn'] );
            }

            if( is_array( $options ) && isset( $options['regbtn'] ) ){
                $text['Register'] = sanitize_text_field( $options['regbtn'] );
            }

            if( is_array( $options ) && isset( $options['passbtn'] ) ){
                $text['Get New Password'] = sanitize_text_field( $options['passbtn'] );
            }

            if( is_array( $options ) && isset( $options['lostpass'] ) ){
                $text['Lost your password?'] = sanitize_text_field( $options['lostpass'] );
            }

            if ( ! empty( $text[$original] ) ) {
                $translations = &get_translations_for_domain( $domain );
                $translated_text = $translations->translate( $text[$original] );
            }
        }

        return $translated_text;
    }
}

if( !function_exists( 'easyloginstyler_body_class' ) ):
    add_action( 'login_body_class', 'easyloginstyler_body_class' );
    function easyloginstyler_body_class( $classes ){

        $options = get_option( 'easy_login_styler_customizer' );

        $classes[] = ( isset( $options['layout'] ) && !empty( $options['layout'] ) ) ? $options['layout'] : 'easy-login-default-left';
         $classes[] = ( isset( $options['template'] ) && !empty( $options['template'] ) ) ? 'easy-login-' . $options['template'] : 'easy-login-template-default';
        $classes[] = ( isset( $options['logostyle'] ) && !empty( $options['logostyle'] ) ) ? 'easy-login-logo-'. $options['logostyle'] : 'easy-login-logo-circular';
        $classes[] = ( isset( $options['logoalign'] ) && !empty( $options['logoalign'] ) ) ? 'easy-login-logo-'. $options['logoalign'] : 'easy-login-logo-center';
        $classes[] = ( isset( $options['txtfld'] ) && !empty( $options['txtfld'] ) ) ? 'easy-login-txtfld-'. $options['txtfld'] : 'easy-login-txtfld-boxed';
        $classes[] = ( isset( $options['logoshow'] ) && !empty( $options['logoshow'] ) ) ? 'easy-login-logo-true' : 'easy-login-logo-false';
        $classes[] = ( isset( $options['txtalign'] ) && !empty( $options['txtalign'] ) ) ? 'easy-login-text-'. $options['txtalign'] : 'easy-login-text-center';
        $classes[] = ( isset( $options['buttonsize'] ) && !empty( $options['buttonsize'] ) ) ? 'easy-login-btn-'. $options['buttonsize'] : 'easy-login-btn-l';
        $classes[] = ( isset( $options['hideback'] ) && !empty( $options['hideback'] ) ) ? 'easy-login-hideback-true' : 'easy-login-hideback-false';
        return $classes;
    }
endif;



if( !function_exists( 'easy_login_styler_title' ) ):
    add_filter( 'login_message', 'easy_login_styler_title' );
    function easy_login_styler_title( $message ) {
        if( isset( $_GET['action'] ) ){
            return $message;
        }
        $options = get_option( 'easy_login_styler_customizer' );
        return '<h2 class="easy-login-styler-title">'. ( (isset( $options[ 'title' ] ) && !empty( $options[ 'title' ] )) ? esc_html__( $options[ 'title' ] ) : 'Log In' ) .'</h2><p class="easy-login-styler-desc '. ( (isset( $options[ 'desc' ] ) && !empty( $options[ 'desc' ] )) ? 'easy-login-styler-desc-show' : '' ) .'">'. ( (isset( $options[ 'desc' ] ) && !empty( $options[ 'desc' ] )) ? esc_html__( $options[ 'desc' ] ) : '' ) .'</p>' . $message;
    }
endif;

if( !function_exists( 'easy_login_styler_url' ) ):
    add_filter( 'login_headerurl', 'easy_login_styler_url' );
    function easy_login_styler_url( $url ){
        $options = get_option( 'easy_login_styler_settings' );

        if( isset( $options['logo'] ) && !empty( $options['logo'] ) ){
            $url = esc_url( home_url( '/' ) );
        }

        return $url;
    }
endif;

if( !function_exists( 'easy_login_styler_alt' ) ):
    add_filter( 'login_headertitle', 'easy_login_styler_alt' );
    function easy_login_styler_alt( $alt ){
        $options = get_option( 'easy_login_styler_settings' );

        if( isset( $options['logo'] ) && !empty( $options['logo'] ) ){
            $alt = get_bloginfo( 'name' );
        }

        return $alt;
    }
endif;

if( !function_exists( 'easy_login_styler_footer' ) ):
    add_action( 'login_footer', 'easy_login_styler_footer' );
    function easy_login_styler_footer( $input_id ){ 
        $options = get_option( 'easy_login_styler_customizer' ); 
        $style   = '';

        if( !empty( $options ) ){
            if( isset( $options['images'] ) && !empty( $options['images'] ) ){
                $options['images'] = array_filter( explode( ',', $options['images'] ) );
            }

            if( isset( $options['images'] ) && !empty( $options['images'] ) && count( $options['images'] ) == 1 ){
               $style = 'background-image: url('. $options['images'][0] .')';
            }
        }
        ?>
        <div class="easy-login-styler-bg" style="<?php echo $style; ?>">
            <?php 
            if( isset( $options['images'] ) && !empty( $options['images'] ) ){
                $options['images'] = array_filter( explode( ',', $options['images'] ) );
                if( is_array( $options['images'] ) && count( $options['images'] ) > 1 ){ ?>
                    <div class="easy-login-slides">
                        <?php foreach( $options['images'] as $k => $v ){ ?>
                            <li>
                                <img src="<?php echo $v;?>">
                            </li>
                        <?php } ?>
                    </div>
                <?php }
            }
            ?>
        </div>
        <div class="easy-login-styler-clear"></div>
    <?php }
endif;

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
if( !function_exists( 'easy_login_styler_redirect' ) ):
    add_filter( 'login_redirect', 'easy_login_styler_redirect', 10, 3 );
    function easy_login_styler_redirect( $redirect_to, $request, $user ) {
        $options = get_option( 'easy_login_styler_customizer' ); 
        if( isset( $options['redirect'] ) && !empty( $options['redirect'] ) ){
            $redirect_to = esc_url( $options['redirect'] );
        }

        return $redirect_to;
    }
endif;
