<?php
/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 3.0
 * @global $widget_options
 * @param string $hook Page hook
 * @return void
 */
if( !function_exists( 'easy_login_styler_admin_scripts' ) ):
    add_action( 'admin_enqueue_scripts', 'easy_login_styler_admin_scripts', 100 );
    function easy_login_styler_admin_scripts( $hook ) {
        wp_enqueue_style( 'login-styler-interim', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/css/interim.css', false );
    }
endif;
?>