<?php

if (!class_exists('WP_Customize_Image_Control')) {
    return null;
}

class EasyLoginStyler_Multi_Image_Custom_Control extends WP_Customize_Control{
    public function enqueue(){
        wp_enqueue_media();
        wp_enqueue_style( 'easyloginstyler-multi-image-style', EASY_LOGIN_STYLER_PRO_PLUGIN_URL .'assets/css/controls/multi-image.css' );
        wp_enqueue_script( 'easyloginstyler-multi-image-script', EASY_LOGIN_STYLER_PRO_PLUGIN_URL .'assets/js/controls/multi-image.js', array( 'jquery' ), rand(), true );
    }

    public function render_content(){ ?>
        <label>
            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <?php if ( ! empty( $this->description ) ) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>
        </label>
        <div>
            <ul class='images'></ul>
        </div>

        <div class='actions'>
            <a class="button-secondary upload"><?php esc_html_e( 'Select Images', 'easy-login-styler' )?></a>
        </div>

        <input class="wp-editor-area" id="images-input" type="hidden" <?php $this->link(); ?>>
      <?php
    }
}
?>