<?php

if (!class_exists('WP_Customize_Control')) {
    return null;
}

class EasyLoginStyler_Activation_Notice_Control extends WP_Customize_Control{
    public function enqueue(){
    
    }

    public function render_content(){ 
        $details = get_option( 'easy_login_styler_pro_license_active' );
        
        if ( is_object( $details ) && 'valid' === $details->license ) {
            return;
        }

        if( function_exists( 'is_multisite' ) && is_multisite() ){
            $license_data = get_site_option( 'easy_login_styler_pro_license_active' );
            if( !empty( $license_data ) && is_object( $license_data ) && isset( $license_data->license ) && $license_data->license == 'valid' ){
                return;
            }
        }

        ?>
        <div class="notice notice-error easy_login_styler_activation-notice" style="padding: 10px 15px;">
            <?php
                echo $message = sprintf( esc_html__( 'Register your copy of %sEasy Login Styler Pro%s to receive access to automatic upgrades and support. Click the back button above then Activate License setting. Need a license key? %sPurchase one now%s.', 'easy-login-styler' ), '<strong>', '</strong>', '<a href="https://easyloginwp.com/?utm_source=customizer_notice" target="_blank">', '</a>' );
            ?>
        </div>
      <?php
    }
}
?>