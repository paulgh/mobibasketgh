<?php
/**
 * Template Name: Easy Login Styler Editor
 * Load wp-login.php as template for customizer editing
 */

// Redirect if viewed from outside the Customizer.
if ( ! is_customize_preview() ) {

	global $easy_login_styler;

	$url = add_query_arg(
		array(
			'autofocus[section]' => 'easy_login_styler--templates',
			'url' => rawurlencode( esc_url( get_permalink( $easy_login_styler['page'] ) ) ) ,
		),
		admin_url( 'customize.php' )
	);

	wp_safe_redirect( $url );
}
/**
 * Output the login page header.
 *
 * @param string   $title    Optional. WordPress login Page title to display in the `<title>` element.
 *                           Default 'Log In'.
 * @param string   $message  Optional. Message to display in header. Default empty.
 * @param WP_Error $wp_error Optional. The error to pass. Default empty.
 */
function easyloginstyler_login_header( $title = 'Log In', $message = '', $wp_error = '' ) {

	global $error, $action;

	// Don't index any of these forms.
	add_action( 'login_head', 'wp_no_robots' );

	if ( empty( $wp_error ) ) {
		$wp_error = new WP_Error();
	}

	$login_title = get_bloginfo( 'name', 'display' );

	/* translators: Login screen title. 1: Login screen name, 2: Network or site name */
	$login_title = sprintf( __( '%1$s &lsaquo; %2$s &#8212; WordPress' ), $title, $login_title );
	/**
	 * Filters the title tag content for login page.
	 *
	 * @since 4.9.0
	 *
	 * @param string $login_title The page title, with extra context added.
	 * @param string $title       The original page title.
	 */
	$login_title = apply_filters( 'login_title', $login_title, $title );
	?><!DOCTYPE html>
	<head>
	<title><?php echo esc_attr( $login_title ); ?></title>
	<?php
	wp_enqueue_style( 'login' );

	/**
	 * Enqueue scripts and styles for the login page.
	 *
	 * @since 3.1.0
	 */
	do_action( 'login_enqueue_scripts' );

	/**
	 * Fires in the login page header after scripts are enqueued.
	 *
	 * @since 2.1.0
	 */
	do_action( 'login_head' );
	?>
	</head>

	<?php
}

/**
 * Fires before a specified login form action.
 */
do_action( 'login_form_login' );

/**
 * Filters the separator used between login form navigation links.
 */
$login_link_separator = apply_filters( 'login_link_separator', ' | ' );

/**
 * Filters the login page errors.
 *
 * @since 3.6.0
 *
 * @param object $errors      WP Error object.
 * @param string $redirect_to Redirect destination URL.
 */
easyloginstyler_login_header( __( 'Log In' ), '', '' );

$login_header_url   = __( 'https://wordpress.org/' );
$login_header_title = __( 'Powered by WordPress' );

/**
 * Filters link URL of the header logo above login form.
 *
 * @since 2.1.0
 *
 * @param string $login_header_url Login header logo URL.
 */
$login_header_url = apply_filters( 'login_headerurl', $login_header_url );

/**
 * Filters the title attribute of the header logo above login form.
 *
 * @since 2.1.0
 *
 * @param string $login_header_title Login header logo title attribute.
 */
$login_header_title = apply_filters( 'login_headertitle', $login_header_title );

/*
 * To match the URL/title set above, Multisite sites have the blog name,
 * while single sites get the header title.
 */
if ( is_multisite() ) {
	$login_header_text = get_bloginfo( 'name', 'display' );
} else {
	$login_header_text = $login_header_title;
}

$customizer = get_option( 'easy_login_styler_customizer' );

if( empty( $customizer ) ){
	$customizer = array();
}

/**
 * Filters the login page body classes.
 *
 * @since 3.5.0
 *
 * @param array  $classes An array of body classes.
 * @param string $action  The action that brought the visitor to the login page.
 */
$classes   = array( 'login-action-login', 'wp-core-ui' );
$classes[] = ' locale-' . sanitize_html_class( strtolower( str_replace( '_', '-', get_locale() ) ) );
$classes   = apply_filters( 'login_body_class', $classes, 'login' );
?>

	<body class="login <?php echo esc_attr( implode( ' ', $classes ) ); ?>">

		<?php
		/**
		 * Fires in the login page header after the body tag is opened.
		 *
		 * @since 4.6.0
		 */
		do_action( 'login_header' );
		?>

		<div id="login">
			<h1><a href="<?php echo esc_url( $login_header_url ); ?>" title="<?php echo esc_attr( $login_header_title ); ?>" tabindex="-1"><?php echo $login_header_text; ?></a></h1>

			<?php
			unset( $login_header_url, $login_header_title );

			/**
			 * Filters the message to display above the login form.
			 *
			 * @since 2.1.0
			 *
			 * @param string $message Login message text.
			 */
			$message = apply_filters( 'login_message', '' );
			if ( !empty( $message ) )
				echo $message . "\n";

			?>

			<form name="loginform" id="loginform"  action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">

				<p>
					<label for="user_login"><span><?php echo ( isset( $customizer['username'] ) ) ? $customizer['username'] : esc_html__( 'Username or Email Address' ); ?></span><br />
					<input type="text" name="log" id="user_login" class="input" value="<?php echo esc_attr( $user_login ); ?>" size="20" /></label>
				</p>
				<p>
					<label for="user_pass"><span><?php echo ( isset( $customizer['password'] ) ) ? $customizer['password'] : esc_html__( 'Password' ); ?></span><br />
					<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" /></label>
				</p>

				<?php do_action( 'login_form' ); ?>

				<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php echo ( isset( $customizer['remember'] ) ) ? $customizer['remember'] : esc_html__( 'Remember Me' ); ?></span></label></p>

				<p class="submit">
					<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php echo ( isset( $customizer['loginbtn'] ) ) ? $customizer['loginbtn'] : esc_html__('Log In'); ?>" />
				</p>
			</form>

			<p id="nav">
				<?php
				if ( get_option( 'users_can_register' ) ) :
					$registration_url = sprintf( '<a href="%s">%s</a>', esc_url( wp_registration_url() ), ( isset( $customizer['regbtn'] ) ) ? $customizer['regbtn'] : esc_html__( 'Register' ) );
					/** This filter is documented in wp-includes/general-template.php */
					echo apply_filters( 'register', $registration_url );
					echo esc_html( $login_link_separator );
				endif;
				?>
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php echo ( isset( $customizer['lostpass'] ) ) ? $customizer['lostpass'] : esc_html__( 'Lost your password?' ); ?></a>

			</p>

			<p id="backtoblog">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
					/* translators: %s: site title */
					printf( _x( '&larr; Back to %s', 'site' ), esc_html( get_bloginfo( 'title', 'display' ) ) );
					?>
				</a>
			</p>

			<?php the_privacy_policy_link( '<div class="privacy-policy-page-link">', '</div>' ); ?>

		</div>

		<?php do_action( 'login_footer' ); ?>

		<div class="clear"></div>

		<style>
			body svg#sprite,
			body #wp-a11y-speak-polite,
			body #wp-a11y-speak-assertive{
				display:  none;
			}
		</style>

	</body>

</html>

<?php
wp_footer();