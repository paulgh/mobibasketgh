<?php

$wp_customize->add_section(
	'easy_login_styler--background', array(
		'priority' => 10,
		'title' => __( 'Background & Images', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation3]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation3]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--background',
		'settings' => 'easy_login_styler_customizer[need_activation3]',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[images]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Multi_Image_Custom_Control( 
	$wp_customize, 'easy_login_styler_customizer[images]', array(
		'type'     => 'multi-image',
		'section'  => 'easy_login_styler--background',
		'settings' => 'easy_login_styler_customizer[images]',
		'label'    => __( 'Select Background Image/s', 'easy-login-styler' ),
		'description'    => __( 'Hold CTRL/CMD key when choosing images to select multiple background images for slideshow.', 'easy-login-styler' ),
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[imagelinks]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage'
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[imagelinks]', array(
        'settings' => 'easy_login_styler_customizer[imagelinks]',
        'section' => 'easy_login_styler--background',
        'type'    => 'hidden',
));
?>