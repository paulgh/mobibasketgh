<?php

$wp_customize->add_section(
	'easy_login_styler--styling', array(
		'priority' => 10,
		'title' => __( 'Colors and Styling', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation7]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation7]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--styling',
		'settings' => 'easy_login_styler_customizer[need_activation7]',
) ) );

$wp_customize->add_setting( 
	'easy_login_styler_customizer[font]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'default' 	 		=> 'default',
		'transport' 		=> 'postMessage',
	) 
);

$fonts = array(
	'default'             => esc_html__( 'Default', 'easy-login-styler' ),
	'Abril Fatface'       => 'Abril Fatface',
	'Arvo'       		  => 'Arvo',
	'Alegreya'       	  => 'Alegreya',
	'Lato'                => 'Lato',
	'Lora'                => 'Lora',
	'Karla'               => 'Karla',
	'Hind'                => 'Hind',
	'Josefin Sans'        => 'Josefin Sans',
	'Montserrat'          => 'Montserrat',
	'Open Sans'           => 'Open Sans',
	'Open Sans Condensed' => 'Open Sans Condensed',
	'Oswald'              => 'Oswald',
	'Overpass'            => 'Overpass',
	'Poppins'             => 'Poppins',
	'Quicksand'           => 'Quicksand',
	'PT Sans'             => 'PT Sans',
	'PT Sans Narrow'      => 'PT Sans Narrow',
	'Roboto'              => 'Roboto',
	'Fira Sans Condensed' => 'Fira Sans',
	'Frank Ruhl Libre' 	  => 'Frank Ruhl Libre',
	'Nunito'              => 'Nunito',
	'Merriweather'        => 'Merriweather',
	'Rubik'               => 'Rubik',
	'Krub'                => 'Krub',
	'Playfair Display'    => 'Playfair Display',
	'Spectral'            => 'Spectral',
	'Fjalla One'          => 'Fjalla One',
	'Ubuntu'          	  => 'Ubuntu',
	'Asap'          	  => 'Asap',
	'Archivo Narrow'      => 'Archivo Narrow',
	'Encode Sans Condensed' => 'Encode Sans Condensed',
);

$fonts = apply_filters( 'easy_login_styler_fonts', $fonts );

$wp_customize->add_control( 'easy_login_styler_customizer[font]', array(
	'type' 		 => 'select',
	'settings'   => 'easy_login_styler_customizer[font]',
	'section' 	 => 'easy_login_styler--styling',
	'label' 	 => __( 'Font Family', 'easy-login-styler' ),
	'choices' 	 => $fonts,
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[loginbg]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#FFFFFF'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[loginbg]',
  array(
    'label' => __( 'Login Section Background' ),
    'settings'   => 'easy_login_styler_customizer[loginbg]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[txtcolor]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#484848'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[txtcolor]',
  array(
    'label' => __( 'Text and Label Colors', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[txtcolor]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting( 
	'easy_login_styler_customizer[txtfld]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'default' 	 		=> 'boxed',
		'transport' 		=> 'postMessage',
	) 
);

$wp_customize->add_control( 'easy_login_styler_customizer[txtfld]', array(
	'type' => 'select',
	'settings'   => 'easy_login_styler_customizer[txtfld]',
	'section' => 'easy_login_styler--styling',
	'label' => __( 'Input Fields Design', 'easy-login-styler' ),
	'choices' => array(
		'boxed' => __( 'Boxed', 'easy-login-styler' ),
		'linear' => __( 'Linear', 'easy-login-styler' ),
	),
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[txtfldcolor]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#32373c'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[txtfldcolor]',
  array(
    'label' => __( 'Input Fields Text Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[txtfldcolor]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[txtfldbg]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#fbfbfb'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[txtfldbg]',
  array(
    'label' => __( 'Input Fields Background Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[txtfldbg]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[txtfldbr]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#dddddd'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[txtfldbr]',
  array(
    'label' => __( 'Input Fields Border Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[txtfldbr]',
    'section' => 'easy_login_styler--styling',
) ) );


$wp_customize->add_setting( 
	'easy_login_styler_customizer[buttonsize]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'default' 	 		=> 'l',
		'transport' 		=> 'postMessage',
	) 
);

$wp_customize->add_control( 'easy_login_styler_customizer[buttonsize]', array(
	'type' => 'select',
	'settings'   => 'easy_login_styler_customizer[buttonsize]',
	'section' => 'easy_login_styler--styling',
	'label' => __( 'Button Width', 'easy-login-styler' ),
	'choices' => array(
		'sm' => __( 'Small', 'easy-login-styler' ),
		'm' => __( 'Medium', 'easy-login-styler' ),
		'l' => __( 'Large', 'easy-login-styler' ),
	),
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[buttonbg]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#0085ba'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[buttonbg]',
  array(
    'label' => __( 'Button Background', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[buttonbg]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[buttonbr]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#006799'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[buttonbr]',
  array(
    'label' => __( 'Button Border', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[buttonbr]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[buttoncr]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#FFFFFF'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[buttoncr]',
  array(
    'label' => __( 'Button Text Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[buttoncr]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[buttonrd]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 3
	)
);

$wp_customize->add_control( new O2_Customizer_Range_Slider_Control( 
	$wp_customize, 'easy_login_styler_customizer[buttonrd]', array(
		'type'     => 'range-value',
		'section'  => 'easy_login_styler--styling',
		'settings' => 'easy_login_styler_customizer[buttonrd]',
		'label'    => __( 'Button Border Radius', 'easy-login-styler' ),
		'input_attrs' => array(
			'min'    => 0,
			'max'    => 100,
			'step'   => 1,
			'suffix' => 'px',
	  	),
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[noticebg]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#FFFFFF'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[noticebg]',
  array(
    'label' => __( 'Notice Background', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[noticebg]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[noticecolor]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#000000'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[noticecolor]',
  array(
    'label' => __( 'Notice Text Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[noticecolor]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[errorbg]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#FFFFFF'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[errorbg]',
  array(
    'label' => __( 'Error Message Background', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[errorbg]',
    'section' => 'easy_login_styler--styling',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[errorcolor]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => '#000000'
	)
);

$wp_customize->add_control(
  new WP_Customize_Color_Control( $wp_customize, 'easy_login_styler_customizer[noterroror]',
  array(
    'label' => __( 'Error Text Color', 'easy-login-styler' ),
    'settings'   => 'easy_login_styler_customizer[errorcolor]',
    'section' => 'easy_login_styler--styling',
) ) );

?>