<?php

$wp_customize->add_section(
	'easy_login_styler--layouts', array(
		'priority' => 10,
		'title' => __( 'Layout', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation2]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation2]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--layouts',
		'settings' => 'easy_login_styler_customizer[need_activation2]',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[layout]', array(
		'default'           => 'easy-login-default-left',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);



$wp_customize->add_control( new O2_Customizer_Radio_Images_Control(
	$wp_customize, 'easy_login_styler_customizer[layout]', array(
		'label'			=> __( 'Layout', 'easy-login-styler' ),
		'section'		=> 'easy_login_styler--layouts',
		'settings' 		=> 'easy_login_styler_customizer[layout]',
		'choices' 		=> array(
			'easy-login-default-left' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/layouts/left.jpg',
			'easy-login-default-middle' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/layouts/center.jpg',
			'easy-login-default-right' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/layouts/right.jpg',
		),
	)
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[loginwidth]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 420
	)
);

$wp_customize->add_control( new O2_Customizer_Range_Slider_Control( 
	$wp_customize, 'easy_login_styler_customizer[loginwidth]', array(
		'type'     => 'range-value',
		'section'  => 'easy_login_styler--layouts',
		'settings' => 'easy_login_styler_customizer[loginwidth]',
		'label'    => __( 'Login Area Width in Pixels', 'easy-login-styler' ),
		'choices' => array(
			'percent' => false,
		),
		'input_attrs' => array(
			'min'    => 100,
			'max'    => 2000,
			'step'   => 1,
	  	),
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[hideback]', array(
		'default' 			=> false,
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control(
	new O2_Customizer_Toggle_Control(
		$wp_customize, 'easy_login_styler_customizer[hideback]', array(
			'label' 	=> __( 'Hide Back To Blog', 'easy-login-styler' ),
			'section' 	=> 'easy_login_styler--layouts',
			// 'priority' 	=> 35,
			'settings' 	=> 'easy_login_styler_customizer[hideback]',
		)
	)
);

?>