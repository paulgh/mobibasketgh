<?php

$wp_customize->add_section(
	'easy_login_styler--head', array(
		'priority' => 10,
		'title' => __( 'Logo', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation6]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation6]', array(
		'type'     => 'need-activation',
		'priority' 	=> 4,
		'section'  => 'easy_login_styler--head',
		'settings' => 'easy_login_styler_customizer[need_activation6]',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[logoshow]', array(
		'default' 			=> true,
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control(
	new O2_Customizer_Toggle_Control(
		$wp_customize, 'easy_login_styler_customizer[logoshow]', array(
			'label' 	=> __( 'Logo Visibility', 'easy-login-styler' ),
			'section' 	=> 'easy_login_styler--head',
			'priority' 	=> 5,
			'settings' 	=> 'easy_login_styler_customizer[logoshow]',
		)
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[logo]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control(
   new WP_Customize_Image_Control(
       $wp_customize,
       'easy_login_styler_customizer[logo]',
       array(
           'label'      => __( 'Upload your Logo', 'easy-login-styler' ),
           'section'    => 'easy_login_styler--head',
           'settings'   => 'easy_login_styler_customizer[logo]',
           'default'    => esc_url( home_url('/') )
       )
   )
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[url]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => esc_url( home_url('/') )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[url]', array(
        'settings' => 'easy_login_styler_customizer[url]',
        'label'   => __( 'Logo URL', 'easy-login-styler' ),
        'section' => 'easy_login_styler--head',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[logostyle]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 'circular'
	)
);

$wp_customize->add_control( new WP_Customize_Control(
	$wp_customize,
	'easy_login_styler_customizer[logostyle]',
	array(
		'label'      => __( 'Logo Style', 'easy-login-styler' ), 
		'settings'   => 'easy_login_styler_customizer[logostyle]', 
		'section'    => 'easy_login_styler--head', 
		'type'    => 'select',
		'choices' => array(
			'circular' => __( 'Circular', 'easy-login-styler' ),
			'square' => __( 'Square', 'easy-login-styler' ),
			// 'full' => __( 'Use Image Width', 'easy-login-styler' ),
		)
	)
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[logoalign]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 'center'
	)
);

$wp_customize->add_control( new WP_Customize_Control(
	$wp_customize,
	'easy_login_styler_customizer[logoalign]',
	array(
		'label'      => __( 'Logo Alignment', 'easy-login-styler' ), 
		'settings'   => 'easy_login_styler_customizer[logoalign]', 
		'section'    => 'easy_login_styler--head', 
		'type'    => 'select',
		'choices' => array(
			'left' 	 => __( 'Left', 'easy-login-styler' ),
			'center' => __( 'Center', 'easy-login-styler' ),
			'right'  => __( 'Right', 'easy-login-styler' ),
		)
	)
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[logowidth]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 84
	)
);

$wp_customize->add_control( new O2_Customizer_Range_Slider_Control( 
	$wp_customize, 'easy_login_styler_customizer[logowidth]', array(
		'type'     => 'range-value',
		'section'  => 'easy_login_styler--head',
		'settings' => 'easy_login_styler_customizer[logowidth]',
		'label'    => __( 'Width in Pixels', 'easy-login-styler' ),
		'input_attrs' => array(
			'min'    => 1,
			'max'    => 600,
			'step'   => 1,
			'suffix' => 'px',
	  	),
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[logoheight]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => 84
	)
);

$wp_customize->add_control( new O2_Customizer_Range_Slider_Control( 
	$wp_customize, 'easy_login_styler_customizer[logoheight]', array(
		'type'     => 'range-value',
		'section'  => 'easy_login_styler--head',
		'settings' => 'easy_login_styler_customizer[logoheight]',
		'label'    => __( 'Height in Pixels', 'easy-login-styler' ),
		'input_attrs' => array(
			'min'    => 1,
			'max'    => 600,
			'step'   => 1,
			'suffix' => 'px',
	  	),
) ) );

?>