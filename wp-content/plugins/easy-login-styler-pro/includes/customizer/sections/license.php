<?php

$details = get_option( 'easy_login_styler_pro_license_active' );

$activate_hidden 	= '';     
$deactivate_hidden 	= 'hidden';     

if ( is_object( $details ) && 'valid' === $details->license ) {
   $activate_hidden 	= 'hidden';     
   $deactivate_hidden 	= '';  
}

$wp_customize->add_section(
	'easy_login_styler--license', array(
		'priority' => 10,
		'title' => __( 'Activate License', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[license]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[license]', array(
        'settings' 		=> 'easy_login_styler_customizer[license]',
        'label'   		=> __( 'License Code', 'easy-login-styler' ),
        'description'   => __( 'Add a valid license code in order to have access to updates.', 'easy-login-styler' ),
        'section' 		=> 'easy_login_styler--license',
        'type'    		=> 'text',
));
$wp_customize->add_setting(
	'easy_login_styler_customizer[activate]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
    	'default'	=> __( 'Activate License', 'easy-login-styler' ),
	)
);
$wp_customize->add_control( 'easy_login_styler_customizer[activate]', array(
    'type' => 'button',
    'settings' => 'easy_login_styler_customizer[activate]',
    'priority' => 10,
    'section' => 'easy_login_styler--license',
    'input_attrs' => array(
        'class' => 'button button-primary '. $activate_hidden, 
    ),
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[deactivate]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
    	'default'	=> __( 'Deactivate License', 'easy-login-styler' ),
	)
);
$wp_customize->add_control( 'easy_login_styler_customizer[deactivate]', array(
    'type' => 'button',
    'settings' => 'easy_login_styler_customizer[deactivate]',
    'priority' => 10,
    'section' => 'easy_login_styler--license',
    'input_attrs' => array(
        'class' => 'button button-secondary ' . $deactivate_hidden, 
    ),
) );


?>