<?php

$wp_customize->add_section(
	'easy_login_styler--extras', array(
		'priority' => 10,
		'title' => __( 'Redirect and Extras', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation4]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation4]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--extras',
		'settings' => 'easy_login_styler_customizer[need_activation4]',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[redirect]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[redirect]', array(
        'settings' 		=> 'easy_login_styler_customizer[redirect]',
        'label'   		=> __( 'Redirect URL', 'easy-login-styler' ),
        'description'   => __( 'Add link where you want users to be redirected after logged in successfully.', 'easy-login-styler' ),
        'section' 		=> 'easy_login_styler--extras',
        'type'    		=> 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[css]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new WP_Customize_Code_Editor_Control( $wp_customize, 'easy_login_styler_customizer[css]', array(
        'settings' 		=> 'easy_login_styler_customizer[css]',
        'label'   		=> __( 'Custom CSS', 'easy-login-styler' ),
        'description'   => __( 'Add extra styling code that will make your login page better.', 'easy-login-styler' ),
        'section' 		=> 'easy_login_styler--extras',
        'code_type' 	=> 'css',
) ) );

?>