<?php

$wp_customize->add_section(
	'easy_login_styler--labels', array(
		'priority' => 10,
		'title' => __( 'Title and Labels', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation5]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation5]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--labels',
		'settings' => 'easy_login_styler_customizer[need_activation5]',
) ) );

$wp_customize->add_setting( 
	'easy_login_styler_customizer[txtalign]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'default' 	 		=> 'center',
		'transport' 		=> 'postMessage',
	) 
);

$wp_customize->add_control( 'easy_login_styler_customizer[txtalign]', array(
	'type' => 'select',
	'settings'   => 'easy_login_styler_customizer[txtalign]',
	'section' => 'easy_login_styler--labels',
	'label' => __( 'Text Alignment', 'easy-login-styler' ),
	'choices' => array(
		'left' => __( 'Left', 'easy-login-styler' ),
		'center' => __( 'Center', 'easy-login-styler' ),
		'right' => __( 'Right', 'easy-login-styler' ),
	),
) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[title]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Log In', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[title]', array(
        'settings' => 'easy_login_styler_customizer[title]',
        'label'   => __( 'Title', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[desc]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => ''
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[desc]', array(
        'settings' => 'easy_login_styler_customizer[desc]',
        'label'   => __( 'Description', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'textarea',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[username]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Username or Email Address', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[username]', array(
        'settings' => 'easy_login_styler_customizer[username]',
        'label'   => __( 'Username Field Label', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[password]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Password', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[password]', array(
        'settings' => 'easy_login_styler_customizer[password]',
        'label'   => __( 'Password Field Label', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[remember]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Remember Me', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[remember]', array(
        'settings' => 'easy_login_styler_customizer[remember]',
        'label'   => __( 'Remember Checkbox Label', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[loginbtn]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Log In', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[loginbtn]', array(
        'settings' => 'easy_login_styler_customizer[loginbtn]',
        'label'   => __( 'Login Button Text', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[regbtn]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Register', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[regbtn]', array(
        'settings' => 'easy_login_styler_customizer[regbtn]',
        'label'   => __( 'Register Button Text', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[lostpass]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Lost your password?', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[lostpass]', array(
        'settings' => 'easy_login_styler_customizer[lostpass]',
        'label'   => __( 'Lost Password Text', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));

$wp_customize->add_setting(
	'easy_login_styler_customizer[passbtn]', array(
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'           => __( 'Get New Password', 'easy-login-styler' )
	)
);

$wp_customize->add_control(
	'easy_login_styler_customizer[passbtn]', array(
        'settings' => 'easy_login_styler_customizer[passbtn]',
        'label'   => __( 'New Password Button Text', 'easy-login-styler' ),
        'section' => 'easy_login_styler--labels',
        'type'    => 'text',
));
?>