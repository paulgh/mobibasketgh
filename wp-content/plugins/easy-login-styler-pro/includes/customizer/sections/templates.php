<?php

$wp_customize->add_section(
	'easy_login_styler--templates', array(
		'priority' => 10,
		'title' => __( 'Predesigned Template', 'easy-login-styler' ),
		'panel'  => 'easy_login_styler',
	)
);

$wp_customize->add_setting(
	'easy_login_styler_customizer[need_activation]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);

$wp_customize->add_control( new EasyLoginStyler_Activation_Notice_Control( 
	$wp_customize, 'easy_login_styler_customizer[need_activation]', array(
		'type'     => 'need-activation',
		'section'  => 'easy_login_styler--templates',
		'settings' => 'easy_login_styler_customizer[need_activation]',
) ) );

$wp_customize->add_setting(
	'easy_login_styler_customizer[template]', array(
		'default'           => 'default',
		'type'      		=> 'option',
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
	)
);



$wp_customize->add_control( new O2_Customizer_Radio_Images_Control(
	$wp_customize, 'easy_login_styler_customizer[template]', array(
		'label'			=> __( 'Templates', 'easy-login-styler' ),
		'section'		=> 'easy_login_styler--templates',
		'settings' 		=> 'easy_login_styler_customizer[template]',
		'choices' 		=> array(
			'template-default' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/default.jpg',
			'template-1' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/one.jpg',
			'template-2' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/two.jpg',
			'template-3' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/three.jpg',
			'template-9' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/nine.jpg',
			'template-4' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/four.jpg',
			'template-5' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/five.jpg',
			'template-6' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/six.jpg',
			'template-8' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/eight.jpg',
			'template-7' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/seven.jpg',
			'template-10' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/ten.jpg',
			'template-11' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/eleven.jpg',
			'template-12' 		=> EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/images/templates/twelve.jpg',
		),
	)
) );

?>