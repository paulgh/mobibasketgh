<?php
/**
 * Customizer functionality
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Easy_Login_Styler_Customizer' ) ) :

	class Easy_Login_Styler_Customizer{		
		/**
		 * The class constructor.
		 */
		public function __construct() {
			add_action( 'admin_menu', array( $this, 'admin_link' ) );
			add_action( 'admin_init', array( $this, 'customizer_redirect' ) );
			add_action( 'customize_register', array( $this, 'customize_register' ), 11 );
			add_action( 'customize_preview_init', array( $this, 'customize_preview_init' ) );
			add_action( 'customize_controls_print_scripts', array( $this, 'customize_controls_print_scripts' ) );
		}

		function admin_link() {
			add_options_page( esc_html__( 'Easy Login Styler', 'easy-login-styler' ), esc_html__( 'Easy Login Styler', 'easy-login-styler' ), 'manage_options', 'easy-login-styler', '__return_null' );
		}

		function customizer_redirect() {
			if ( isset( $_GET['page'] ) && !empty( $_GET['page'] ) ) { 
				if ( $_GET['page'] === 'easy-login-styler'  ) {
					global $easy_login_styler;

					$url = add_query_arg(
						array(
							'autofocus[section]' => 'easy_login_styler--templates',
							'url' => rawurlencode( esc_url( get_permalink( $easy_login_styler['page'] ) ) ) ,
						),
						admin_url( 'customize.php' )
					);

					wp_safe_redirect( $url );
				}
			}
		}

		function customize_preview_init(){

			global $easy_login_styler;

			wp_enqueue_script( 'customizer-login-styler-preview', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/js/customizer-preview.js',  array( 'jquery', 'customize-preview' ), false, true );

			$localize = $easy_login_styler;
			$localize['permalink'] 		= get_permalink( $easy_login_styler['page'] );
			$localize['sitename'] 		= get_bloginfo( 'name' );
			$localize['ajaxurl'] 		= esc_url( admin_url( 'admin-ajax.php' ) );
			$localize['ajax_action'] 	= 'easy_login_styler_pro_ajax_settings';

			// Localize Script
			wp_localize_script( 'customizer-login-styler-preview', 'easy_login_styler', $localize );
		}

		function customize_controls_print_scripts(){
			global $easy_login_styler;

			wp_enqueue_script( 'customizer-login-styler-js', EASY_LOGIN_STYLER_PRO_PLUGIN_URL . 'assets/js/customizer.js',  array( 'jquery', 'customize-preview'  ), false, true );

			$localize = $easy_login_styler;
			$localize['permalink'] = get_permalink( $easy_login_styler['page'] );
			$localize['sitename'] = get_bloginfo( 'name' );
			$localize['ajaxurl'] 		= esc_url( admin_url( 'admin-ajax.php' ) );
			$localize['ajax_action'] 	= 'easy_login_styler_pro_ajax_settings';
			$localize['activated'] 		= __( 'License Successfully Activated. Please click the Publish Button above to save the license key. Thanks!', 'easy-login-styler' );
			$localize['deactivated'] 	= __( 'License Successfully Deactivated.', 'easy-login-styler' );
			$localize['invalid'] 		= __( 'Please provide valid license code.', 'easy-login-styler' );
			$localize['nonce'] 			= wp_create_nonce( 'easy_login_styler_pro-settings-nonce' );

			// Localize Script
			wp_localize_script( 'customizer-login-styler-js', 'easy_login_styler', $localize );
		}

		function customize_register( $wp_customize ){

			// require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/image-select-control.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/activation-notice/activation-notice.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/multi-image-control.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/radio-images/radio-images-control.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/range-slider/range-slider-control.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/controls/toggle/toggle-control.php';

			//Main Login Styler Panel
			$wp_customize->add_panel(
				'easy_login_styler', array(
					'title'       => esc_html__( 'Easy Login Styler', 'easy-login-styler' ),
					'capability'  => 'edit_theme_options',
					'description' => esc_html__( 'Easily customize admin login page to match your brand.', 'easy-login-styler' ),
					'priority'    => 150,
				)
			);

			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/templates.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/layout.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/logo.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/labels.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/background.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/styling.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/extra.php';

			if( function_exists( 'is_multisite' ) && is_multisite() ){
				$license_data = get_site_option( 'easy_login_styler_pro_license_active' );
				if( !empty( $license_data ) && is_object( $license_data ) && isset( $license_data->license ) && $license_data->license == 'valid' ){
		    		//do nothing
		    	}else{
		    		require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/license.php';
		    	}
			}else{
				require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/sections/license.php';
			}
		}

	}

new Easy_Login_Styler_Customizer();

endif;

?>