<?php
/**
 * Plugin Name: Easy Login Styler Pro
 * Plugin URI: https://easyloginwp.com/
 * Description: The easiest way to match your admin login page with your website's branding.
 * Version: 1.0
 * Author: Phpbits Creative Studio
 * Author URI: https://phpbits.net/
 * Text Domain: easy-login-styler
 * Domain Path: languages
 *
 * @category Login
 * @author Jeffrey Carandang
 * @version 1.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;
if ( ! class_exists( 'Easy_Login_Styler_Pro' ) ) :

/**
 * Main Easy_Login_Styler_Pro Class.
 *
 * @since  1.0
 */
final class Easy_Login_Styler_Pro {
	/**
	 * @var Easy_Login_Styler_Pro The one true Easy_Login_Styler_Pro
	 * @since  1.0
	 */
	private static $instance;

	/**
	 * Main Easy_Login_Styler_Pro Instance.
	 *
	 * Insures that only one instance of Easy_Login_Styler_Pro exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since  1.0
	 * @static
	 * @staticvar array $instance
	 * @uses Easy_Login_Styler_Pro::setup_constants() Setup the constants needed.
	 * @uses Easy_Login_Styler_Pro::includes() Include the required files.
	 * @uses Easy_Login_Styler_Pro::load_textdomain() load the language files.
	 * @see Easy_Login_Styler_Pro()
	 * @return object|Easy_Login_Styler_Pro The one true Easy_Login_Styler_Pro
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Easy_Login_Styler_Pro ) ) {
			self::$instance = new Easy_Login_Styler_Pro;
			self::$instance->setup_constants();

			// add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );

			self::$instance->includes();
			// self::$instance->roles         = new WIDGETOPTS_Roles();
		}
		return self::$instance;
	}

	/**
	 * Setup plugin constants.
	 *
	 * @access private
	 * @since 4.1
	 * @return void
	 */
	private function setup_constants() {

		// Plugin version.
		if ( ! defined( 'EASY_LOGIN_STYLER_PRO_PLUGIN_NAME' ) ) {
			define( 'EASY_LOGIN_STYLER_PRO_PLUGIN_NAME', 'Easy Login Styler Pro' );
		}

		// Plugin version.
		if ( ! defined( 'EASY_LOGIN_STYLER_PRO_VERSION' ) ) {
			define( 'EASY_LOGIN_STYLER_PRO_VERSION', '1.0' );
		}

		// Plugin Folder Path.
		if ( ! defined( 'EASY_LOGIN_STYLER_PRO_PLUGIN_DIR' ) ) {
			define( 'EASY_LOGIN_STYLER_PRO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL.
		if ( ! defined( 'EASY_LOGIN_STYLER_PRO_PLUGIN_URL' ) ) {
			define( 'EASY_LOGIN_STYLER_PRO_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File.
		if ( ! defined( 'EASY_LOGIN_STYLER_PRO_PLUGIN_FILE' ) ) {
			define( 'EASY_LOGIN_STYLER_PRO_PLUGIN_FILE', __FILE__ );
		}
	}

	/**
	 * Include required files.
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function includes() {
		global $easy_login_styler;

		$easy_login_styler = get_option( 'easy_login_styler_pro_settings' );

		require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/login-functions.php';
		require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/template-loader.php';
		if( is_admin() ){
			global $easy_login_styler_license;

			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/install.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/welcome.php';
			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/scripts.php';

			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/class-edd-license-handler.php';
			// auto updater
			if( class_exists( 'Easy_Login_Styler_License' ) ) {
				$easy_login_styler_license = new Easy_Login_Styler_License( EASY_LOGIN_STYLER_PRO_PLUGIN_FILE, EASY_LOGIN_STYLER_PRO_PLUGIN_NAME, EASY_LOGIN_STYLER_PRO_VERSION, 'Phpbits Creative Studio' );
			}

			require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/ajax-functions.php';

			if( function_exists( 'is_multisite' ) && is_multisite() ){
				require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/network/multisite.php';
			}

		}
		require_once EASY_LOGIN_STYLER_PRO_PLUGIN_DIR . 'includes/customizer/customize.php';
	}

}

endif; // End if class_exists check.


/**
 * The main function for that returns Easy_Login_Styler_Pro
 *
 * The main function responsible for returning the one true Easy_Login_Styler_Pro
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $widgetopts = Easy_Login_Styler_Pro(); ?>
 *
 * @since 1.0
 * @return object|Easy_Login_Styler_Pro The one true Easy_Login_Styler_Pro Instance.
 */
if( !function_exists( 'Easy_Login_Styler_PRO_FN' ) ){
	function Easy_Login_Styler_PRO_FN() {
		return Easy_Login_Styler_Pro::instance();
	}
	// Get Plugin Running.
	Easy_Login_Styler_PRO_FN();
}
?>
