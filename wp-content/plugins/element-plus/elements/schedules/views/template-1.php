<?php
/**
 * Element Name: Schedules
 *
 * @package elements/schedules/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id     = uniqid();
$style         = eplus()->get_shortcode_atts( 'style' );
$schedules     = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'schedules' ) );
$img_url       = wp_get_attachment_image_url( eplus()->get_shortcode_atts( 'ss_imgid' ) );
$primary_color = eplus()->get_shortcode_atts( 'primary_color' );
$label_color   = eplus()->get_shortcode_atts( 'label_color' );
$value_color   = eplus()->get_shortcode_atts( 'value_color' );
$icon_color    = eplus()->get_shortcode_atts( 'icon_color' );

?>

<style>
    <?php if ( !empty( $primary_color ) && $style == '3') : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-schedules {
        background-color: <?php echo esc_attr( $primary_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $primary_color ) && $style == '5') : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-schedule {
        background-color: <?php echo esc_attr( $primary_color ); ?>;
        border-color: rgba(255,255,255,0.3);
    }

    <?php endif; ?>

    <?php if ($style == '4' && !empty( $primary_color )) : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-schedules {
        border-color: <?php echo esc_attr( $primary_color ); ?>;
    }

    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-schedules > div + div {
        border-color: <?php echo esc_attr( $primary_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $label_color ) ) : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-day-schedule {
        color: <?php echo esc_attr( $value_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $label_color ) ) : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-day-name {
        color: <?php echo esc_attr( $label_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $icon_color ) ) : ?>
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-arrow-icon:after,
    #eplus-schedules-<?php echo esc_attr( $unique_id ); ?> .eplus-arrow-icon:before {
        background-color: <?php echo esc_attr( $icon_color ); ?>;
    }

    <?php endif; ?>
</style>


<div id="eplus-schedules-<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-schedules-wrap eplus-schedules-style-<?php echo esc_attr( $style ); ?>">
    <div class="eplus-schedules">

		<?php if ( ! empty( $img_url ) ) : ?>
            <div class="eplus-status-img">
                <img src="<?php echo esc_url( $img_url ); ?>"
                     alt="<?php echo esc_attr__( 'Schedules Status', 'element-plus' ) ?>">
            </div>
		<?php endif ?>

		<?php foreach ( $schedules as $schedule ) :
			$sessions = (array) vc_param_group_parse_atts( isset( $schedule['sessions'] ) ? $schedule['sessions'] : '' );
			$day = isset( $schedule['day'] ) ? $schedule['day'] : '';
			?>
            <div class="eplus-schedule  shop-close">
                <div class="eplus-day-name">
					<?php echo esc_html( $day ); ?> <span class="eplus-arrow-icon"></span>
                </div>
                <div class="eplus-day-schedules">
					<?php foreach ( $sessions as $session ) :
						$open = isset( $session['open'] ) ? $session['open'] : '';
						$close = isset( $session['close'] ) ? $session['close'] : '';
						if ( empty( $open ) || empty( $close ) ) {
							continue;
						}
						?>
                        <div class="eplus-day-schedule"><?php echo esc_html( $open . ' - ' . $close ); ?></div>
					<?php endforeach; ?>
                </div>
            </div>
		<?php endforeach; ?>
    </div>
</div>
