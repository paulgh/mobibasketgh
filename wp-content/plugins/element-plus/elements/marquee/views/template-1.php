<?php
/**
 * Element Name: Marquee
 *
 * @package elements/marquee/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id       = uniqid();
$style           = eplus()->get_shortcode_atts( 'style' );
$content_type    = eplus()->get_shortcode_atts( 'content_type', 'by_posts' );
$label           = eplus()->get_shortcode_atts( 'label', esc_html__( 'Latest News', 'element-plus' ) );
$label_color     = eplus()->get_shortcode_atts( 'label_color' );
$text_color      = eplus()->get_shortcode_atts( 'text_color' );
$primary_color   = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color = eplus()->get_shortcode_atts( 'secondary_color' );
$divider_color   = eplus()->get_shortcode_atts( 'divider_color' );
$post_ids        = array_unique( eplus()->get_post_ids( $content_type ) );

?>

<style>
    <?php if ( !empty( $label_color ) ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-label {
        color: <?php echo esc_attr( $label_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $divider_color ) ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-single {
        border-color: <?php echo esc_attr( $divider_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $text_color ) ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-single a {
        color: <?php echo esc_attr( $text_color ); ?>;
    }

    <?php endif; ?>


    <?php if ( !empty( $primary_color ) && !empty( $secondary_color ) && ( $style == '1' || $style == '5' ) ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-label {
        background-color: <?php echo esc_attr($primary_color)?>;
        background-image: linear-gradient(120deg, <?php echo esc_attr($primary_color); ?> 9.6%, <?php echo esc_attr( $secondary_color )?> 96.1%);
    }

    <?php endif; ?>

    <?php if ( !empty( $primary_color ) && !empty( $secondary_color ) &&  $style == '5' ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> {
        background-color: <?php echo esc_attr($primary_color)?>;
        background-image: linear-gradient(30deg, <?php echo esc_attr($primary_color)?> 9.6%, <?php echo esc_attr($secondary_color)?> 96.1%);
    }

    <?php endif; ?>


    <?php if ( !empty( $primary_color ) && (! $style == '5' || $style == '2' || $style == '4' )): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-label {
        background-color: <?php echo esc_attr($primary_color)?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $secondary_color ) && $style == '4'  ): ?>
    #eplus-marquee<?php echo esc_attr( $unique_id ); ?> .eplus-marquee-label {
        border-color: <?php echo esc_attr($secondary_color)?>;
    }

    <?php endif; ?>
</style>


<div id="eplus-marquee<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-marquee eplus-marquee-<?php echo esc_attr( $style ); ?>">
    <span class="eplus-marquee-label"><?php echo esc_html( $label ); ?></span>
    <ul class="eplus-marquee-wrap">
		<?php foreach ( $post_ids as $post_id ) : ?>
            <li class="eplus-marquee-single">
                <a href="<?php echo esc_url( get_the_permalink( $post_id ) ); ?>"><?php echo esc_html( get_the_title( $post_id ) ); ?></a>
            </li>
		<?php endforeach; ?>
    </ul>
</div>

<script>
    jQuery('#eplus-marquee<?php echo esc_attr( $unique_id ); ?>').jConveyorTicker({reverse_elm: true});
</script>

