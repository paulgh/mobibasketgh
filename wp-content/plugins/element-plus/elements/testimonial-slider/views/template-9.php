<?php
/**
 * Element Name: Testimonial Slider
 *
 * @package elements/testimonial-slider/view/template-2
 * @copyright Pluginbazar 2019
 */

$unique_id    = uniqid();
$style        = eplus()->get_shortcode_atts( 'style' );
$testimonials = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'testimonials' ) );

$arrows                 = eplus()->get_shortcode_atts( 'arrows' );
$dots                   = eplus()->get_shortcode_atts( 'dots' );
$autoplay               = eplus()->get_shortcode_atts( 'autoplay' );
$autoplay_speed         = eplus()->get_shortcode_atts( 'autoplay_speed' );
$animation_speed        = eplus()->get_shortcode_atts( 'animation_speed' );
$pause_on_hover         = eplus()->get_shortcode_atts( 'pause_on_hover' );
$gutter                 = eplus()->get_shortcode_atts( 'gutter' );
$display_columns        = eplus()->get_shortcode_atts( 'display_columns' );
$tablet_display_columns = eplus()->get_shortcode_atts( 'tablet_display_columns' );
$mobile_display_columns = eplus()->get_shortcode_atts( 'mobile_display_columns' );
$primary_color          = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color        = eplus()->get_shortcode_atts( 'secondary_color' );


if ( ! empty( $primary_color ) || ! empty( $secondary_color ) ) : ?>
    <style>
        #eplus-testimonial<?php echo esc_attr( $unique_id ); ?> {
        <?php if ( !empty( $primary_color ) ) { ?> background-color: <?php echo esc_attr( $primary_color ); } ?>;
        }

        #eplus-testimonial<?php echo esc_attr( $unique_id ); ?> .eplus-testi-img::before {
        <?php if ( !empty( $secondary_color ) ) { ?> background-color: <?php echo esc_attr( $secondary_color ); } ?>;
        }
        #eplus-testimonial<?php echo esc_attr( $unique_id ); ?> .eplus-testi-rating i {
        <?php if ( !empty( $secondary_color ) ) { ?> color: <?php echo esc_attr( $secondary_color ); } ?>;
        }
        #eplus-testimonial<?php echo esc_attr( $unique_id ); ?> .eplus-testi-title {
        <?php if ( !empty( $secondary_color ) ) { ?> color: <?php echo esc_attr( $secondary_color ); } ?>;
        }
        #eplus-testimonial<?php echo esc_attr( $unique_id ); ?> .eplus-testi-desc p {
        <?php if ( !empty( $secondary_color ) ) { ?> color: <?php echo esc_attr( $secondary_color ); } ?>;
        }
    </style>
<?php endif;

?>


<div class="eplus-testimonial-slider-<?php echo esc_attr( $style ); ?>">

    <div id="eplus-testimonial<?php echo esc_attr( $unique_id ); ?>" class="owl-carousel">

		<?php foreach ( $testimonials as $testimonial ) :

			$img_url = wp_get_attachment_image_url( eplus()->get_shortcode_atts( 'img', '', $testimonial ) );
			$name = eplus()->get_shortcode_atts( 'name', 'Alex Hales', $testimonial );
			$designation = eplus()->get_shortcode_atts( 'designation', 'Alex Hales', $testimonial );
			$review_text = eplus()->get_shortcode_atts( 'review_text', 'Testimonial review text here', $testimonial );
			$rating = eplus()->get_shortcode_atts( 'rating', '', $testimonial );
			$recommend_text = eplus()->get_shortcode_atts( 'recommend_text', '', $testimonial );

			?>

            <div class="eplus-testimonial-item">
				<?php if ( ! empty( $img_url ) ) : ?>
                    <div class="eplus-testi-img">
                        <img src="<?php echo esc_url( $img_url ); ?>" alt="<?php echo esc_attr( $name ); ?>">
                    </div>
				<?php endif; ?>
                <div class="eplus-testi-info">
                    <h3 class="eplus-testi-title"><?php echo esc_html( $name ); ?></h3>
                    <span class="eplus-testi-designation"><?php echo esc_html( $designation ); ?></span>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" width="147" height="140" viewBox="0 0 147 140">
                    <path id="quote"
                          d="M112.887,79.844V68.026H83.325V4.433H147V68.026h0V79.844c0,24-6.913,41.817-20.549,52.966-9.433,7.712-21.373,11.623-35.489,11.623V110.367C98.624,110.367,112.887,110.367,112.887,79.844ZM7.635,110.367v34.066c14.115,0,26.055-3.91,35.488-11.623,13.636-11.149,20.55-28.969,20.55-52.966V68.026h0V4.433H0V68.026H29.563V79.844C29.562,110.367,15.3,110.367,7.635,110.367Z"
                          transform="translate(0 -4.433)" fill="#ffbe55" opacity="0.1"/>
                </svg>
                <div class="eplus-testi-rating">
					<?php
					for ( $index = 1; $index <= 5; $index ++ ) {
						if ( $rating != 0 && $index <= $rating ) {
							echo wp_kses_post( '<i class="fa fa-star"></i>' );
						} else if ( abs( $rating - $index ) === 0.5 ) {
							echo wp_kses_post( '<i class="fa fa-star-half-o"></i>' );
						} else {
							echo wp_kses_post( '<i class="fa fa-star-o"></i>' );
						}
					}
					?>
                </div>


                <div class="eplus-testi-desc">
					<?php if ( ! empty( $review_text ) ) : ?>
                        <p><?php echo esc_html( $review_text ); ?></p>
					<?php endif; ?>
                </div>

            </div>

		<?php endforeach; ?>
    </div>
</div>


<script>
    (function ($) {
        "use strict";

        $(function () {
            $('#eplus-testimonial<?php echo esc_attr( $unique_id ); ?>').owlCarousel({
                loop: <?php echo esc_attr( ! empty( $autoplay ) ? $autoplay : 'false' ); ?>,
                margin: <?php echo esc_attr( ! empty( $gutter ) ? $gutter : '20' ); ?>,
                nav: <?php echo esc_attr( ! empty( $arrows ) ? $arrows : 'false' ); ?>,
                dots: <?php echo esc_attr( ! empty( $dots ) ? $dots : 'false' ); ?>,
                autoplay: <?php echo esc_attr( ! empty( $autoplay ) ? $autoplay : 'false' ); ?>,
                autoplayTimeout: <?php echo esc_attr( ! empty( $autoplay_speed ) ? $autoplay_speed : 2000 ); ?>,
                autoplayHoverPause: <?php echo esc_attr( ! empty( $pause_on_hover ) ? $pause_on_hover : 'false' ); ?>,
                autoHeight: true,
                smartSpeed: <?php echo esc_attr( ! empty( $animation_speed ) ? $animation_speed : 500 ); ?>,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive: {
                    0: {
                        items: <?php echo esc_attr( ! empty( $mobile_display_columns ) ? $mobile_display_columns : 1 ); ?>,
                    },
                    600: {
                        items: <?php echo esc_attr( ! empty( $tablet_display_columns ) ? $tablet_display_columns : 2 ); ?>,
                    },
                    992: {
                        items: <?php echo esc_attr( ! empty( $display_columns ) ? $display_columns : 3 ); ?>,
                    }
                }
            });
        });

    })(jQuery);
</script>