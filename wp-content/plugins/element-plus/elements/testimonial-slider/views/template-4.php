<?php
/**
 * Element Name: Testimonial Slider
 *
 * @package elements/testimonial-slider/view/template-4
 * @copyright Pluginbazar 2019
 */

$unique_id       = uniqid();
$unique_id_2     = uniqid();
$style           = eplus()->get_shortcode_atts( 'style' );
$testimonials    = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'testimonials' ) );
$primary_color   = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color = eplus()->get_shortcode_atts( 'secondary_color' );

$arrows                 = eplus()->get_shortcode_atts( 'arrows' );
$dots                   = eplus()->get_shortcode_atts( 'dots' );
$autoplay               = eplus()->get_shortcode_atts( 'autoplay' );
$autoplay_speed         = eplus()->get_shortcode_atts( 'autoplay_speed' );
$animation_speed        = eplus()->get_shortcode_atts( 'animation_speed' );
$pause_on_hover         = eplus()->get_shortcode_atts( 'pause_on_hover' );
$gutter                 = eplus()->get_shortcode_atts( 'gutter' );
$display_columns        = eplus()->get_shortcode_atts( 'display_columns' );
$tablet_display_columns = eplus()->get_shortcode_atts( 'tablet_display_columns' );
$mobile_display_columns = eplus()->get_shortcode_atts( 'mobile_display_columns' );

?>


<div class="eplus-testimonial-slider-<?php echo esc_attr( $style ); ?>">

    <div id="eplus-testimonial<?php echo esc_attr( $unique_id ); ?>" class="owl-carousel">

		<?php foreach ( $testimonials as $testimonial ) :

			$img_url = wp_get_attachment_image_url( eplus()->get_shortcode_atts( 'img', '', $testimonial ) );
			$name = eplus()->get_shortcode_atts( 'name', 'Alex Hales', $testimonial );
			$designation = eplus()->get_shortcode_atts( 'designation', 'Alex Hales', $testimonial );
			$review_text = eplus()->get_shortcode_atts( 'review_text', 'Testimonial review text here', $testimonial );
			$rating = eplus()->get_shortcode_atts( 'rating', '', $testimonial );

			?>

            <div class="eplus-testimonial-item">

				<?php if ( ! empty( $img_url ) ) : ?>
                    <div class="eplus-testi-img">
                        <img src="<?php echo esc_url( $img_url ); ?>" alt="<?php echo esc_attr( $name ); ?>">
                    </div>
				<?php endif; ?>

                <div class="eplus-testi-desc">
					<?php if ( ! empty( $review_text ) ) : ?>
                        <p><?php echo esc_html( $review_text ); ?></p>
					<?php endif; ?>
                </div>

                <div class="eplus-testi-info">
                    <h3 class="eplus-testi-title"><?php echo esc_html( $name ); ?></h3>
                    <span class="eplus-testi-designation"><?php echo esc_html( $designation ); ?></span>
                    <div class="eplus-testi-rating">
						<?php
						for ( $index = 1; $index <= 5; $index ++ ) {
							if ( $rating != 0 && $index <= $rating ) {
								echo wp_kses_post( '<i class="fa fa-star"></i>' );
							} else if ( abs( $rating - $index ) === 0.5 ) {
								echo wp_kses_post( '<i class="fa fa-star-half-o"></i>' );
							} else {
								echo wp_kses_post( '<i class="fa fa-star-o"></i>' );
							}
						}
						?>
                    </div>
                </div>

                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="184"
                     height="89" viewBox="0 0 184 89">
                    <defs>
                        <linearGradient id="linear-gradient<?php echo esc_attr( $unique_id ); ?>" x1="0.5" x2="0.5"
                                        y2="1"
                                        gradientUnits="objectBoundingBox">
                            <stop offset="0"
                                  stop-color="<?php echo esc_attr( ! empty( $primary_color ) ? $primary_color : '#E91E63' ); ?>"></stop>
                            <stop offset="1"
                                  stop-color="<?php echo esc_attr( ! empty( $secondary_color ) ? $secondary_color : '#5828bb' ); ?>"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient<?php echo esc_attr( $unique_id_2 ); ?>" x1="0.5" x2="0.5"
                                        y2="1"
                                        gradientUnits="objectBoundingBox">
                            <stop offset="0"
                                  stop-color="<?php echo esc_attr( ! empty( $secondary_color ) ? $secondary_color : '#5828bb' ); ?>"></stop>
                            <stop offset="1"
                                  stop-color="<?php echo esc_attr( ! empty( $primary_color ) ? $primary_color : '#E91E63' ); ?>"></stop>
                        </linearGradient>
                    </defs>
                    <g transform="translate(-711 -860)">
                        <path d="M131.091-5.321C81.815-26.421,32.026-14.712.085,2.487s-46.036,39.888,3.24,60.987,76.42.111,108.361-17.088S180.367,15.778,131.091-5.321Z"
                              transform="translate(739.886 876.661)" opacity="0.9"
                              fill="url(#linear-gradient<?php echo esc_attr( $unique_id ); ?>)"></path>
                        <path d="M-4.863-5.321c49.276-21.1,99.064-9.391,131.006,7.808s46.036,39.888-3.24,60.987-76.42.111-108.361-17.088S-54.139,15.778-4.863-5.321Z"
                              transform="translate(739.886 876.661)" opacity="0.8"
                              fill="url(#linear-gradient<?php echo esc_attr( $unique_id_2 ); ?>)"></path>
                    </g>
                </svg>

            </div>

		<?php endforeach; ?>
    </div>
</div>


<script>
    (function ($) {
        "use strict";

        $(function () {
            $('#eplus-testimonial<?php echo esc_attr( $unique_id ); ?>').owlCarousel({
                loop: <?php echo esc_attr( ! empty( $autoplay ) ? $autoplay : 'false' ); ?>,
                margin: <?php echo esc_attr( ! empty( $gutter ) ? $gutter : '20' ); ?>,
                nav: <?php echo esc_attr( ! empty( $arrows ) ? $arrows : 'false' ); ?>,
                dots: <?php echo esc_attr( ! empty( $dots ) ? $dots : 'false' ); ?>,
                autoplay: <?php echo esc_attr( ! empty( $autoplay ) ? $autoplay : 'false' ); ?>,
                autoplayTimeout: <?php echo esc_attr( ! empty( $autoplay_speed ) ? $autoplay_speed : 2000 ); ?>,
                autoplayHoverPause: <?php echo esc_attr( ! empty( $pause_on_hover ) ? $pause_on_hover : 'false' ); ?>,
                autoHeight: true,
                smartSpeed: <?php echo esc_attr( ! empty( $animation_speed ) ? $animation_speed : 500 ); ?>,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive: {
                    0: {
                        items: <?php echo esc_attr( ! empty( $mobile_display_columns ) ? $mobile_display_columns : 1 ); ?>,
                    },
                    600: {
                        items: <?php echo esc_attr( ! empty( $tablet_display_columns ) ? $tablet_display_columns : 2 ); ?>,
                    },
                    992: {
                        items: <?php echo esc_attr( ! empty( $display_columns ) ? $display_columns : 3 ); ?>,
                    }
                }
            });
        });

    })(jQuery);
</script>