<?php
/**
 * Element Name: Dual Button
 *
 * @package elements/dual-button/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id = uniqid();
$style     = eplus()->get_shortcode_atts( 'style' );
$button_1  = vc_build_link( eplus()->get_shortcode_atts( 'btn_1_property' ) );
$button_2  = vc_build_link( eplus()->get_shortcode_atts( 'btn_2_property' ) );

$custom_design = eplus()->get_shortcode_atts( 'custom_design' );

$title_1  = isset( $button_1['title'] ) ? $button_1['title'] : '';
$url_1    = isset( $button_1['url'] ) ? $button_1['url'] : '';
$target_1 = isset( $button_1['target'] ) ? $button_1['target'] : '';
$rel_1    = isset( $button_1['rel'] ) ? $button_1['rel'] : '';

$title_2             = isset( $button_2['title'] ) ? $button_2['title'] : '';
$url_2               = isset( $button_2['url'] ) ? $button_2['url'] : '';
$target_2            = isset( $button_2['target'] ) ? $button_2['target'] : '';
$rel_2               = isset( $button_2['rel'] ) ? $button_2['rel'] : '';

if ( ! empty( $custom_design ) ) :

	$btn1_primary_color = eplus()->get_shortcode_atts( 'btn1_primary_color' );
	$btn1_secondary_color = eplus()->get_shortcode_atts( 'btn1_secondary_color' );
	$btn1_color           = eplus()->get_shortcode_atts( 'btn1_color' );
	$btn1_hover_color     = eplus()->get_shortcode_atts( 'btn1_hover_color' );
	$btn1_margin          = eplus()->get_shortcode_atts( 'btn1_margin' );
	$btn1_min_width       = eplus()->get_shortcode_atts( 'btn1_min_width' );
	$btn1_padding         = eplus()->get_shortcode_atts( 'btn1_padding' );
	$btn1_font_size       = eplus()->get_shortcode_atts( 'btn1_font_size' );
	$btn1_radius          = eplus()->get_shortcode_atts( 'btn1_radius' );
	$btn1_stroke          = eplus()->get_shortcode_atts( 'btn1_stroke' );


	$btn2_primary_color = eplus()->get_shortcode_atts( 'btn2_primary_color' );
	$btn2_secondary_color = eplus()->get_shortcode_atts( 'btn2_secondary_color' );
	$btn2_color           = eplus()->get_shortcode_atts( 'btn2_color' );
	$btn2_hover_color     = eplus()->get_shortcode_atts( 'btn2_hover_color' );
	$btn2_margin          = eplus()->get_shortcode_atts( 'btn2_margin' );
	$btn2_min_width       = eplus()->get_shortcode_atts( 'btn2_min_width' );
	$btn2_padding         = eplus()->get_shortcode_atts( 'btn2_padding' );
	$btn2_font_size       = eplus()->get_shortcode_atts( 'btn2_font_size' );
	$btn2_radius          = eplus()->get_shortcode_atts( 'btn2_radius' );
	$btn2_stroke          = eplus()->get_shortcode_atts( 'btn2_stroke' );

	?>

    <style>

        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-1 {
        <?php if ( !empty( $btn1_radius ) ) { ?> border-radius: <?php echo esc_attr( $btn1_radius ); } ?>;
        <?php if ( !empty( $btn1_min_width ) ) { ?> min-width: <?php echo esc_attr( $btn1_min_width . 'px' ); } ?>;
        <?php if ( !empty( $btn1_padding ) ) { ?> padding: <?php echo esc_attr( $btn1_padding ); } ?>;
        <?php if ( !empty( $btn1_margin ) ) { ?> margin: <?php echo esc_attr( $btn1_margin ); } ?>;
        <?php if ( !empty( $btn1_font_size ) ) { ?> font-size: <?php echo esc_attr( $btn1_font_size ); } ?>;
        }

        <?php if( empty( $btn1_stroke ) ) : ?>
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-1 {
        <?php if ( !empty( $btn1_primary_color ) ) { ?> background-color: <?php echo esc_attr( $btn1_primary_color ); } ?>;
        <?php if ( !empty( $btn1_color ) ) { ?> color: <?php echo esc_attr( $btn1_color ); } ?>;
        }

        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-1:hover,
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-1:focus {
        <?php if ( !empty( $btn1_hover_color ) ) { ?> color: <?php echo esc_attr( $btn1_hover_color ); } ?>;
        <?php if ( !empty( $btn1_secondary_color ) ) { ?> background-color: <?php echo esc_attr( $btn1_secondary_color ); } ?>;
        }

        <?php endif; ?>

        <?php if( !empty( $btn1_stroke ) ) : ?>
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-1 {
        <?php if ( !empty( $btn1_primary_color ) ) { ?> border-color: <?php echo esc_attr( $btn1_primary_color ); } ?>;
        <?php if ( !empty( $btn1_color ) ) { ?> color: <?php echo esc_attr( $btn1_color ); } ?>;
        }

        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?>  .dual-btn-1:hover,
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?>  .dual-btn-1:focus {
        <?php if ( !empty( $btn1_hover_color ) ) { ?> color: <?php echo esc_attr( $btn1_hover_color ); } ?>;
        <?php if ( !empty( $btn1_secondary_color ) ) { ?> background-color: <?php echo esc_attr( $btn1_secondary_color ); } ?>;
        <?php if ( !empty( $btn1_secondary_color ) ) { ?> border-color: <?php echo esc_attr( $btn1_secondary_color ); } ?>;
        }
        <?php endif; ?>


        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-2 {
        <?php if ( !empty( $btn2_radius ) ) { ?> border-radius: <?php echo esc_attr( $btn2_radius ); } ?>;
        <?php if ( !empty( $btn2_min_width ) ) { ?> min-width: <?php echo esc_attr( $btn2_min_width . 'px' ); } ?>;
        <?php if ( !empty( $btn2_padding ) ) { ?> padding: <?php echo esc_attr( $btn2_padding ); } ?>;
        <?php if ( !empty( $btn2_margin ) ) { ?> margin: <?php echo esc_attr( $btn2_margin ); } ?>;
        <?php if ( !empty( $btn2_font_size ) ) { ?> font-size: <?php echo esc_attr( $btn2_font_size ); } ?>;
        }

        <?php if( empty( $btn2_stroke ) ) : ?>
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-2 {
        <?php if ( !empty( $btn2_primary_color ) ) { ?> background-color: <?php echo esc_attr( $btn2_primary_color ); } ?>;
        <?php if ( !empty( $btn2_color ) ) { ?> color: <?php echo esc_attr( $btn2_color ); } ?>;
        }

        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-2:hover,
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-2:focus {
        <?php if ( !empty( $btn2_hover_color ) ) { ?> color: <?php echo esc_attr( $btn2_hover_color ); } ?>;
        <?php if ( !empty( $btn2_secondary_color ) ) { ?> background-color: <?php echo esc_attr( $btn2_secondary_color ); } ?>;
        }

        <?php endif; ?>

        <?php if( !empty( $btn2_stroke ) ) : ?>
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?> .dual-btn-2 {
        <?php if ( !empty( $btn2_primary_color ) ) { ?> border-color: <?php echo esc_attr( $btn2_primary_color ); } ?>;
        <?php if ( !empty( $btn2_color ) ) { ?> color: <?php echo esc_attr( $btn2_color ); } ?>;
        }

        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?>  .dual-btn-2:hover,
        #eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?>  .dual-btn-2:focus {
        <?php if ( !empty( $btn2_hover_color ) ) { ?> color: <?php echo esc_attr( $btn2_hover_color ); } ?>;
        <?php if ( !empty( $btn2_secondary_color ) ) { ?> background-color: <?php echo esc_attr( $btn2_secondary_color ); } ?>;
        <?php if ( !empty( $btn2_secondary_color ) ) { ?> border-color: <?php echo esc_attr( $btn2_secondary_color ); } ?>;
        }
        <?php endif; ?>

    </style>
<?php endif;
?>

<div id="eplus-dual-btn-<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-dual-button-<?php echo esc_attr( $style ); ?>">
    <a href="<?php echo esc_url( $url_1 ); ?>"
       target="<?php echo esc_attr( trim( $target_1, ' ' ) ); ?>"
       rel="<?php echo esc_attr( $rel_1 ); ?>"
       class="dual-btn-1 <?php echo esc_attr( ! empty( $btn1_stroke ) ? 'eplus-dual-btn-stroke' : '' ); ?>">
		<?php echo esc_html( $title_1 ); ?>
    </a>

    <a href="<?php echo esc_url( $url_2 ); ?>"
       target="<?php echo esc_attr( trim( $target_2, ' ' ) ); ?>"
       rel="<?php echo esc_attr( $rel_2 ); ?>"
       class="dual-btn-2 <?php echo esc_attr( ! empty( $btn2_stroke ) ? 'eplus-dual-btn-stroke' : '' ); ?>">
		<?php echo esc_html( $title_2 ); ?>
    </a>
</div>
