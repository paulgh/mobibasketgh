<?php
/**
 * Element Name: Dual Button
 *
 * @class EPLUS_Element_dual_button
 *
 * @package elements/dual-button
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_dual_button extends EPLUS_Element {

	function __construct( $id, $element_name = '', $element = array()  ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus dual button', 'element-plus' ),
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {
		return array(
			array(
				'type'       => 'vc_link',
				'heading'    => esc_html__( 'Button 1 Property', 'element-plus' ),
				'param_name' => 'btn_1_property',
			),
			array(
				'type'       => 'vc_link',
				'heading'    => esc_html__( 'Button 2 Property', 'element-plus' ),
				'param_name' => 'btn_2_property',
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'custom_design',
				'description' => esc_html__( 'If checked, Custom design option will be available on design tab.', 'element-plus' ),
				'value'       => array( esc_html__( 'Custom Design', 'element-plus' ) => 'yes' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Padding', 'element-plus' ),
				'param_name'  => 'btn1_padding',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Margin', 'element-plus' ),
				'param_name'  => 'btn1_margin',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'range_slider',
				'heading'    => esc_html__( 'Min Width', 'element-plus' ),
				'param_name' => 'btn1_min_width',
				'min'        => 80,
				'max'        => 320,
				'value'      => 90,
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Font Size', 'element-plus' ),
				'param_name' => 'btn1_font_size',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'description' => esc_html__( 'Example: 10px Default: 0', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Radius', 'element-plus' ),
				'param_name' => 'btn1_radius',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'btn1_stroke',
				'value'       => array( esc_html__( 'Stroke Button', 'element-plus' ) => 'yes' ),
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Color', 'element-plus' ),
				'param_name' => 'btn1_color',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Hover Color', 'element-plus' ),
				'param_name' => 'btn1_hover_color',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Primary Color', 'element-plus' ),
				'param_name' => 'btn1_primary_color',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Secondary Color', 'element-plus' ),
				'param_name' => 'btn1_secondary_color',
				'group'      => esc_html__( 'Button 1 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),

			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Padding', 'element-plus' ),
				'param_name'  => 'btn2_padding',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Margin', 'element-plus' ),
				'param_name'  => 'btn2_margin',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'range_slider',
				'heading'    => esc_html__( 'Min Width', 'element-plus' ),
				'param_name' => 'btn2_min_width',
				'min'        => 80,
				'max'        => 320,
				'value'      => 90,
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Font Size', 'element-plus' ),
				'param_name' => 'btn2_font_size',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'description' => esc_html__( 'Example: 10px Default: 0', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Radius', 'element-plus' ),
				'param_name' => 'btn2_radius',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'btn2_stroke',
				'value'       => array( esc_html__( 'Stroke Button', 'element-plus' ) => 'yes' ),
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Color', 'element-plus' ),
				'param_name' => 'btn2_color',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Hover Color', 'element-plus' ),
				'param_name' => 'btn2_hover_color',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Primary Color', 'element-plus' ),
				'param_name' => 'btn2_primary_color',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Secondary Color', 'element-plus' ),
				'param_name' => 'btn2_secondary_color',
				'group'      => esc_html__( 'Button 2 Design', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
		);
	}
}