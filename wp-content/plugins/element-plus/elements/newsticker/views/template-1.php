<?php
/**
 * Element Name: Newsticker
 *
 * @package elements/newsticker/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id       = uniqid();
$style           = eplus()->get_shortcode_atts( 'style' );
$content_type    = eplus()->get_shortcode_atts( 'content_type', 'by_posts' );
$label           = eplus()->get_shortcode_atts( 'label', esc_html__( 'Latest News', 'element-plus' ) );
$post_ids        = array();
$label_color     = eplus()->get_shortcode_atts( 'label_color' );
$text_color      = eplus()->get_shortcode_atts( 'text_color' );
$primary_color   = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color = eplus()->get_shortcode_atts( 'secondary_color' );
$divider_color   = eplus()->get_shortcode_atts( 'divider_color' );
$post_ids        = array_unique( eplus()->get_post_ids( $content_type ) );
?>

<style>
    <?php if ( !empty( $label_color ) ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-label {
        color: <?php echo esc_attr( $label_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $divider_color ) ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-single {
        border-color: <?php echo esc_attr( $divider_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $text_color ) ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-single a {
        color: <?php echo esc_attr( $text_color ); ?>;
    }

    <?php endif; ?>


    <?php if ( !empty( $primary_color ) && !empty( $secondary_color ) && ( $style == '1' || $style == '5' ) ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-label {
        background-color: <?php echo esc_attr($primary_color)?>;
        background-image: linear-gradient(120deg, <?php echo esc_attr($primary_color); ?> 9.6%, <?php echo esc_attr( $secondary_color )?> 96.1%);
    }

    <?php endif; ?>

    <?php if ( !empty( $primary_color ) && !empty( $secondary_color ) &&  $style == '5' ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> {
        background-color: <?php echo esc_attr($primary_color)?>;
        background-image: linear-gradient(30deg, <?php echo esc_attr($primary_color)?> 9.6%, <?php echo esc_attr($secondary_color)?> 96.1%);
    }

    <?php endif; ?>


    <?php if ( !empty( $primary_color ) && (! $style == '5' || $style == '2' || $style == '4' )): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-label {
        background-color: <?php echo esc_attr($primary_color)?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $secondary_color ) && $style == '4'  ): ?>
    #eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .eplus-newsticker-label {
        border-color: <?php echo esc_attr($secondary_color)?>;
    }

    <?php endif; ?>
</style>

<div id="eplus-newsticker<?php echo esc_attr( $unique_id ); ?>" class="eplus-newsticker eplus-newsticker-<?php echo esc_attr( $style ); ?>">
    <span class="eplus-newsticker-label"><?php echo esc_html( $label ); ?></span>
    <div class="eplus-newsticker-wrap">
        <div class="owl-carousel">
			<?php foreach ( $post_ids as $post_id ) : ?>
                <h3 class="eplus-newsticker-single">
                    <a href="<?php echo esc_url( get_the_permalink( $post_id ) ); ?>"><?php echo esc_html( get_the_title( $post_id ) ); ?></a>
                </h3>
			<?php endforeach; ?>
        </div>
    </div>
</div>

<script>
    (function ($) {
        "use strict";

        $(function () {
            $('#eplus-newsticker<?php echo esc_attr( $unique_id ); ?> .owl-carousel').owlCarousel({
                animateOut: 'fadeOut',
                animateIn: 'fadeInUp',
                loop: true,
                margin: 10,
                nav: false,
                dots: false,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                items: 1,
                smartSpeed: 1000,
            });
        });

    })(jQuery);
</script>
