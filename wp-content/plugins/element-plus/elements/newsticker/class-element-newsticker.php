<?php
/**
 * Element Name: Newsticker
 *
 * @class EPLUS_Element_newsticker
 *
 * @package elements/accordion
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_newsticker extends EPLUS_Element {

	/**
	 * EPLUS_Element_newsticker constructor.
	 *
	 * @param $id
	 * @param string $element_name
	 */
	function __construct( $id, $element_name = '', $element = array()  ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus Newsticker', 'element-plus' ),
			'style_variations'        => 5,
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {
		$this_params = array(
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Label Color', 'element-plus' ),
				'param_name' => 'label_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Marquee Text Color', 'element-plus' ),
				'param_name' => 'text_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Divider Color', 'element-plus' ),
				'param_name' => 'divider_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Primary Color', 'element-plus' ),
				'param_name' => 'primary_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Secondary Color', 'element-plus' ),
				'param_name' => 'secondary_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency'      => array(
					'element' => 'style',
					'value' => array( '1', '4', '5' )
				)
			),
		);

		return array_merge( eplus()->get_posts_params(), $this_params );
	}
}