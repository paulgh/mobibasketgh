<?php
/**
 * Element Name: Contact
 *
 * @class EPLUS_Element_contact
 *
 * @package elements/contact
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_contact extends EPLUS_Element {

	/**
	 * EPLUS_Element_contact constructor.
	 *
	 * @param $id
	 * @param string $element_name
	 * @param array $element
	 */
	function __construct( $id, $element_name = '', $element = array() ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus contact us', 'element-plus' ),
			'style_variations'        => 1,
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {

		return array(

			array(
				'type'       => 'select2',
				'heading'    => esc_html__( 'Select Contact', 'element-plus' ),
				'param_name' => 'contact_id',
				'value'      => eplus()->get_wpcf7(),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'custom_design',
				'description' => esc_html__( 'If checked, Custom design option will be available on design tab.', 'element-plus' ),
				'value'       => array( esc_html__( 'Custom Design', 'element-plus' ) => 'yes' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Margin', 'element-plus' ),
				'param_name'  => 'label_margin',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: 0 0 0 0', 'element-plus' ),
				'group'       => esc_html__( 'Label', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Font Size', 'element-plus' ),
				'param_name'  => 'label_font_size',
				'group'       => esc_html__( 'Label', 'element-plus' ),
				'description' => esc_html__( 'Example: 16px Default: inherit', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Color', 'element-plus' ),
				'param_name' => 'label_color',
				'group'      => esc_html__( 'Label', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Padding', 'element-plus' ),
				'param_name'  => 'input_padding',
				'description' => esc_html__( 'Format: Top Right Bottom Left. Example: 10px 15px 10px 15px | Default: inherit from theme', 'element-plus' ),
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Font Size', 'element-plus' ),
				'param_name'  => 'input_font_size',
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Example: 16px Default: inherit', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'range_slider',
				'heading'     => esc_html__( 'Width (%)', 'element-plus' ),
				'param_name'  => 'input_width',
				'min'         => 0,
				'max'         => 100,
				'value'       => 100,
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Set input field width, applied value in % unit.', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'range_slider',
				'heading'     => esc_html__( 'Input Height (px)', 'element-plus' ),
				'param_name'  => 'input_height',
				'min'         => 0,
				'max'         => 200,
				'value'       => 52,
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Set input field height | applied value in px unit.', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'range_slider',
				'heading'     => esc_html__( 'Textarea Height (px)', 'element-plus' ),
				'param_name'  => 'textarea_height',
				'min'         => 0,
				'max'         => 500,
				'value'       => 70,
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Set textarea field height, applied value in px unit.', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'range_slider',
				'heading'     => esc_html__( 'Margin Bottom all Inputs (px)', 'element-plus' ),
				'param_name'  => 'input_margin_bottom',
				'min'         => 0,
				'max'         => 200,
				'value'       => 20,
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Set bottom margin for all input fields, applied value in px unit.', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Border', 'element-plus' ),
				'param_name'  => 'input_border',
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Example: 1px solid #eee | Default: inherit from theme', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Box Shadow', 'element-plus' ),
				'param_name'  => 'input_box_shadow',
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Example: 0px 13px 50px 0px rgba(0, 0, 0, 0.15)', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Border Radius', 'element-plus' ),
				'param_name'  => 'input_radius',
				'group'       => esc_html__( 'Input Fields', 'element-plus' ),
				'description' => esc_html__( 'Example: top, right, bottom, left | Default: 4px', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Background', 'element-plus' ),
				'param_name' => 'input_bg',
				'group'      => esc_html__( 'Input Fields', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Text Color', 'element-plus' ),
				'param_name' => 'input_color',
				'group'      => esc_html__( 'Input Fields', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Placeholder Color', 'element-plus' ),
				'param_name' => 'placeholder_color',
				'group'      => esc_html__( 'Input Fields', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Hint Color', 'element-plus' ),
				'param_name' => 'hint_color',
				'group'      => esc_html__( 'Input Fields', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'btn_align',
				'heading'    => esc_html__( 'Alignment', 'element-plus' ),
				'group'      => esc_html__( 'Button', 'element-plus' ),
				'value'      => array(
					esc_html__( 'Left' )   => 'left',
					esc_html__( 'Center' ) => 'center',
					esc_html__( 'Right' )  => 'right',
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Width', 'element-plus' ),
				'param_name'  => 'btn_width',
				'group'       => esc_html__( 'Button', 'element-plus' ),
				'description' => esc_html__( 'Enter button width, Example: 200px | Supported units (px, %, em, rem)', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Border Radius', 'element-plus' ),
				'param_name'  => 'btn_radius',
				'group'       => esc_html__( 'Button', 'element-plus' ),
				'description' => esc_html__( 'Example: 4px 4px 4px 4px | Supported units (px, %, em, rem)', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'        => 'range_slider',
				'heading'     => esc_html__( 'Height (px)', 'element-plus' ),
				'param_name'  => 'btn_height',
				'min'         => 0,
				'max'         => 200,
				'value'       => 52,
				'group'       => esc_html__( 'Button', 'element-plus' ),
				'description' => esc_html__( 'Enter button height | applied value in px unit.', 'element-plus' ),
				'dependency'  => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Color', 'element-plus' ),
				'param_name' => 'btn_color',
				'group'      => esc_html__( 'Button', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Hover Color', 'element-plus' ),
				'param_name' => 'btn_hover_color',
				'group'      => esc_html__( 'Button', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Background Color', 'element-plus' ),
				'param_name' => 'btn_bg_color',
				'group'      => esc_html__( 'Button', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Hover Background Color', 'element-plus' ),
				'param_name' => 'btn_bg_hover_color',
				'group'      => esc_html__( 'Button', 'element-plus' ),
				'dependency' => array(
					'element'   => 'custom_design',
					'not_empty' => true,
				)
			),

		);
	}
}