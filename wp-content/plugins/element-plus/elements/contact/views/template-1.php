<?php
/**
 * Element Name: Contact us
 *
 * @package elements/post-grid/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id  = uniqid();
$style      = eplus()->get_shortcode_atts( 'style' );
$contact_id = eplus()->get_shortcode_atts( 'contact_id' );

$custom_design       = eplus()->get_shortcode_atts( 'custom_design' );

if ( ! empty( $custom_design ) ) :

	$label_margin = eplus()->get_shortcode_atts( 'label_margin' );
	$label_font_size = eplus()->get_shortcode_atts( 'label_font_size' );
	$label_color     = eplus()->get_shortcode_atts( 'label_color' );

	$input_padding       = eplus()->get_shortcode_atts( 'input_padding' );
	$input_font_size     = eplus()->get_shortcode_atts( 'input_font_size' );
	$input_width         = eplus()->get_shortcode_atts( 'input_width' );
	$input_height        = eplus()->get_shortcode_atts( 'input_height' );
	$input_border        = eplus()->get_shortcode_atts( 'input_border' );
	$input_box_shadow    = eplus()->get_shortcode_atts( 'input_box_shadow' );
	$input_radius        = eplus()->get_shortcode_atts( 'input_radius' );
	$input_color         = eplus()->get_shortcode_atts( 'input_color' );
	$input_bg            = eplus()->get_shortcode_atts( 'input_bg' );
	$hint_color          = eplus()->get_shortcode_atts( 'hint_color' );
	$input_margin_bottom = eplus()->get_shortcode_atts( 'input_margin_bottom' );

	$placeholder_color = eplus()->get_shortcode_atts( 'placeholder_color' );
	$textarea_height   = eplus()->get_shortcode_atts( 'textarea_height' );

	$btn_align    = eplus()->get_shortcode_atts( 'btn_align', 'left' );
	$btn_width    = eplus()->get_shortcode_atts( 'btn_width' );
	$btn_height   = eplus()->get_shortcode_atts( 'btn_height' );
	$btn_color    = eplus()->get_shortcode_atts( 'btn_color' );
	$btn_bg_color = eplus()->get_shortcode_atts( 'btn_bg_color' );
	$btn_radius   = eplus()->get_shortcode_atts( 'btn_radius' );

	$btn_hover_color    = eplus()->get_shortcode_atts( 'btn_hover_color' );
	$btn_bg_hover_color = eplus()->get_shortcode_atts( 'btn_bg_hover_color' );

	?>

    <style>

        <?php if ( !empty( $input_margin_bottom ) ) : ?>
        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap {
            margin-bottom: <?php echo esc_attr( $input_margin_bottom . 'px' ); ?>;
        }

        <?php endif; ?>

        <?php if ( !empty( $label_margin ) || !empty( $label_font_size ) || !empty( $label_color ) ) : ?>
        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> form label, #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-list-item-label {
            margin: <?php echo esc_attr( $label_margin ); ?>;
            font-size: <?php echo esc_attr( $label_font_size ); ?>;
            color: <?php echo esc_attr( $label_color ); ?>;
        }

        <?php endif; ?>

        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap input:not([type="submit"]):not([type="checkbox"]):not([type="radio"]) {
        <?php if ( !empty( $input_height ) ) { ?> height: <?php echo esc_attr( $input_height . 'px' ); } ?>;
        }

        <?php if( !empty( $textarea_height ) ) : ?>
        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap textarea {
            height: <?php echo esc_attr( $textarea_height ) ?>;
        }

        <?php endif; ?>

        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap input:not([type="submit"]):not([type="checkbox"]):not([type="radio"]), #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap textarea, #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap select {
        <?php if ( !empty( $input_padding ) ) { ?> padding: <?php echo esc_attr( $input_padding ); } ?>;
        <?php if ( !empty( $input_font_size ) ) { ?> font-size: <?php echo esc_attr( $input_font_size ); } ?>;
        <?php if ( !empty( $input_width ) ) { ?> width: <?php echo esc_attr( $input_width . '%'); } ?>;
        <?php if ( !empty( $input_border ) ) { ?> border: <?php echo esc_attr( $input_border ); } ?>;
        <?php if ( !empty( $input_box_shadow ) ) { ?> box-shadow: <?php echo esc_attr( $input_box_shadow ); } ?>;
        <?php if ( !empty( $input_radius ) ) { ?> border-radius: <?php echo esc_attr( $input_radius ); } ?>;
        <?php if ( !empty( $input_color ) ) { ?> color: <?php echo esc_attr( $input_color ); } ?>;
        <?php if ( !empty( $input_bg ) ) { ?> background-color: <?php echo esc_attr( $input_bg ); } ?>;
        }

        <?php if ( !empty( $placeholder_color ) ) : ?>
        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-form-control-wrap input::placeholder {
            color: <?php echo esc_attr( $placeholder_color ); ?>;
        }

        <?php endif; ?>

        <?php if ( !empty( $hint_color ) ) : ?>
        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-not-valid-tip {
            color: <?php echo esc_attr( $hint_color ); ?>;
        }

        <?php endif; ?>

        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-submit {
        <?php if ( !empty( $btn_color ) ) { ?> color: <?php echo esc_attr( $btn_color ); } ?>;
        <?php if ( !empty( $btn_bg_color ) ) { ?> background-color: <?php echo esc_attr( $btn_bg_color ); } ?>;
        <?php if ( !empty( $btn_width ) ) { ?> width: <?php echo esc_attr( $btn_width ); } ?>;
        <?php if ( !empty( $btn_height ) ) { ?> height: <?php echo esc_attr( $btn_height . 'px'); } ?>;
        <?php if ( !empty( $btn_radius ) ) { ?> border-radius: <?php echo esc_attr( $btn_radius); } ?>;
        }

        #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-submit:hover, #eplus-cf7-<?php echo esc_attr( $unique_id ); ?> .wpcf7-submit:focus {
        <?php if ( !empty( $btn_hover_color ) ) { ?> color: <?php echo esc_attr( $btn_hover_color ); } ?>;
        <?php if ( !empty( $btn_bg_hover_color ) ) { ?> background-color: <?php echo esc_attr( $btn_bg_hover_color ); } ?>;
        }

    </style>
<?php endif;

printf( '<div id="eplus-cf7-%s" class="eplus-cf7-wrapper eplus-cf7-%s">%s</div>', $unique_id, $btn_align, do_shortcode( '[contact-form-7 id="' . $contact_id . '"]' ) );
