<?php
/**
 * Element Name: Info Box
 *
 * @class EPLUS_Element_info_box
 *
 * @package elements/info_box
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_info_box extends EPLUS_Element {

	function __construct( $id, $element_name = '', $element = array() ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus info box', 'element-plus' ),
			'style_variations'        => 13,
			'views'                   => array(
				'13' => 'template-1',
			),
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {
		return array(
			array(
				'type'        => 'checkbox',
				'param_name'  => 'featured',
				'description' => esc_html__( 'If checked, info box will have featured.', 'element-plus' ),
				'value'       => array( esc_html__( 'Featured', 'element-plus' ) => 'no' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '2', '8' ),
				),
			),
			array(
				'type' => 'iconlibrary',
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Count Text', 'element-plus' ),
				'param_name' => 'info_count',
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '6' ),
				),
			),
			array(
				'param_name'  => 'title',
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Title', 'element-plus' ),
				'admin_label' => true,
				'description' => esc_html__( 'Info box title here.', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12', '13'),
				),
			),
			array(
				'type'        => 'textarea',
				'heading'     => esc_html__( 'Short Description', 'element-plus' ),
				'param_name'  => 'short_desc',
				'admin_label' => false,
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12', '13'),
				),
			),
			array(
				'type'        => 'vc_link',
				'heading'     => esc_html__( 'URL (Link)', 'element-plus' ),
				'param_name'  => 'url',
				'description' => esc_html__( 'Add custom link.', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Icon Color', 'element-plus' ),
				'param_name' => 'icon_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Icon Background', 'element-plus' ),
				'param_name' => 'icon_bg',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '1', '2' ),
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Title Color', 'element-plus' ),
				'param_name' => 'title_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Description Color', 'element-plus' ),
				'param_name' => 'desc_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Button Color', 'element-plus' ),
				'param_name' => 'btn_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Button Background', 'element-plus' ),
				'param_name' => 'btn_bg',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '1', '2', '4', '5', '6', '7', '8', '12', '13'),
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Button Hover Color', 'element-plus' ),
				'param_name' => 'btn_hover_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '1', '2', '4', '5', '6', '7', '8' ),
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Button Hover Background', 'element-plus' ),
				'param_name' => 'btn_hover_bg',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '1', '2', '4', '5', '6', '7', '8' ),
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Primary Color', 'element-plus' ),
				'param_name' => 'primary_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '3', '4', '5', '6', '8', '9', '10', '12', '13' ),
				),
			),
			array(
				'type'       => 'colorpicker',
				'heading'    => esc_html__( 'Secondary Color', 'element-plus' ),
				'param_name' => 'secondary_color',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '4', '5', '6', '8', '9', '10', '12', '13' ),
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Box Shadow', 'element-plus' ),
				'param_name' => 'box_shadow',
				'group'      => esc_html__( 'Design', 'element-plus' ),
				'description'      => esc_html__( 'Enter box shadow value here. Example: 0 5px 40px rgba(0, 0, 0, 0.05)', 'element-plus' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( '4', '8', '12', '13' ),
				),
			),
		);
	}
}