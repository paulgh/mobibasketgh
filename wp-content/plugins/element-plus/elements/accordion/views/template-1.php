<?php
/**
 * Element Name: Accordion
 *
 * @package elements/accordion/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id           = uniqid( 'gradient-p' );
$style               = eplus()->get_shortcode_atts( 'style' );
$regular_price       = eplus()->get_shortcode_atts( 'regular_price' );
$accordions          = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'accordions' ) );
$title_color         = eplus()->get_shortcode_atts( 'title_color' );
$content_color       = eplus()->get_shortcode_atts( 'content_color' );
$toggle_color        = eplus()->get_shortcode_atts( 'toggle_color' );
$primary_color       = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color     = eplus()->get_shortcode_atts( 'secondary_color' );
$title_active_color  = eplus()->get_shortcode_atts( 'title_active_color' );
$toggle_active_color = eplus()->get_shortcode_atts( 'toggle_active_color' );
?>

<style>

    <?php if ( !empty( $title_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-item .eplus-accordion-title {
        color: <?php echo esc_attr( $title_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $content_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-content, #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-content * {
        color: <?php echo esc_attr( $content_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $toggle_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .toggle-icon:after, #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .toggle-icon:before {
        background-color: <?php echo esc_attr( $toggle_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $primary_color ) || !empty( $secondary_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-item.active .eplus-accordion-title {
        background-image: linear-gradient(180deg, <?php echo esc_attr( $primary_color ); ?> 34%, <?php echo esc_attr( $secondary_color ); ?> 86%);
    }

    <?php endif; ?>

    <?php if ( !empty( $title_active_color ) || !empty( $toggle_active_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .active .eplus-accordion-title {
        color: <?php echo esc_attr( $title_active_color ); ?>;
    }

    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .active .toggle-icon:after, #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .active .toggle-icon:before {
        background-color: <?php echo esc_attr( $toggle_active_color ); ?>;
    }

    <?php endif; ?>

    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-content:before {
    <?php if ( !empty( $primary_color ) ) { ?> background-color: <?php echo esc_attr( $primary_color ); } ?>;
    }

</style>


<div id="eplus-accordion<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-accordion eplus-accordion-<?php echo esc_attr( $style ); ?>">
	<?php foreach ( $accordions as $index => $accordion ) :
		$title = isset( $accordion['title'] ) && ! empty( $accordion['title'] ) ? $accordion['title'] : '';
		$content = isset( $accordion['content'] ) && ! empty( $accordion['content'] ) ? $accordion['content'] : '';
		?>

        <div class="eplus-accordion-item">
            <h3 class="eplus-accordion-title">
				<?php echo esc_html( $title ); ?>
                <span class="toggle-icon icon-plus"></span>
            </h3>
            <div class="eplus-accordion-content"><?php echo wp_kses_post( $content ); ?></div>
        </div>
	<?php endforeach; ?>
</div>