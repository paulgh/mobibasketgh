<?php
/**
 * Element Name: Accordion
 *
 * @package elements/accordion/view/template-2
 * @copyright Pluginbazar 2019
 */

$unique_id          = uniqid();
$unique_id_2        = uniqid( 'linear-gradient' );
$unique_id_3        = uniqid( 'linear-gradient-2' );
$unique_id_4        = uniqid( 'linear-gradient-3' );
$style              = eplus()->get_shortcode_atts( 'style' );
$accordions         = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'accordions' ) );
$title_color        = eplus()->get_shortcode_atts( 'title_color' );
$content_color      = eplus()->get_shortcode_atts( 'content_color' );
$toggle_color       = eplus()->get_shortcode_atts( 'toggle_color' );
$primary_color      = eplus()->get_shortcode_atts( 'primary_color' );
$secondary_color    = eplus()->get_shortcode_atts( 'secondary_color' );
$title_active_color = eplus()->get_shortcode_atts( 'title_active_color' );
$item_bg            = eplus()->get_shortcode_atts( 'item_bg' );

?>

<style>
    <?php if( !empty( $toggle_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-item.active .onoffswitch-label .onoffswitch-switch {
        background: <?php echo esc_attr( $toggle_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $title_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-item .eplus-accordion-title {
        color: <?php echo esc_attr( $title_color ); ?>;
    }

    <?php endif; ?>

    <?php if ( !empty( $content_color ) ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-content, #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-content * {
        color: <?php echo esc_attr( $content_color ); ?>;
    }

    <?php endif; ?>


    <?php if ( !empty( $item_bg ) || $style == '6' ) : ?>
    #eplus-accordion<?php echo esc_attr( $unique_id ); ?> .eplus-accordion-item {
        background-color: <?php echo esc_attr( $item_bg ); ?>;
    }

    <?php endif; ?>
</style>


<div id="eplus-accordion<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-accordion eplus-accordion-<?php echo esc_attr( $style ); ?>">

    <div class="eplus-accordion-inner">


        <div class="eplus-accordion-wrap">
			<?php foreach ( $accordions as $index => $accordion ) :
				$title = isset( $accordion['title'] ) && ! empty( $accordion['title'] ) ? $accordion['title'] : '';
				$content = isset( $accordion['content'] ) && ! empty( $accordion['content'] ) ? $accordion['content'] : '';
				?>

                <div class="eplus-accordion-item">
                    <h3 class="eplus-accordion-title">
						<?php echo esc_html( $title ); ?>

                        <div class="onoffswitch">
                            <label class="onoffswitch-label" for="myonoffswitch">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>

                    </h3>
                    <div class="eplus-accordion-content"><?php echo wp_kses_post( $content ); ?></div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</div>