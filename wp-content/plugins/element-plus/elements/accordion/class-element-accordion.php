<?php
/**
 * Element Name: Accordion
 *
 * @class EPLUS_Element_accordion
 *
 * @package elements/accordion
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_accordion extends EPLUS_Element {

	function __construct( $id, $element_name = '', $element = array()  ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus accordion', 'element-plus' ),
			'enable_js'               => true,
			'style_variations'        => 6,
			'views'                   => array(
				'4' => 'template-2',
				'5' => 'template-2',
				'6' => 'template-3',
			),
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {
		return array(
			array(
				'type'       => 'param_group',
				'heading'    => esc_html__( 'Feature (s)', 'element-plus' ),
				'param_name' => 'accordions',
				'params'     => array(
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Title', 'element-plus' ),
						'param_name' => 'title',
					),
					array(
						'type'       => 'textarea',
						'heading'    => esc_html__( 'Content', 'element-plus' ),
						'param_name' => 'content',
					),
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Title Color', 'element-plus' ),
				'param_name'  => 'title_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Content Color', 'element-plus' ),
				'param_name'  => 'content_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Toggle Color', 'element-plus' ),
				'param_name'  => 'toggle_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Title Active Color', 'element-plus' ),
				'param_name'  => 'title_active_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '3' ),
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Toggle Active Color', 'element-plus' ),
				'param_name'  => 'toggle_active_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '3' ),
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Item Background', 'element-plus' ),
				'param_name'  => 'item_bg',
				'group'       => esc_html__( 'Design', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '4', '5' ),
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Primary Color', 'element-plus' ),
				'param_name'  => 'primary_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '3', '4', '5' ),
				),
			),
			array(
				'type'        => 'colorpicker',
				'heading'     => esc_html__( 'Secondary Color', 'element-plus' ),
				'param_name'  => 'secondary_color',
				'group'       => esc_html__( 'Design', 'element-plus' ),
				'dependency'  => array(
					'element' => 'style',
					'value'   => array( '3', '4', '5' ),
				),
			),
		);
	}
}