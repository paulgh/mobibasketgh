<?php
/**
 * Element Name: Gallery - Justified
 *
 * @class EPLUS_Element_justified_gallery
 *
 * @package elements/justified-gallery
 * @copyright Pluginbazar 2019
 */

class EPLUS_Element_justified_gallery extends EPLUS_Element {

	function __construct( $id, $element_name = '', $element = array() ) {
		parent::__construct( $id, $element_name, $element );

		$this->set_element_config( array(
			'content_element'         => true,
			'show_settings_on_create' => true,
			'description'             => esc_html__( 'Element plus justified gallery', 'element-plus' ),
			'style_variations'        => 1,
			'enable_js'               => true,
			'scripts_in_footer'       => false,
			'enqueue_assets'          => array(
				'jquery.colorbox-min.js',
				'jquery.justifiedGallery.min.js',
				'colorbox.css',
			),
		) );
	}


	/**
	 * Return settings fields for this element
	 *
	 * @return array
	 */
	function setting_fields() {
		return array(
			array(
				'type'       => 'param_group',
				'heading'    => esc_html__( 'Select Images', 'element-plus' ),
				'param_name' => 'gallerys',
				'params'     => array(
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Single Image', 'element-plus' ),
						'param_name' => 'img_id',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Title', 'element-plus' ),
						'param_name' => 'title',
					),
				),
			),
			array(
				'type'       => 'number_field',
				'param_name' => 'img_height',
				'heading'    => esc_html__( 'Image Height', 'element-plus' ),
				'group'      => esc_html__( 'Gallery Options', 'element-plus' ),
				'value'      => 200,
			),
			array(
				'type'       => 'number_field',
				'param_name' => 'margins',
				'heading'    => esc_html__( 'Margins or Gap', 'element-plus' ),
				'group'      => esc_html__( 'Gallery Options', 'element-plus' ),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'last_row',
				'heading'    => esc_html__( 'Last Row', 'element-plus' ),
				'group'      => esc_html__( 'Gallery Options', 'element-plus' ),
				'value'      => array(
					esc_html__( 'No Justify' ) => 'nojustify',
					esc_html__( 'Justify' )    => 'justify',
					esc_html__( 'Hide' )       => 'hide',
					esc_html__( 'Center' )     => 'center',
					esc_html__( 'Right' )      => 'right',
				),
			),
			array(
				'type'       => 'checkbox',
				'param_name' => 'captions',
				'heading'    => esc_html__( 'Captions', 'element-plus' ),
				'group'      => esc_html__( 'Gallery Options', 'element-plus' ),
			),
			array(
				'type'       => 'range_slider',
				'heading'    => esc_html__( 'Image Max Width', 'element-plus' ),
				'group'      => esc_html__( 'Light Box Option', 'element-plus' ),
				'param_name' => 'max_width',
				'min'        => 0,
				'max'        => 100,
				'step'       => 1,
				'value'      => 80,
			),
			array(
				'type'       => 'range_slider',
				'heading'    => esc_html__( 'Image Max Height', 'element-plus' ),
				'group'      => esc_html__( 'Light Box Option', 'element-plus' ),
				'param_name' => 'max_height',
				'min'        => 0,
				'max'        => 100,
				'step'       => 1,
				'value'      => 80,
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'transition',
				'heading'    => esc_html__( 'Transition', 'element-plus' ),
				'group'      => esc_html__( 'Light Box Option', 'element-plus' ),
				'value'      => array(
					esc_html__( 'Elastic' ) => 'elastic',
					esc_html__( 'Fade' )    => 'fade',
					esc_html__( 'None' )    => 'none',
				),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'slideshow',
				'heading'     => esc_html__( 'Slideshow', 'element-plus' ),
				'description' => esc_html__( 'If true, adds an automatic slideshow to a content group / gallery.', 'element-plus' ),
				'group'       => esc_html__( 'Light Box Option', 'element-plus' ),
			),
			array(
				'type'        => 'checkbox',
				'param_name'  => 'overlay_close',
				'heading'     => esc_html__( 'Overlay Close', 'element-plus' ),
				'description' => esc_html__( 'If false, disables closing Colorbox by clicking on the background overlay.', 'element-plus' ),
				'group'       => esc_html__( 'Light Box Option', 'element-plus' ),
			),
			array(
				'type'        => 'number_field',
				'param_name'  => 'opacity',
				'heading'     => esc_html__( 'Opacity', 'element-plus' ),
				'description' => esc_html__( 'The overlay opacity level. Range: 0.1 - 1', 'element-plus' ),
				'group'       => esc_html__( 'Light Box Option', 'element-plus' ),
			),
			array(
				'type'        => 'number_field',
				'param_name'  => 'speed',
				'heading'     => esc_html__( 'Speed', 'element-plus' ),
				'description' => esc_html__( 'Sets the speed of the fade and elastic transitions, in milliseconds.', 'element-plus' ),
				'group'       => esc_html__( 'Light Box Option', 'element-plus' ),
			),
		);
	}
}