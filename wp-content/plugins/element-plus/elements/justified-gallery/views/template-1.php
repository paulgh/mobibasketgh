<?php
/**
 * Element Name: Gallery - Justified
 *
 * @package elements/justified-gallery
 * @copyright Pluginbazar 2019
 */

$unique_id  = uniqid();
$gallerys   = (array) vc_param_group_parse_atts( eplus()->get_shortcode_atts( 'gallerys' ) );
$img_height = eplus()->get_shortcode_atts( 'img_height', 200 );
$margins    = eplus()->get_shortcode_atts( 'margins', 0 );
$last_row   = eplus()->get_shortcode_atts( 'last_row', 'nojustify' );
$captions   = eplus()->get_shortcode_atts( 'captions', 'false' );

$max_width     = eplus()->get_shortcode_atts( 'max_width', 80 );
$max_height    = eplus()->get_shortcode_atts( 'max_height', 80 );
$opacity       = eplus()->get_shortcode_atts( 'opacity', 0.8 );
$transition    = eplus()->get_shortcode_atts( 'transition', 'elastic' );
$slideshow     = eplus()->get_shortcode_atts( 'slideshow', 'false' );
$overlay_close = eplus()->get_shortcode_atts( 'overlay_close', 'false' );
$speed         = eplus()->get_shortcode_atts( 'speed', 350 );

?>

<div id="eplus-gallery<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-gallery-grid">
	<?php foreach ( $gallerys as $gallery ) :

		$img_url = isset( $gallery['img_id'] ) ? wp_get_attachment_url( $gallery['img_id'] ) : '';

		$title = eplus()->get_shortcode_atts( 'title', 'Gallery Image', $gallery );

		if ( empty( $img_url ) ) {
			continue;
		}

		?>
        <a href="<?php echo esc_url( $img_url ); ?>" class="gallery-zoom">
            <img src="<?php echo esc_url( $img_url ); ?>" alt="<?php echo esc_attr( $title ); ?>">
        </a>
	<?php endforeach; ?>

</div>

<script>
    (function ($, document) {
        "use strict";

        $(document).on('ready', function () {
            $("#eplus-gallery<?php echo esc_attr( $unique_id ); ?>").each(function (i, el) {
                $(el).justifiedGallery({
                    rel: 'gal' + i,
                    rowHeight: <?php echo esc_attr( $img_height ); ?>,
                    lastRow: '<?php echo esc_attr( $last_row ); ?>',
                    captions: <?php echo esc_attr( $captions ); ?>,
                    margins: <?php echo esc_attr( ! empty( $margins ) ? $margins : 0 ); ?>,
                }).on('jg.complete', function () {
                    $(this).find('a').colorbox({
                        maxWidth: '<?php echo esc_attr( $max_width ); ?>%',
                        maxHeight: '<?php echo esc_attr( $max_height ); ?>%',
                        opacity: <?php echo esc_attr( ! empty( $opacity ) ? $opacity : 0.8 ); ?>,
                        transition: '<?php echo esc_attr( $transition ); ?>',
                        speed: <?php echo esc_attr( $speed ); ?>,
                        current: '',
                        overlayClose: <?php echo esc_attr( $overlay_close ); ?>,
                        slideshow: <?php echo esc_attr( $slideshow ); ?>,
                    });
                });
            });
        });

    })(jQuery, document);
</script>

