<?php
/**
 * Element Name: Banner
 *
 * @package elements/banner/view/template-1
 * @copyright Pluginbazar 2019
 */

$unique_id  = uniqid();
$style      = eplus()->get_shortcode_atts( 'style' );
$title      = eplus()->get_shortcode_atts( 'title' );
$sub_title  = eplus()->get_shortcode_atts( 'sub_title' );
$short_desc = eplus()->get_shortcode_atts( 'short_desc' );
$btn_1      = vc_build_link( eplus()->get_shortcode_atts( 'btn_1' ) );
$btn_2      = vc_build_link( eplus()->get_shortcode_atts( 'btn_2' ) );
$align      = eplus()->get_shortcode_atts( 'align' );
$padding    = eplus()->get_shortcode_atts( 'padding' );

$desc_size      = eplus()->get_shortcode_atts( 'desc_size' );
$title_size     = eplus()->get_shortcode_atts( 'title_size' );
$sub_title_size = eplus()->get_shortcode_atts( 'sub_title_size' );
$btn_padding     = eplus()->get_shortcode_atts( 'btn_padding' );
$btn_radius     = eplus()->get_shortcode_atts( 'btn_radius' );
$btn_shadow     = eplus()->get_shortcode_atts( 'btn_shadow' );

?>

<style>
    <?php if( !empty( $padding ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> {
        padding: <?php echo esc_attr( $padding ); ?>
    }

    <?php endif; ?>

    <?php if( !empty( $title_size ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .banner-title {
        font-size: <?php echo esc_attr( $title_size ); ?>
    }

    <?php endif; ?>

    <?php if( !empty( $sub_title_size ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .banner-sub-title {
        font-size: <?php echo esc_attr( $sub_title_size ); ?>
    }

    <?php endif; ?>

    <?php if( !empty( $desc_size ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .short-desc {
        font-size: <?php echo esc_attr( $desc_size ); ?>
    }

    <?php endif; ?>

    <?php if( !empty( $btn_radius ) || !empty( $btn_padding ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .banner-btns > a {
        border-radius: <?php echo esc_attr( $btn_radius ); ?>;
        padding: <?php echo esc_attr( $btn_padding ); ?>;
    }

    <?php endif; ?>

    <?php if( !empty( $btn_shadow ) ) : ?>
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .banner-btns > a:hover,
    #eplus-banner<?php echo esc_attr( $unique_id ); ?> .banner-btns > a:focus {
        box-shadow: <?php echo esc_attr( $btn_shadow ); ?>
    }

    <?php endif; ?>
</style>


<div id="eplus-banner<?php echo esc_attr( $unique_id ); ?>"
     class="eplus-banner-<?php echo esc_attr( $style . ' ' . $align ) ?>">
    <div class="container">
		<?php if ( ! empty( $sub_title ) ) : ?>
            <h4 class="banner-sub-title"><?php echo esc_html( $sub_title ); ?></h4>
		<?php endif; ?>

		<?php if ( ! empty( $title ) ) : ?>
            <h2 class="banner-title"><?php echo esc_html( $title ); ?></h2>
		<?php endif; ?>

		<?php if ( ! empty( $short_desc ) ) : ?>
            <p class="short-desc"><?php echo wp_kses_post( $short_desc ); ?></p>
		<?php endif; ?>

        <div class="banner-btns">
			<?php if ( ! empty( $btn_1['url'] && ! empty( $btn_1['title'] ) ) ) : ?>
                <a href="<?php echo esc_url( $btn_1['url'] ); ?>"
                   class="banner-btn-1"><?php echo esc_html( $btn_1['title'] ); ?></a>
			<?php endif; ?>

			<?php if ( ! empty( $btn_2['url'] && ! empty( $btn_2['title'] ) ) ) : ?>
                <a href="<?php echo esc_url( $btn_1['url'] ); ?>"
                   class="banner-btn-2"><?php echo esc_html( $btn_2['title'] ); ?></a>
			<?php endif; ?>
        </div>
    </div>
</div>