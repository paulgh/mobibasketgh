<?php
/**
 * Class Hooks
 */

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'EPLUS_Hooks' ) ) {
	/**
	 * Class EPLUS_Hooks
	 */
	class EPLUS_Hooks {

		/**
		 * EPLUS_Hooks constructor.
		 */
		function __construct() {

			$this->generate_settings_page();

			add_action( 'admin_notices', array( $this, 'check_dependencies' ), 99 );
			add_action( 'vc_edit_form_fields_after_render', array( $this, 'check_element_dependencies' ) );
			add_filter( 'eplus_filters_element_params', array( $this, 'filter_element_params' ), 10, 2 );

			add_action( 'init', array( $this, 'register_all_elements' ) );
			add_action( 'vc_after_init', array( $this, 'register_param_types' ) );
			add_filter( 'plugin_action_links_' . EPLUS_PLUGIN_FILE, array( $this, 'add_plugin_actions' ), 10, 1 );
			add_filter( 'upload_mimes', array( $this, 'add_mimes' ) );
		}



		/**
		 * Add svg Support
		 *
		 * @param $mimes
		 *
		 * @return mixed
		 */
		function add_mimes( $mimes ) {

			if ( in_array( 'yes', eplus()->get_option( 'eplus_enable_svg', array() ) ) ) {
				$mimes['svg'] = 'image/svg+xml';

				if ( ! defined( 'ALLOW_UNFILTERED_UPLOADS' ) ) {
					define( 'ALLOW_UNFILTERED_UPLOADS', true );
				}
			}

			return $mimes;
		}


		/**
		 * Add Settings Menu at Plugin Menu
		 *
		 * @param $links
		 *
		 * @return array
		 */
		function add_plugin_actions( $links ) {

			$action_links = array(
				'settings' => sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=eplus' ),
					esc_html__( 'Settings', 'element-plus' )
				),
			);

			return array_merge( $action_links, $links );
		}


		/**
		 * Register Param types
		 *
		 * @throws ReflectionException
		 */
		function register_param_types() {

			foreach ( eplus()->get_param_types() as $param_type => $param ) {

				$param_class     = isset( $param['class_name'] ) ? $param['class_name'] : '';
				$file_to_include = sprintf( '%1$sparam-types/%2$s/class-%2$s.php', EPLUS_PLUGIN_DIR, eplus()->get_element_raw_id( $param_type ) );

				if ( ! file_exists( $file_to_include ) || empty( $param_class ) ) {
					continue;
				}

				require $file_to_include;

				$reflection_class = new ReflectionClass( $param_class );
				$reflection_class->newInstanceArgs( array( $param_type ) );
			}
		}


		/**
		 * Register all Elements
		 *
		 * @throws ReflectionException
		 */
		function register_all_elements() {

			foreach ( eplus()->get_elements() as $element_id => $element ) {

				$element_name     = isset( $element['label'] ) ? $element['label'] : '';
				$element_class    = isset( $element['class_name'] ) ? $element['class_name'] : '';
				$file_to_include  = sprintf( '%1$selements/%2$s/class-element-%2$s.php', EPLUS_PLUGIN_DIR, eplus()->get_element_raw_id( $element_id ) );
				$disable_elements = eplus()->get_option( 'eplus_disable_elements', array() );

				if ( in_array( $element_id, $disable_elements ) || ! file_exists( $file_to_include ) || empty( $element_class ) ) {
					continue;
				}

				require $file_to_include;

				$reflection_class = new ReflectionClass( $element_class );
				$reflection_class->newInstanceArgs( array( $element_id, $element_name, $element ) );
			}

			add_image_size( 'gallery-thumb', 260, 260, true );
			add_image_size( 'post-grid-thumb', 430, 323, true );
		}


		/**
		 * Check dependency for element and return param fields
		 *
		 * @param $params
		 * @param $element_id
		 *
		 * @return array
		 */
		function filter_element_params( $params, $element_id ) {

			$has_dependency = false;

			foreach ( eplus_get_element_dependencies( $element_id ) as $dependency ) {

				$class = isset( $dependency['class'] ) ? $dependency['class'] : '';

				if ( ! empty( $class ) && ! $has_dependency && ! class_exists( $class ) ) {
					$has_dependency = true;
				}
			}

			if ( $has_dependency ) {
				return array();
			}

			return $params;
		}


		/**
		 * Check for dependencies of Element
		 */
		function check_element_dependencies() {

			$element_id = isset( $_POST['tag'] ) ? sanitize_text_field( $_POST['tag'] ) : '';

			foreach ( eplus_get_element_dependencies( $element_id ) as $dependency ) {

				$class   = isset( $dependency['class'] ) ? $dependency['class'] : '';
				$message = isset( $dependency['message'] ) ? $dependency['message'] : '';

				if ( ! empty( $class ) && ! class_exists( $class ) ) {
					printf( '<p class="eplus-notice eplus-error">%s</p>', $message );
				}
			}
		}


		/**
		 * Check whether main plugin WP Poll is installed or not
		 */
		function check_dependencies() {

			if ( ! class_exists( 'Vc_Manager' ) ) {

				$message = sprintf( '<strong>%s</strong> %s. <a href="%s" target="_blank">%s</a>',
					esc_html( 'WPBakery Page Builder' ),
					esc_html__( 'plugin is required for Element Plus', 'element-plus' ),
					esc_url( '//codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431' ),
					esc_html__( 'Get it Now', 'element-plus' )
				);

				printf( '<div class="notice notice-error is-dismissible"><p>%s</p></div>', $message );
				deactivate_plugins( EPLUS_PLUGIN_FILE );
			}
		}

		/**
		 * Generate settings page
		 */
		function generate_settings_page() {

			$elements = array();
			foreach ( eplus()->get_elements() as $element_id => $element ) {
				$elements[ $element_id ] = eplus()->get_shortcode_atts( 'label', '', $element );
			}

			$pages['eplus-general'] = array(
				'page_nav'      => esc_html__( 'Settings', 'element-plus' ),
				'page_settings' => array(
					array(
						'title'   => esc_html__( 'General Settings', 'element-plus' ),
						'options' => array(
							array(
								'id'      => 'eplus_enable_svg',
								'title'   => esc_html__( 'SVG Uploader', 'element-plus' ),
								'details' => esc_html__( 'Enable SGV uploading in your WordPress website', 'element-plus' ),
								'type'    => 'checkbox',
								'args'    => array(
									'yes' => esc_html__( 'Enable / Disable', 'element-plus' ),
								),
							),
							array(
								'id'      => 'eplus_fontawesome_support',
								'title'   => esc_html__( 'FontAwesome 4+ Support', 'element-plus' ),
								'details' => esc_html__( 'Enable FontAwesome 4 in your WordPress website', 'element-plus' ),
								'type'    => 'checkbox',
								'args'    => array(
									'yes' => esc_html__( 'Enable / Disable', 'element-plus' ),
								),
							),
						),
					),
					array(
						'title'   => esc_html__( 'Elements Settings', 'element-plus' ),
						'options' => array(
							array(
								'id'      => 'eplus_disable_elements',
								'title'   => esc_html__( 'Disable Elements', 'element-plus' ),
								'details' => esc_html__( 'You can disable some elements that are not required in your website. These will not load and thus your website will be more faster.', 'element-plus' ),
								'type'    => 'checkbox',
								'args'    => $elements,
							),
						),
					),
				),
			);

			eplus()->PB( array(
				'add_in_menu' => true,
				'menu_type'   => 'submenu',
				'menu_title'  => esc_html__( 'ElementPlus', 'element-plus' ),
				'menu_name'   => esc_html__( 'ElementPlus Settings', 'element-plus' ),
				'page_title'  => esc_html__( 'ElementPlus - Settings', 'element-plus' ),
				'capability'  => 'manage_options',
				'menu_slug'   => 'eplus',
				'parent_slug' => 'vc-general',
				'pages'       => $pages,
				'menu_icon'   => EPLUS_PLUGIN_URL . 'assets/images/eplus-logo.svg',
				'position'    => 20,
			) );
		}
	}

	new EPLUS_Hooks();
}