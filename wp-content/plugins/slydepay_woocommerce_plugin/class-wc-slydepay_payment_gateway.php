<?php
/**
 * WC Slydepay Gateway Class.
 * Built the slydepay method.
 */
class WC_Slydepay_Payment_Gateway extends WC_Payment_Gateway {

    /**
     * Constructor for the gateway.
     *
     * @return void
     */
    public function __construct() {
        global $woocommerce;

        $this->id             = 'slydepay';
        //$this->icon           = apply_filters( 'woocommerce_slydepay_icon', 'http://www.doerslab.com/slydepay/images/badges/pay_with_slydepay.png' );
        $this->has_fields     = false;
        $this->method_title   = __( 'Slydepay', 'slydepay' );

		$this->servUri = 'https://app.slydepay.com.gh/webservices/paymentservice.asmx?wsdl';
		$this->payUri = 'https://app.slydepay.com.gh/paylive/detailsnew.aspx';
		$this->ns = 'http://www.i-walletlive.com/payLIVE';
		$this->SvcType = 'C2B';

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        // Define user set variables.
        $this->title          = $this->settings['title'];
        $this->description    = $this->settings['description'];

        $this->version          = $this->settings['version'];
        $this->merchant_key          = $this->settings['merchant_key'];
        $this->merchant_email          = $this->settings['merchant_email'];
        $this->callback_url          = $this->settings['callback_url'];
        $this->integration_mode          = $this->settings['integration_mode'];

		$this->instructions       = $this->get_option( 'instructions' );
		$this->enable_for_methods = $this->get_option( 'enable_for_methods', array() );

        // Actions.
        if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) )
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
        else
            add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );


		add_action( 'woocommerce_api_wc_slydepay_payment_gateway', array( $this, 'callback_response' ) );


    }

	public function get_icon() {
		$icon = 'https://s3.amazonaws.com/slydepay-public/badges/pay_with_slydepay.png';
		$icon_html = '<img src="' . esc_attr( apply_filters( 'woocommerce_slydepay_icon', $icon ) ) . '" alt="Slydepay Acceptance Badge" />';

		$link = 'https://www.slydepay.com.gh';
		$what_is_slydepay = sprintf( '<a href="%1$s" style="float: right;font-size: 0.83em;line-height: 52px;" onclick="javascript:window.open(\'%1$s\',\'WISlydepay\',\'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700\'); return false;" title="' . esc_attr__( 'What is Slydepay?', 'woocommerce' ) . '">' . esc_attr__( 'What is Slydepay?', 'woocommerce' ) . '</a>', esc_url( $link ) );

		return apply_filters( 'woocommerce_gateway_icon', $icon_html . $what_is_slydepay, $this->id );
	}

	private function setHeader($soapClient){

		$header_params = array(
			"APIVersion"	=> trim($this->version),
			"MerchantKey"	=> trim($this->merchant_key),
			"MerchantEmail" => trim($this->merchant_email),
			"SvcType" 		=> $this->SvcType,
			"UseIntMode" 	=> $this->integration_mode,
		);

		$header = new SOAPHeader($this->ns, 'PaymentHeader', $header_params);
		$soapClient->__setSoapHeaders($header);

		return TRUE;
	}

	private function getOrderParams($order){
		// Products
		$products = array();
		if ( sizeof( $order->get_items() ) > 0 ) {
			foreach ( $order->get_items() as $item ) {
				if ( ! $item['qty'] ) {
					continue;
				}
				$item_loop ++;
				$product   = $order->get_product_from_item( $item );
				$item_name = $item['name'];

				$products[] = array(
									'ItemCode' => $product->get_sku(),
				                    'ItemName' => $item_name,
				                    'UnitPrice' => $product->get_price(),
				                    'Quantity' =>  $item['qty'],
				                    'SubTotal' =>  $order->get_item_subtotal( $item, false )
									);

			}
		}

		//slydepay vars to send
		$params = array();
		$params['orderId'] = $order->id;
		//$params['subtotal'] = number_format( $order->get_total() - round( $order->get_total_shipping() + $order->get_shipping_tax(), 2 ) + $order->get_order_discount(), 2, '.', '' );
		$params['subtotal'] = number_format( $order->get_total() - round( $order->get_total_shipping() + $order->get_shipping_tax(), 2 ) + $order->get_total_discount(), 2, '.', '' );
		$params['shippingCost'] = number_format( $order->get_total_shipping() + $order->get_shipping_tax(), 2, '.', '' );
		$params['taxAmount'] = $order->get_shipping_tax();
		$params['total'] = $order->get_total();
		$params['comment1'] = substr($comment, 0, -2);;
		$params['orderItems'] = array('OrderItem' => $products);
		//echo '<pre>';print_r($order);print_r($params);exit;
		return $params;
	}

    /* Admin Panel Options.*/
	function admin_options() {
		?>
		<h3><?php _e('Slydepay','slydepay'); ?></h3>
    	<table class="form-table">
    		<?php $this->generate_settings_html(); ?>
		</table> <?php
    }

    /* Initialise Gateway Settings Form Fields. */
    public function init_form_fields() {
    	global $woocommerce;

    	$shipping_methods = array();

    	if ( is_admin() )
	    	foreach ( $woocommerce->shipping->load_shipping_methods() as $method ) {
		    	$shipping_methods[ $method->id ] = $method->get_title();
	    	}

        $this->form_fields = array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'slydepay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable Slydepay', 'slydepay' ),
                'default' => 'no'
            ),
            'title' => array(
                'title' => __( 'Title', 'Slydepay' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'slydepay' ),
                'desc_tip' => true,
                'label' => __( 'slydepay', 'slydepay' ),
                'default' => __( 'Slydepay', 'slydepay' )
            ),
            'description' => array(
                'title' => __( 'Description', 'slydepay' ),
                'type' => 'textarea',
                'description' => __( 'This controls the description which the user sees during checkout.', 'slydepay' ),
                'default' => __( 'Pay via Slydepay; you can pay with your debit/credit card, bank account or mobile money if you don\'t have a Slydepay account.', 'slydepay' )
            ),
            'version' => array(
                'title' => __( 'API Version', 'slydepay' ),
                'type' => 'text',
                'description' => __( 'Specify the API version from which you are integrating your application. For this API version specify (1.4).', 'slydepay' ),
                'desc_tip' => true,
                'default' => __( '1.4', 'slydepay' )
            ),
            'merchant_key' => array(
                'title' => __( 'API Key', 'slydepay' ),
                'type' => 'text',
                'description' => __( 'API Key is generated from the settings page in your Slydepay business account.', 'slydepay' ),
                'desc_tip' => true,
                'default' => ''
            ),
            'merchant_email' => array(
                'title' => __( 'Merchant Email', 'slydepay' ),
                'type' => 'text',
                'description' => __( 'The email address for your Slydepay business account.', 'slydepay' ),
                'desc_tip' => true,
                'default' => ''
            ),
            'callback_url' => array(
                'title' => __( 'Callback URL', 'slydepay' ),
                'type' => 'textarea',
                'description' => __( 'The is the url Slydepay will post back to once transaction is complete.', 'slydepay' ),
                'desc_tip' => true,
                'default' => __( WC()->api_request_url( 'WC_Slydepay_Payment_Gateway' ), 'slydepay' )
            ),
			'integration_mode' => array(
				'title'       => __( 'Use Integration Mode', 'slydepay' ),
				'type'        => 'select',
				'description' => __( 'Select [Yes] if you are in test mode.  Select [No] if you are in live mode.', 'woocommerce' ),
				'default'     => '0',
				'desc_tip'    => true,
				'options'     => array(
					'1'          => __( 'Yes', 'slydepay' ),
					'0' => __( 'No', 'slydepay' )
				)
			),

        );

    }




    /* Process the payment and return the result. */
	function process_payment ($order_id) {

		global $woocommerce;

		$order = new WC_Order( $order_id );
		// create soap instance
		$soapClient = new SoapClient($this->servUri);
		// set header
		$this->setHeader($soapClient);
		// get order params
		$params = $this->getOrderParams($order);
		//make a request for proccessing payment
		$result = $soapClient->ProcessPaymentOrder($params);

		$resposnse = array();
		$resposnse['error'] = '';

		// if soap fault occerred
		if (is_soap_fault($result)) {
			$resposnse['error'] = 'ERROR: SOAP Fault! Order proccessing failed!';
		}
		// check response and do accordingly
		if(isset($result->ProcessPaymentOrderResult) && strlen($result->ProcessPaymentOrderResult)==36){
			$resposnse['pay_token'] = $result->ProcessPaymentOrderResult;
			$resposnse['order_id'] = $order_id;
			$resposnse['pay_uri'] = $this->payUri;
		}elseif(isset($result->ProcessPaymentOrderResult)){
			$resposnse['error'] = $result->ProcessPaymentOrderResult;
		}else{
			$resposnse['error'] = 'Unknow ERROR! Order proccessing failed!';
		}
		//echo '<pre>';print_r($result);print_r($resposnse);
		//echo add_query_arg(array( 'pay_token' => $resposnse['pay_token'], 'order_id' => $this->oid ), $this->payUri);exit;
		if($resposnse['pay_token']!=''){
			// Mark as on-hold
			//$order->update_status('on-hold', __( 'Your order wont be shipped until the funds have cleared in our account.', 'woocommerce' ));

			// Reduce stock levels
			//$order->reduce_order_stock();

			// Remove cart
			//$woocommerce->cart->empty_cart();

			// Return thankyou redirect
			return array(
				'result' 	=> 'success',
				'redirect'	=> add_query_arg(array( 'pay_token' => $resposnse['pay_token'], 'order_id' => $order_id ), $this->payUri)
			);
		}else{
			$error_message = 'Order Id already exists. please use a different one';
			if($resposnse['error'] == 'Error: 1'){
				$error_message = 'Merchant Credentials wrong check email or merchant key';
			}else if($resposnse['error'] == 'Error: -1'){
				$error_message = 'Technical error contact <a href="https://support.i-walletlive.com/">Slydepay Support</a>';
			}else if($resposnse['error'] == 'Error: 2'){
				$error_message = 'Merchant not confirmed';
			}else if($resposnse['error'] == 'Error: 3'){
				$error_message = 'Merchant account not verified';
			}else if($resposnse['error'] == 'Error: 4'){
				$error_message = 'Integration Mode set to OFF in account. Set UseIntMode param to "0" or switch integration mode on in account.';
			}
			wc_add_notice( __( 'Slydepay error: ', 'slydepay' ) . $error_message, 'error' );
		}
	}

	public function callback_response() {
		@ob_clean();
		$status = isset($_GET['status']) ? $_GET['status'] : -1;
		$cust_ref = isset($_GET['cust_ref']) ? $_GET['cust_ref'] : '';
		$transac_id = isset($_GET['transac_id']) ? $_GET['transac_id'] : '';
		$pay_token = isset($_GET['pay_token']) ? $_GET['pay_token'] : '';
		$order = $order = wc_get_order( $cust_ref );
		if($status == 0){
			$soapClient = new SoapClient($this->servUri);
			$this->setHeader($soapClient);
			$params = array();
			$params['payToken'] = $pay_token;
			$params['transactionId'] = $transac_id;
			$result = $soapClient->ConfirmTransaction($params);
			if(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==1){ //Confirmation Successful
				$comment = "transac_id = ". $transac_id . "\n";
				$order->add_order_note( __( $comment, 'woocommerce' ) );
				$order->payment_complete( $transac_id );
				header('Location:'.$this->get_return_url( $order ));
				exit;
			}elseif(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==0){ //Confirmation failed: Invalid transaction Id
				wp_die( "Confirmation failed: Invalid transaction Id", "Slydepay", array( 'response' => 200 ) );
			}elseif(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==-1){ //Confirmation Failed: Invalid pay token
				wp_die( "Confirmation Failed: Invalid pay token", "Slydepay", array( 'response' => 200 ) );
			}else{
				return array(
					'result' 	=> 'success',
					'redirect'	=> home_url()
				);
				header('Location:'.home_url());
				exit;
			}
		}elseif($status==-1){ // Technical error contact
			wp_die( "Technical error contact", "Slydepay", array( 'response' => 200 ) );
		}elseif($status==-2){ // User cancelled transaction
				header('Location:'.home_url());
				exit;
		}


	}


    /* Output for the order received page.   */
	function thankyou() {
		echo $this->instructions != '' ? wpautop( $this->instructions ) : '';
	}



}
