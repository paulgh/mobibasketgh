<?php
	$this->sections[] = array(
		'title'  => __( 'Import / Export', 'arnica' ),
		'desc'   => __( 'Import and Export your Arnica settings from file, text or URL.', 'arnica' ),
		'icon'   => 'el-icon-refresh',
		'fields' => array(
		
			//Import / Export
			array(
				'id'         => 'opt-import-export',
				'type'       => 'import_export',
				'title'      => __( 'Import Export', 'arnica' ),
				'subtitle'   => __( 'Save and restore your Arnica options', 'arnica' ),
				'full_width' => false,
			),
		),
	);
?>