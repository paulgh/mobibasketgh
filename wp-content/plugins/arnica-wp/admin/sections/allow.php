<?php
	$this->sections[] = array(
		'title'  => __( 'Allow Access', 'arnica' ),
		'icon'   => 'el el-user',
		  'fields' => array(
		  
			//IP Address
			array(
				'id'	   => 'ip_address',
				'type' 	   => 'textarea',
				'title'    => __('IP Address', 'arnica'),
				'subtitle' => __( 'Enter each IP address to one line.', 'arnica' ),
				'validate' => 'no_html',
				'default'  => '',
			),
			
			//Exclude URL
			array(
				'id'	   => 'exclude_url',
				'type' 	   => 'textarea',
				'title'    => __('Exclude URL', 'arnica'),
				'subtitle' => __( 'Enter each link to one line.', 'arnica' ),
				'validate' => 'no_html',
				'default'  => '',
			),
			
		),
	);
?>