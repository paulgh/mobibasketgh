<?php
	$this->sections[] = array(
		'title'  => __( 'Main Options', 'arnica' ),
		'icon'   => 'el el-home',
		  'fields' => array(
	
		   	//Plugin mode
			array(
				'id'        => 'plugin_mode',
				'type'      => 'switch',
				'title'     => __( 'Plugin Mode', 'arnica' ),
				'subtitle' => __( 'Choose the mode in which you want the plugin to operate in.', 'arnica' ),
				'desc'     => __( '<ul><li><strong>Off</strong> - Arnica will be switch off.</li><li><strong>Coming Soon Mode</strong> - <strong>JUST</strong> Visitors will see Arnica while you work on your theme (Administrator can not see).</li>', 'arnica' ),
				'on'        => __( 'Coming Soon Mode', 'arnica' ),
				'off'       => __( 'Off', 'arnica' ),
				'default'   => false
			),
					
			//Skin color
			array(
				'id'       => 'color',
				'type'     => 'select',
				'title'    => __( 'Skin Color', 'arnica' ),
				'subtitle' => __( 'Choose your skin Color', 'arnica' ),
				'options'  => array(
					'green' 	=> 'Green',	
					'blue' 		=> 'Blue',	
					'orange' 	=> 'Orange',			
					'purple' 	=> 'Purple',
					'yellow' 	=> 'Yellow',					
					'grey' 		=> 'Grey'
				),
				'default'  => 'green'	
			),
			  
			//Logo
			array(
				'id'       => 'logo',
				'type'     => 'media',
				'title'    => __( 'Logo', 'arnica' ),
			),
		  
		   	//Favicon
			array(
				'id'       => 'favicon',
				'type'     => 'media',
				'title'    => __( 'Favicon', 'arnica' ),
				'desc'     => __( 'Use a 16x16 .ico or .png file.', 'arnica' ),
			),	
			
			//Title
			array(
				'id'       => 'title',
				'type'     => 'text',
				'title'    => __( 'Title', 'arnica' ),
				'validate' => 'html',
				'default'  => "<strong>Arnica</strong> Is ",
			),
			  
			//Title words
			array(
				'id'       => 'words_wrapper',
				'type'     => 'text',
				'title'    => __( 'Words Wrapper', 'arnica' ),
				'validate' => 'html',
				'default'  => "Coming Soon|Creative|Launching",
			),	
			
			//Slogan
		  	array(
				'id'        => 'slogan',
				'type'      => 'editor',
				'title'     => __( 'Slogan', 'arnica' ),
				'default'   => __( 'Our website is under construction, we are working very hard to give you the best experience. <br class="hidden-xs">You will love <strong>Arnica</strong> as much as we do. <span class="hidden-xs">It will morph perfectly on your needs!</span>', 'arnica' )
			),
			
		),
	);
?>