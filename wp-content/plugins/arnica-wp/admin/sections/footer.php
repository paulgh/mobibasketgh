<?php
	$this->sections[] = array(
		'title'  => __( 'Footer Options', 'arnica' ),
		'icon'   => 'el el-icon-chevron-down',
		'fields' => array(
		  
		  	//Copyright
		  	array(
				'id'        => 'copyright_text',
				'type'      => 'editor',
				'title'     => __( 'Copyright Text', 'arnica' ),
				'desc'      => __( 'You can use the shortcodes in your footer text', 'arnica' ),
				'default'   => __( 'Copyright &copy; 2019 <strong>Arnica</strong>', 'arnica' ),
				'class'		=> 'arnica-editor-style'
			),
		  
			//Social link
			array(
			'id'			=> 'social_link',
			'type' 			=> 'social',
			'title' 		=> __('Social Links', 'arnica'),
			'options' 		=> ArnicaFontAwesomeIcons(),
			'default_show' 	=> false,
			'default' 		=> array(
								0 => array(
									'title' 	=> 'Twitter',
									'select' 	=> 'fab fa-twitter',
									'url' 		=> '#',
									'sort' 		=> 0
								),
								1 => array(
									'title' 	=> 'Facebook',
									'url' 		=> '#',
									'select' 	=> 'fab fa-facebook-f',
									'sort' 		=> 1
								),
								2 => array(
									'title' 	=> 'Instagram',
									'url' 		=> '#',
									'select' 	=> 'fab fa-instagram',
									'sort' 		=> 2
								),
							)
			),
			
		),
	);
?>