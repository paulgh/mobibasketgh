<?php
	$this->sections[] = array(
		'title'  => __( 'Background', 'arnica' ),
		'icon'   => 'el el-picture',
		  'fields' => array(
		  
			//Background type
			array(
				'id'       => 'bg_type',
				'type'     => 'radio',
				'title'    => __('Background Type', 'redux-framework-demo'), 
				'options'  => array(
					'1' => 'Single Image', 
					'2' => 'Image Slideshow', 
					'3' => 'Youtube Background'
				),
				'default' => '1'
			),
			
			//Single image
			array(
				'id'       => 'single_image_section',
				'type'     => 'section',
				'indent'   => true,
				'required' => array( 'bg_type', "=", 1 ),
			),
			
			array(
				'id'       => 'single_image',
				'type'     => 'media',
				'subtitle' => __( 'Select the image you want in the background', 'arnica' ),
				'title'    => __( 'Single Image', 'arnica' ),
			),	
			
			array(
				'id'       => 'single_image_section_end',
				'type'     => 'section',
				'indent'   => false,
			),
			
			//Image slideshow
			array(
				'id'       => 'image_slideshow_section',
				'type'     => 'section',
				'indent'   => true,
				'required' => array( 'bg_type', "=", 2 ),
			),
			
			array(
				'id'       => 'image_slideshow',
				'type'     => 'gallery',
				'title'    => __( 'Image Slideshow', 'arnica' ),
				'subtitle' => __( 'Select images you want in the background', 'arnica' ),
			),	
			
			array(
				'id'        => 'image_slideshow_animation',
				'type'      => 'switch',
				'title'     => __( 'Ken Burns', 'arnica' ),
				'desc'     	=> __( 'Enable/disable Ken Burns effect.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
			
			array(
				'id'       => 'image_slideshow_section_end',
				'type'     => 'section',
				'indent'   => false,
			),
			
			//Youtube background
			array(
				'id'       => 'youtube_bg_section',
				'type'     => 'section',
				'indent'   => true,
				'required' => array( 'bg_type', "=", 3 ),
			),
			
			array(
				'id'       => 'youtube_video_id',
				'type'     => 'text',
				'title'    => __( 'Youtube Video ID', 'arnica' ),
				'desc'    => __( 'Example: kn-1D5z3-Cs<br />https://www.youtube.com/watch?v=<b>kn-1D5z3-Cs</b>', 'arnica' ),
				'default'  => '',
			),
			  
			array(
				'id'        => 'video_placeholder',
				'type'      => 'media',
				'title'     => esc_html__( 'Video Callback Image', 'arnica' ),
				'desc'      => esc_html__( 'This image will be shown if browser does not support fullscreen video background', 'arnica' ),
				'mode'      => false,
			),
			
			array(
				'id'       => 'youtube_bg_section_end',
				'type'     => 'section',
				'indent'   => false,
			),
			
		),
	);
?>