<?php
	//Read subscribed email addresses from address.txt
	if ( file_exists( dirname( __FILE__ ) . '/../../template/php/address.txt' ) ) {
		Redux_Functions::initWpFilesystem();
		global $wp_filesystem;
		
		$address_list = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/../../template/php/address.txt' );
	}
	
	$this->sections[] = array(
		'title'  => __( 'Subscribe', 'arnica' ),
		'icon'   => 'el el-envelope',
		  'fields' => array(
			  
			//Visibility
			array(
				'id'        => 'subscribe_visibility',
				'type'      => 'switch',
				'title'     => __( 'Visibility', 'arnica' ),
				'desc'     	=> __( 'Enable/disable subscription form.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
			  
			//Title
			array(
				'id'       => 'subscribe_title',
				'type'     => 'text',
				'title'    => __( 'Title', 'arnica' ),
				'default'  => 'Stay Tuned',
			),
			
			//Background image
			array(
				'id'       => 'subscribe_bg_image',
				'type'     => 'media',
				'subtitle' => __( 'Select the image you want in the background', 'arnica' ),
				'title'    => __( 'Background Image', 'arnica' ),
			),	
			  
			//Info
			array(
				'id'       => 'subscribe_info',
				'type'     => 'editor',
				'title'    => __( 'Info', 'arnica' ),
				'default'  => "We launch our new website soon.<br>Please stay updated and follow!"
			),
			  
			//Main Button label
			array(
				'id'       => 'subscribe_button_label',
				'type'     => 'text',
				'title'    => __( 'Main Button Label', 'arnica' ),
				'validate' => 'html',
				'default'  => '<i class="fas fa-envelope"></i> Subscribe',
			),
			  
			//Form Button label
			array(
				'id'       => 'subscribe_form_button_label',
				'type'     => 'text',
				'title'    => __( 'Form Button Label', 'arnica' ),
				'validate' => 'html',
				'default'  => 'Get Notified',
			),
		  
			//Email field label
			array(
				'id'       => 'subscribe_email_field_label',
				'type'     => 'text',
				'title'    => __( 'Email Field Label', 'arnica' ),
				'default'  => 'Enter your email address',
			),
			
			//Subscribers list
			array(
                'id'         => 'subscribe_addresses',
                'type'       => 'raw',
                'full_width' => false,
                'title'    	 => __('Subscribers List', 'arnica'),
                'content'    => "<textarea readonly style='float: left; width: 25em;'>".$address_list."</textarea>",
            ),
			
			/*********************
			    - Mailschimp -
			*********************/
			array(
				'id'       => 'subscribe_use_mailchimp',
				'type'     => 'switch',
				'title'    => __( 'Use MailChimp?', 'arnica' ),
				'subtitle' => __( 'Set to Yes if you want to use MailChimp to manage subscribers. If set to No the email addresses will be added to a simple text string.', 'arnica' ),

			),
			
			 array(
				'id'       => 'mailchimp_section',
				'type'     => 'section',
				'title'    => __( 'Mailchimp Details', 'arnica' ),
				'indent'   => true,
				'required' => array( 'subscribe_use_mailchimp', "=", 1 ),
			),
			
			//Mailchimp API Key
			array(
				'id'       => 'mailchimp_api_key',
				'type'     => 'text',
				'title'    => __( 'Mailchimp API Key', 'arnica' ),
				'default'  => '',
			),
			
			//Mailchimp List ID
			array(
				'id'       => 'mailchimp_list_id',
				'type'     => 'text',
				'title'    => __( 'Mailchimp List ID', 'arnica' ),
				'default'  => '',
			),

			array(
				'id'       => 'mailchimp_section_end',
				'type'     => 'section',
			),
			
		),
	);
?>