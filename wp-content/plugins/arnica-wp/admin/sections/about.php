<?php
	$this->sections[] = array(
		'title'  => __( 'About', 'arnica' ),
		'icon'   => 'el el-website',
		  'fields' => array(
		  
		  	//Visibility
			array(
				'id'        => 'about_visibility',
				'type'      => 'switch',
				'title'     => __( 'Visibility', 'arnica' ),
				'desc'     => __( 'Enable/disable about section.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
			
			//Info nav
			array(
				'id'       => 'about_info_button_label',
				'type'     => 'text',
				'title'    => __( 'Info Button Label', 'arnica' ),
				'default'  => 'More Info',
			),			
		  
		  	//Title
			array(
				'id'       => 'about_title',
				'type'     => 'text',
				'title'    => __( 'Title', 'arnica' ),
				'default'  => 'About',
			),
			
			//Info
			array(
				'id'       => 'about_content',
				'type'     => 'editor',
				'title'    => __( 'Content', 'arnica' ),
				'default'  => "Curabitur ac <strong>fringilla mauris</strong>, vitae luctus orci. Pellentesque eu placerat nunc. <strong>Vivamus</strong> tellus nec semper. Etiam ex felis, maximus id commodo sit amet, <strong>congue vitae ipsum</strong>. Aliquam at nisl nulla. Fusce quis purus nec lacus laoreet luctus at vitae libero."
			),
			
		),
	);
?>