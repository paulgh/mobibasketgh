<?php
	$this->sections[] = array(
		'title'  => __( 'Contact', 'arnica' ),
		'icon'   => 'el el-icon-phone',
		  'fields' => array(
		  
		  	//Visibility
			array(
				'id'        => 'contact_visibility',
				'type'      => 'switch',
				'title'     => __( 'Section Visibility', 'arnica' ),
				'desc'     	=> __( 'Enable/disable contact info section.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
		  
		  	//Title
			array(
				'id'       	=> 'contact_title',
				'type'     	=> 'text',
				'title'    	=> __( 'Title', 'arnica' ),
				'default'  	=> 'Contact',
			),
			
			//Phone
			array(
				'id'      	=> 'contact_phone',
				'type'     	=> 'section',
				'title'  	=> __('Phone'),
				'indent' 	=> true,
			),
			
			array(
				'id'	   	=>'contact_phone_icon',
				'type' 	   	=> 'select',
				'title'    	=> __('Phone Icon', 'arnica'),
				'options'  	=> ArnicaFontAwesomeIcons(),
				'class'    	=> 'font-awesome-icons',
				'default'  	=> 'fas fa-phone'
			),
			
			array(
				'id'       	=> 'contact_phone_text',
				'type'     	=> 'text',
				'title'    	=> __( 'Phone Number', 'arnica' ),
				'validate' 	=> 'html',
				'default'  	=> "P: (866) 496-3250",
			),
			
			array(
				'id'       	=> 'contact_fax_text',
				'type'     	=> 'text',
				'title'    	=> __( 'Fax Number', 'arnica' ),
				'validate' 	=> 'html',
				'default'  	=> "F: (866) 496-3251",
			),
			
			array(
				'id'     	=> 'contact_phone_end',
				'type'   	=> 'section',
				'indent' 	=> false,
			),
			  
			//Address
			array(
				'id'      	=> 'contact_address',
				'type'     	=> 'section',
				'title'    	=> __('Address'),
				'indent'   	=> true,
			),
			
			array(
				'id'	   	=>'contact_address_icon',
				'type' 	   	=> 'select',
				'title'    	=> __('Address Icon', 'arnica'),
				'options'  	=> ArnicaFontAwesomeIcons(),
				'class'    	=> 'font-awesome-icons',
				'default'  	=> 'fas fa-map'
			),
			
			array(
				'id'       	=> 'contact_address_text',
				'type'     	=> 'text',
				'title'    	=> __( 'Address', 'arnica' ),
				'validate' 	=> 'html',
				'default'  	=> "San Francisco, CA 94123, US",
			),
			
			array(
				'id'     	=> 'contact_address_end',
				'type'   	=> 'section',
				'indent' 	=> false,
			),
			
			//Email address
			array(
				'id'      	=> 'contact_email',
				'type'    	=> 'section',
				'title'    	=> __('Email'),
				'indent'  	=> true,
			),
			
			array(
				'id'	   	=>'contact_email_icon',
				'type' 	   	=> 'select',
				'title'    	=> __('Email Icon', 'arnica'),
				'options'  	=> ArnicaFontAwesomeIcons(),
				'class'    	=> 'font-awesome-icons',
				'default'  	=> 'fas fa-envelope'
			),
			
			array(
				'id'       	=> 'contact_email_text',
				'type'     	=> 'text',
				'title'    	=> __( 'Email Address', 'arnica' ),
				'validate' 	=> 'html',
				'default'  	=> "info@example.com",
			),
			
			array(
				'id'       	=> 'contact_email2_text',
				'type'     	=> 'text',
				'title'    	=> __( 'Email Address 2', 'arnica' ),
				'validate' 	=> 'html',
				'default'  	=> "support@example.com",
			),
			
			array(
				'id'     	=> 'contact_email_end',
				'type'   	=> 'section',
				'indent' 	=> false,
			),
			
		),
	);
?>