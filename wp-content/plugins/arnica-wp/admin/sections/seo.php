<?php
	$this->sections[] = array(
		'title'  => __( 'SEO', 'arnica' ),
		'icon'   => 'el el-link',
		  'fields' => array(
		  
			//Site title
			array(
				'id'       => 'site_title',
				'type'     => 'text',
				'title'    => __( 'Site Title', 'arnica' ),
				'default'  => get_bloginfo('title'),
			),
			
			//Description
			array(
				'id'       => 'site_description',
				'type'     => 'text',
				'title'    => __( 'Site Description', 'arnica' ),
				'default'  => get_bloginfo('description'),
			),
			
			//Keywords
			array(
				'id'=>'site_keywords',
				'type' => 'textarea',
				'title' => __('Site keywords', 'arnica'),
				'validate' => 'no_html',
				'default' => '',
			),
			
			/**************************
			    - Webmaster tools -
			**************************/
			array(
				'id'       => 'webmaster_tools',
				'type'     => 'switch',
				'title'    => __( 'Webmaster Tools', 'arnica' ),
				'subtitle' => __( 'You can use the boxes below to verify with the different Webmaster Tools, if your site is already verified, you can just forget about these. Enter the verify meta values for', 'arnica' ),
				'indent' => true 
			),
		   
		    //Google Webmaster Tools
			array(
				'id'       => 'google_verify',
				'type'     => 'text',
				'title'    => __( '<a href="https://www.google.com/webmasters/verification/verification?hl=en&siteUrl='.get_bloginfo('url').'">Google Webmaster Tools</a>', 'arnica' ),
				'default'  => '',
				'required' => array( 'webmaster_tools', "=", 1 ),
			), 
			
			//Bing Webmaster Tools
			array(
				'id'       => 'ms_verify',
				'type'     => 'text',
				'title'    => __( '<a href="http://www.bing.com/webmaster/?rfp=1#/Dashboard/?url='.get_bloginfo('url').'">Bing Webmaster Tools</a>', 'arnica' ),
				'default'  => '',
				'required' => array( 'webmaster_tools', "=", 1 ),
			),
			
			//Pinterest
			array(
				'id'       => 'pin_verify',
				'type'     => 'text',
				'title'    => __( '<a href="https://help.pinterest.com/entries/22488487-Verify-with-HTML-meta-tags">Pinterest</a>', 'arnica' ),
				'default'  => '',
				'required' => array( 'webmaster_tools', "=", 1 ),
			),
			
			//Yandex Webmaster Tools
			array(
				'id'       => 'yandex_verify',
				'type'     => 'text',
				'title'    => __( '<a href="http://help.yandex.com/webmaster/service/rights.xml#how-to">Yandex Webmaster Tools</a>', 'arnica' ),
				'default'  => '',
				'required' => array( 'webmaster_tools', "=", 1 ),
			),
			
			array(
				'id'       => 'webmaster_tools_section_end',
				'type'     => 'section',
				'indent' => false,
			),
			
			/***********************
			    - Social tools -
			***********************/
			array(
				'id'       => 'social_tools',
				'type'     => 'switch',
				'title'    => __( 'Social Tools', 'arnica' ),
				'subtitle' => __( 'It is a way for website owners to send structured data to search engine robots. helping them to understand your content and create well-presented search results.', 'arnica' ),
			),
			
			//Google Author profile
			array(
				'id'       => 'site_author',
				'type'     => 'text',
				'title'    => __( 'Google Author profile', 'arnica' ),
				'desc'     => __( "If you have a Google+ profile , add that URL here and link it on your Google+ profile's about page.", 'arnica' ),
				'default'  => '',
				'validate' => 'url',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Google Publisher Page
			array(
				'id'       => 'site_publisher',
				'type'     => 'text',
				'title'    => __( 'Google Publisher Page', 'arnica' ),
				'desc'     => __( "If you have a Google+ page for your business, add that URL here and link it on your Google+ page's about page.", 'arnica' ),
				'default'  => '',
				'validate' => 'url',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook Page URL
			array(
				'id'       => 'fb_url',
				'type'     => 'text',
				'title'    => __( 'Facebook Page URL', 'arnica' ),
				'default'  => '',
				'validate' => 'url',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook Page Title
			array(
				'id'       => 'fb_title',
				'type'     => 'text',
				'title'    => __( 'Title', 'arnica' ),
				'desc'     => __( 'This is the title used in the Open Graph meta tags on the front page of your site.', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook Description
			array(
				'id'       => 'fb_description',
				'type'     => 'text',
				'title'    => __( 'Description', 'arnica' ),
				'desc'     => __( 'This is the  description used in the Open Graph meta tags on the front page of your site.', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook Image
			 array(
				'id'       => 'fb_image',
				'type'     => 'media',
				'title'    => __( 'Image', 'arnica' ),
				'desc'     => __( 'This is the image used in the Open Graph meta tags on the front page of your site.', 'arnica' ),
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook page ID
			array(
				'id'       => 'fb_page',
				'type'     => 'text',
				'title'    => __( 'Facebook page ID', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Facebook APP ID
			array(
				'id'       => 'fb_app',
				'type'     => 'text',
				'title'    => __( 'Facebook APP ID', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Twitter Username
			array(
				'id'       => 'tw_name',
				'type'     => 'text',
				'title'    => __( 'Twitter Username', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Twitter Title
			array(
				'id'       => 'tw_title',
				'type'     => 'text',
				'title'    => __( 'Title', 'arnica' ),
				'desc'     => __( 'Add title to Twitter card meta data.', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Twitter Description
			array(
				'id'       => 'tw_description',
				'type'     => 'text',
				'title'    => __( 'Description', 'arnica' ),
				'desc'     => __( 'Add description to Twitter card meta data.', 'arnica' ),
				'default'  => '',
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			//Twitter Image
			 array(
				'id'       => 'tw_image',
				'type'     => 'media',
				'title'    => __( 'Image', 'arnica' ),
				'desc'     => __( 'Add image to Twitter card meta data.', 'arnica' ),
				'required' => array( 'social_tools', "=", 1 ),
			),
			
			array(
				'id'       => 'social_tools_section_end',
				'type'     => 'section',
			),
			
		),
	);
?>