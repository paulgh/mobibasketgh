<?php
	$this->sections[] = array(
		'title'  => __( 'Google Maps', 'arnica' ),
		'icon'   => 'el el-icon-map-marker',
		  'fields' => array(
		  
		  	//Visibility
			array(
				'id'        => 'map_visibility',
				'type'      => 'switch',
				'title'     => __( 'Section Visibility', 'arnica' ),
				'desc'     	=> __( 'Enable/disable map section.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
		  
		  	//Latitude
		  	array(
				'id'        => 'map_latitude',
				'type'      => 'text',
				'title'     => __( 'Latitude of a Point', 'arnica' ),
				'desc'      => __( 'Example, 37.800976', 'arnica' ),
				'default'   => '37.800976'
			),
			
			//Longitude
			array(
				'id'        => 'map_longitude',
				'type'      => 'text',
				'title'     => __( 'Longitude of a Point', 'arnica' ),
				'desc'      => __( 'Example, -122.428502', 'arnica' ),
				'default'   => '-122.428502'
			),
			
			//Zoom level
			array(
				'id'            => 'map_zoom_level',
				'type'          => 'slider',
				'title'         => __( 'Zoom Level', 'arnica' ),
				'desc'          => __( 'Zoom level between 0 to 21', 'arnica' ),
				'default'       => 12,
				'min'           => 0,
				'step'          => 1,
				'max'           => 21,
				'display_value' => 'text'
			),
			
			//API key
			array(
				'id'        => 'map_google_api',
				'type'      => 'text',
				'title'     => __( 'Google Maps JavaScript API Key', 'arnica' ),
				'default'   => ''
			),
			
			//Map color
			array(
				'id'           => 'map_color',
				'type'         => 'color',
				'transparent'  => false,
				'title'        => __( 'Map Color', 'arnica' ),
				'desc'         => __( 'Pick a color', 'arnica' ),
				'default'      => '#0fe0ba'
			),
			
			//Marker image
			array(
				'id'        => 'map_marker',
				'type'      => 'media',
				'title'     => __( 'Marker Image', 'arnica' ),
				'mode'      => false,
			),
			
			//Marker popup title
			array(
				'id'     	=> 'map_marker_popup_title',
				'type'      => 'text',
				'title'     => __( 'Marker Popup Title', 'arnica' ),
				'default'   => __( 'Arnica Main Office', 'arnica' )
			),
			
		),
	);
?>