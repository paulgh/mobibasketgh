<?php
	$this->sections[] = array(
		'title'  => __( 'Countdown', 'arnica' ),
		'icon'   => 'el el-calendar',
		  'fields' => array(
			  
			//Visibility
			array(
				'id'        => 'countdown_visibility',
				'type'      => 'switch',
				'title'     => __( 'Visibility', 'arnica' ),
				'desc'     	=> __( 'Enable/disable countdown timer.', 'arnica' ),
				'on'        => __( 'Enable', 'arnica' ),
				'off'       => __( 'Disable', 'arnica' ),
				'default'   => true
			),
		  
			//Countdown date
			array(
				'id'       		=> 'countdown_date',
				'type'     		=> 'date',
				'title'    		=> __( 'Countdown Date', 'arnica' ),
				'desc'     		=> __( 'mm/dd/yyyy', 'arnica' ),
				'placeholder' 	=> '25/12/2020'
			),
			
			array(
				'id'       => 'countdown_time',
				'type'     => 'text',
				'title'    => __( 'Countdown Time', 'arnica' ),
				'desc'     => __( 'hh/mm/ss', 'arnica' ),
				'default'  => '00:00:00'
			),
			
			//Countdown labels
			array(
				'id'		=> 'countdown_labels',
				'type' 		=> 'text',
				'title' 	=> __('Labels', 'arnica'),			
				'default' 	=> "'Years', 'Months', 'Weeks', 'Days', 'Hours', 'Minutes', 'Seconds'",
			),
			  
			//Countdown labels1
			array(
				'id'		=> 'countdown_labels_one',
				'type' 		=> 'text',
				'title' 	=> __('Labels One', 'arnica'),			
				'default' 	=> "'Year', 'Month', 'Week', 'Day', 'Hour', 'Minute', 'Second'",
			),
			
		),
	);
?>