<?php 
	global $arnicaConfig;
	global $arnica_template_url;
	$arnica_template_url = plugins_url()."/arnica-wp/template";
?>

<!DOCTYPE html>
<html class="no-js" lang="en-US">
	
	<head>
	
		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
		<!-- Title -->
		<title><?php echo $arnicaConfig['site_title']; ?></title>
		
		<?php if (!empty($arnicaConfig['site_description'])) {?><meta name="description" content="<?php echo $arnicaConfig['site_description']; ?>"><?php } ?>		
		<?php if (!empty($arnicaConfig['site_keywords'])) {?><meta name="keywords" content="<?php echo $arnicaConfig['site_keywords']; ?>"><?php } ?>
		
		<?php if ($arnicaConfig['webmaster_tools']) { ?>
			<?php if (!empty($arnicaConfig['google_verify'])) {?><meta name="google-site-verification" content="<?php echo $arnicaConfig['google_verify']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['ms_verify'])) {?><meta name="msvalidate.01" content="<?php echo $arnicaConfig['ms_verify']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['pin_verify'])) {?><meta name="p:domain_verify" content="<?php echo $arnicaConfig['pin_verify']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['yandex_verify'])) {?><meta name="yandex-verification" content="<?php echo $arnicaConfig['yandex_verify']; ?>"><?php } ?>
		<?php } ?>
		
		<?php if ($arnicaConfig['social_tools']) { ?>
			<?php if (!empty($arnicaConfig['site_publisher'])) {?><link rel="publisher" href="<?php echo $arnicaConfig['site_publisher']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['site_author'])) {?><link rel="author" href="<?php echo $arnicaConfig['site_author']; ?>"><?php } ?>
			
			<meta property="og:type" content="website">
			<meta property="og:url" content="<?php echo bloginfo('url');?>">
			<meta property="og:locale" content="<?php echo bloginfo('language');?>">
			<?php if (!empty($arnicaConfig['fb_url'])) {?><meta property="article:publisher" content="<?php echo $arnicaConfig['fb_url']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_title'])) {?><meta property="og:title" content="<?php echo $arnicaConfig['fb_title']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_description'])) {?><meta property="og:description" content="<?php echo $arnicaConfig['fb_description']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_image']['url'])) {?><meta property="og:image" content="<?php echo $arnicaConfig['fb_image']['url']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['site_title'])) {?><meta property="og:site_name" content="<?php echo $arnicaConfig['site_title']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_url'])) {?><meta property="fb:admins" content="<?php echo $arnicaConfig['fb_url']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_app'])) {?><meta property="fb:app_id" content="<?php echo $arnicaConfig['fb_app']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['fb_page'])) {?><meta property="fb:page_id" content="<?php echo $arnicaConfig['fb_page']; ?>"><?php } ?>
			
			<meta name="twitter:card" content="summary">
			<meta name="twitter:url" content="<?php echo bloginfo('url');?>">
			<?php if (!empty($arnicaConfig['tw_title'])) {?><meta name="twitter:title" content="<?php echo $arnicaConfig['tw_title']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['tw_description'])) {?><meta name="twitter:description" content="<?php echo $arnicaConfig['tw_description']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['tw_name'])) {?><meta name="twitter:site" content="@<?php echo $arnicaConfig['tw_name']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['site_title'])) {?><meta name="twitter:domain" content="<?php echo $arnicaConfig['site_title']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['tw_name'])) {?><meta name="twitter:creator" content="@<?php echo $arnicaConfig['tw_name']; ?>"><?php } ?>
			<?php if (!empty($arnicaConfig['tw_image']['url'])) {?><meta name="twitter:image:src" content="<?php echo $arnicaConfig['tw_image']['url']; ?>"><?php } ?>
		<?php } ?>
		
		<?php if (!empty($arnicaConfig['favicon']['url'])) { ?>
			<!-- Favicon -->
			<link rel="shortcut icon" href="<?php echo $arnicaConfig['favicon']['url']; ?>">
		<?php } ?>
		
    	<!-- Bootstrap -->
    	<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/bootstrap/css/bootstrap.min.css">
		
		<!-- Font awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/fontawesome/css/fontawesome-all.min.css" />
		
		<!-- Google web fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400italic,600,700,700italic,800">
		
		<!-- Stylesheet -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/media.css">
		
		<!-- Color schema -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/colors/<?php echo $arnicaConfig['color']; ?>.css" class="colors">
		
		<!-- Animate -->
    	<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/animate/animate.css">
	
		<!-- Animated headline -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/animatedheadline/animated.headline.css">
		
		<!-- Custom scrollbar -->
    	<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/mcustomscrollbar/jquery.mCustomScrollbar.css">
	
		<!-- Vegas -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/vegas/vegas.min.css">
		
		<!-- YTPlayer -->
		<link rel="stylesheet" type="text/css" href="<?php echo $arnica_template_url; ?>/layout/plugins/ytplayer/jquery.mb.ytplayer.min.css">
		
		<!-- Modernizr -->
    	<script src="<?php echo $arnica_template_url; ?>/layout/plugins/modernizr/modernizr.custom.js"></script>
    	
    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    	<!--[if lt IE 9]>
      		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/html5shiv/html5shiv.js"></script>
      		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/respond/respond.min.js"></script>
      	<![endif]-->
		
		<script type="text/javascript">
			var launchDay, labels;
			
			<?php $launchDay = !empty($arnicaConfig['countdown_date']) ? date("M d, Y H:i:s", strtotime($arnicaConfig['countdown_date']." ".$arnicaConfig['countdown_time'])) : "December 25, 2020 00:00:00";?>
			
			launchDay = "<?php echo $launchDay; ?>";
			labels = [<?php echo $arnicaConfig['countdown_labels']; ?>];
			labelsOne = [<?php echo $arnicaConfig['countdown_labels_one']; ?>];
		</script>
	
	</head>
	
	<body>
	
		<!-- Loader -->
		<div class="page-loader">
			<div class="progress"></div>
		</div>
		
		<!-- Background -->
		<?php
			switch ((int)$arnicaConfig['bg_type']) {
				
				//Single image
				case 1:	
				?>		
					<div class="bg modal-effect image"></div>		
				<?php
					break;		
				
				//Image slideshow
				case 2:
					$kenburns = '';
					if ($arnicaConfig['image_slideshow_animation']) {
						$kenburns = 'kenburns';
					}
				?>		
					<div class="bg modal-effect slideshow" data-animation="<?php echo $kenburns; ?>"></div>
				<?php
					break;		
					
				//Youtube background
				case 3:
				?>		
					<a id="bgndVideo" class="player" 
						data-property="{
							videoURL:'https://www.youtube.com/watch?v=<?php echo $arnicaConfig['youtube_video_id']; ?>',
							mobileFallbackImage:'<?php echo $arnicaConfig['video_placeholder']['url']; ?>',
							containment:'body',
							autoPlay:true,
							showControls:false,
							mute:true,
							startAt:0,
							stopAt:0,
							opacity:1
						}">
					</a>		
				<?php
					break;
					
			}
		?>
		
		<!-- Page content -->
		<div class="page modal-effect">
			
			<?php if ($arnicaConfig['logo']['url']!="") { ?>
				<!-- Logo -->
				<section class="logo wow fadeInUp">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">

								<img src="<?php echo $arnicaConfig['logo']['url']; ?>" alt="">

							</div>
						</div>
					</div>
				</section>
			<?php } ?>
			
			<!-- Table -->
			<div class="tbl">
				<div class="tbl-cell">
					
					<!-- Welcome -->
					<section class="welcome">
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-12">
									
									<?php if ($arnicaConfig['title']!="" || $arnicaConfig['words_wrapper']!="") { ?>
										<h2 class="wow fadeInUp cd-headline clip" data-wow-delay="0.3s">

											<?php if ($arnicaConfig['title']!="") { ?>
												<span class="blc">
													<?php echo $arnicaConfig['title']; ?>
												</span>
											<?php } ?>

											<?php 
												if ($arnicaConfig['words_wrapper']!="") {
													$arrWords = explode('|', $arnicaConfig['words_wrapper']);
											?>
												<span class="cd-words-wrapper">
													<?php for ($i=0; $i<count($arrWords); $i++) { ?>
														<b <?php echo ($i==0 ? 'class="is-visible"' : ''); ?>>
															<?php echo $arrWords[$i]; ?>
														</b>
													<?php } ?>
												</span>
											<?php } ?>

										</h2>
									<?php } ?>

									<?php if ($arnicaConfig['slogan']!="") { ?>
										<p class="wow fadeInUp" data-wow-delay="0.6s">
											<?php echo $arnicaConfig['slogan']; ?>
										</p>
									<?php } ?>

								</div>
							</div>
						</div>
					</section>
					
					<?php if ($arnicaConfig['countdown_visibility']) { ?>
						<!-- Countdown -->
						<section class="countdown">
							<div class="container-fluid">
								<div class="row">
									<div class="col-lg-12">
										<div id="countdown" class="wow fadeInUp" data-wow-delay="0.9s"></div>
									</div>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php if ($arnicaConfig['subscribe_visibility'] || $arnicaConfig['about_visibility']) { ?>
						<!-- Buttons -->
						<section class="buttons">
							<div class="container-fluid">
								<div class="row">
									<div class="col-lg-12">

										<?php if ($arnicaConfig['subscribe_visibility']) { ?>
											<!-- Subscribe -->
											<div class="action-btn white wow fadeInUp" data-wow-delay="1.2s" data-dialog="newsletter">
												<?php echo $arnicaConfig['subscribe_button_label']; ?>
											</div>
										<?php } ?>
										
										<?php if ($arnicaConfig['about_visibility']) { ?>
											<div class="action-btn more-info wow fadeInUp" data-wow-delay="1.5s" data-toggle="modal" data-target="#about">
												<?php echo $arnicaConfig['about_info_button_label']; ?>
											</div>
										<?php } ?>

									</div>
								</div>
							</div>
						</section>
					<?php } ?>
					
				</div>
			</div>
			
			<?php 
				if (!empty($arnicaConfig['social_link']) && count($arnicaConfig['social_link']) >= 1) { 
					$arrSocial = array_values($arnicaConfig['social_link']);
			?>
				<!-- Social -->
				<section class="social">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">

								<ul>
									
									<?php 
										for ($i=0, $cnt = count($arrSocial); $i < $cnt; $i++) {
											$item = $arrSocial[$i];
									?>	
										<li>
											<a href="<?php echo $item['url']; ?>" title="<?php echo htmlspecialchars($item['title']); ?>">
												<i class="<?php echo $item['select']; ?>"></i>
											</a>
										</li>
									<?php } ?>
								</ul>

							</div>
						</div>
					</div>
				</section>
			<?php } ?>
			
		</div>
		
		<!-- Info nav -->
		<a class="info-nav wow fadeInUp" data-wow-delay="0.3s" href="#" data-toggle="modal" data-target="#about">
			<span></span>
		</a>

		<!-- About us -->
		<div id="about" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">

			<div class="tbl">
				<div class="tbl-cell">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="tbl-top">

								<div class="modal-body">

									<?php if ($arnicaConfig['about_visibility']) { ?>
										<!-- About -->
										<div class="about-dsc">

											<?php if ($arnicaConfig['about_title']!="") { ?>
												<!-- Title -->
												<div class="row section-title">
													<div class="col-lg-12">
														<h3><?php echo $arnicaConfig['about_title']; ?></h3>
														<hr>
													</div>
												</div>
											<?php } ?>

											<?php if ($arnicaConfig['about_content']!="") { ?>
												<div class="row">
													<div class="col-lg-12">
														<p>
															<?php echo $arnicaConfig['about_content']; ?>
														</p>
													</div>
												</div>
											<?php } ?>

										</div>
									<?php } ?>

									<?php if ($arnicaConfig['contact_visibility']) { ?>
										<!-- Contact -->
										<div class="contact">

											<?php if ($arnicaConfig['contact_title']!="") { ?>
												<!-- Title -->
												<div class="row section-title">
													<div class="col-lg-12">
														<h3><?php echo $arnicaConfig['contact_title']; ?></h3>
														<hr>
													</div>
												</div>
											<?php } ?>

											<div class="row">		
												
												<div class="col-lg-4">
													<h5><i class="<?php echo $arnicaConfig['contact_phone_icon']; ?>"></i></h5>
													<p>
														<?php 
															//Phone
															if ($arnicaConfig['contact_phone_text']!="") {
																echo $arnicaConfig['contact_phone_text'];
																
																if ($arnicaConfig['contact_fax_text']!="") {
																	echo "<br />";	
																}
														 	}
															
															//Fax
															if ($arnicaConfig['contact_fax_text']!="") {
																echo $arnicaConfig['contact_fax_text'];
															}
														 ?>
													</p>	
												</div>
												
												<div class="col-lg-4">
													<h5><i class="<?php echo $arnicaConfig['contact_address_icon']; ?>"></i></h5>
													<p><?php echo $arnicaConfig['contact_address_text']; ?></p>
												</div>
												
												<div class="col-lg-4">
													<h5><i class="fas fa-envelope"></i></h5>
													<p>
														
														<?php if ($arnicaConfig['contact_email_text']!="") {?>
															<a href="mailto:<?php echo $arnicaConfig['contact_email_text']; ?>">
																<?php echo $arnicaConfig['contact_email_text']; ?>
															</a>
														<?php }?>
														
														<?php if ($arnicaConfig['contact_email2_text']!="") {?>
															<a href="mailto:<?php echo $arnicaConfig['contact_email2_text']; ?>">
																<?php echo $arnicaConfig['contact_email2_text']; ?>
															</a>
														<?php }?>
														
													</p>
												</div>
												
											</div>
										</div>
									<?php } ?>

									<?php 
										if ($arnicaConfig['map_visibility']) {
											$marker = $arnicaConfig['map_marker']['url'];

											if ($marker=="") {
												$marker = $arnica_template_url."/layout/images/map-marker-".$arnicaConfig['color'].".png";	
											}
									?>
										<!-- Google Maps -->
										<div class="google-map">
											<div id="google-container"
												 data-title="<?php echo $arnicaConfig['map_marker_popup_title']; ?>"
												 data-latitude="<?php echo $arnicaConfig['map_latitude']; ?>"
												 data-longitude="<?php echo $arnicaConfig['map_longitude']; ?>"
												 data-zoom="<?php echo $arnicaConfig['map_zoom_level']; ?>"
												 data-marker="<?php echo $marker; ?>"
												 data-color="<?php echo $arnicaConfig['map_color']; ?>">
											</div>
											<div id="zoom-in"></div>
											<div id="zoom-out"></div>
										</div>
									<?php } ?>

									<?php if ($arnicaConfig['copyright_text']!="") { ?>
										<!-- Copyright -->
										<div class="copyright">
											<?php echo $arnicaConfig['copyright_text']; ?>
										</div>
									<?php } ?>

								</div>

							</div>						  

						</div>
					</div>
				</div>
			</div>

		</div>
		
		<!-- Newsletter popup -->
		<div id="newsletter" class="dialog">
			
			<div class="dialog__overlay"></div>
			
			<div class="dialog__content" style="background:url(<?php echo $arnicaConfig['subscribe_bg_image']['url']; ?>) center;">						
				<div class="dialog-inner">
							
					<?php if ($arnicaConfig['subscribe_title']!="") { ?>
						<h4><?php echo $arnicaConfig['subscribe_title']; ?></h4>
					<?php } ?>
						
					<?php if ($arnicaConfig['subscribe_info']!="") { ?>
						<p class="hidden-xs"><?php echo $arnicaConfig['subscribe_info']; ?></p>
					<?php } ?>
					
					<!-- Newsletter form -->
					<div id="subscribe">

		                <form action="<?php echo plugins_url()."/arnica-wp/template/php/subscribe.php"; ?>" id="notifyMe" method="POST">
		                    <div class="form-group">
		                        <div class="controls">
		                            
		                        	<!-- Field  -->
		                        	<input type="text" id="mail-sub" name="email" placeholder="<?php echo $arnicaConfig['subscribe_email_field_label']; ?>" class="email srequiredField" />

		                        	<!-- Spinner top left during the submission -->
		                        	<i class="fas fa-spinner opacity-0"></i>

		                            <!-- Button -->
		                            <button class="action-btn submit"><?php echo $arnicaConfig['subscribe_form_button_label']; ?></button>

		                            <div class="clear"></div>

		                        </div>
		                    </div>
		                </form>

						<!-- Answer for the newsletter form is displayed in the next div, do not remove it. -->
						<div class="block-message">
							<div class="message">

								<p class="notify-valid"></p>

							</div>

						</div>
						
					</div>
				</div>

				<!-- Popup close button -->
				<button class="close-newsletter" data-dialog-close><i class="fas fa-times"></i></button>

			</div>
						
		</div>
		
		<!-- jQuery -->
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/jquery/jquery.js"></script>

		<!-- Bootstrap -->
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!-- Plugins -->
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/wow/wow.min.js"></script>	
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/plugin/jquery.plugin.min.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/animatedheadline/animated.headline.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/countdown/jquery.countdown.min.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/notifyme/notifyme.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/classie/classie.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/dialogfx/dialogfx.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/mcustomscrollbar/jquery.mCustomScrollbar.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/vegas/vegas.min.js"></script>
		<script src="<?php echo $arnica_template_url; ?>/layout/plugins/ytplayer/jquery.mb.ytplayer.min.js"></script>

		<?php if ($arnicaConfig['map_visibility']) { ?>
			<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $arnicaConfig['map_google_api']; ?>"></script>
		<?php } ?>

		<!-- Main -->
		<script src="<?php echo $arnica_template_url; ?>/layout/js/main.js"></script>

		<script type="text/javascript">
			jQuery(document).ready(function() {

				<?php
					switch ((int)$arnicaConfig['bg_type']) {

						case 1:	//Single image
							$img = $arnicaConfig['single_image']['url'];
						?>						
							jQuery('.bg.image').vegas({
								slides:[
									{src:'<?php echo $img; ?>'},
								],
								transition:'fade'
							});
						<?php
							break;	

						case 2:	//Image slideshow
							$img = "";						
							$gallery_id = explode(',', $arnicaConfig['image_slideshow']);

							foreach($gallery_id as $photo_id) {
								$img .= "{src:'". wp_get_attachment_url($photo_id) . "'},";
							}
						?>
							jQuery('.bg.slideshow').vegas({
								slides:[
									<?php echo $img; ?>
								],
								transition:'fade',
								animation:jQuery('.bg.slideshow').data("animation")
							});
						<?php break; ?>
				<?php } ?>

				//Fix Vegas responsive issue
				jQuery('.bg').removeAttr('style');

			});
		</script>
	
	</body>
	
</html>