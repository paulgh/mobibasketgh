<?php
	if(!$_POST) exit;
	
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once( $parse_uri[0] . 'wp-load.php' );
	
	global $arnicaConfig;

	//Form Fields
	$name = htmlspecialchars(trim($_POST["name"]));
	$email = htmlspecialchars(trim($_POST["email"]));
	$message = htmlspecialchars(trim($_POST["message"]));
	
	//Error variable
	$error = "";
	
	//Check name
	if(!$name) {
		$error .= "Please enter your name.<br />";
	}

	//Check if email is valid
	if(empty($email)) {
		$error .= "Please enter your e-mail.<br />";
	}
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$error .= "Please enter a valid email address.<br />";
	}
	
	//Check message
	if(get_magic_quotes_gpc()) {
		$message = stripslashes($message);
	}
	
	if(!$message) {
		$error .= "Please enter the message.<br />";
	}
	
	//Result
	if(!$error) {
		$variables = array(	'{from}', 	'{email}', '{message}', 	'{date}', 	'{ip}');
		$values = array(	$name, 		$email, 	$message, 		$date, 		$ip);
		
		$text = trim( str_replace($variables, $values, $arnicaConfig['contact_form_template']));
		$headers = array('From: '.$name.' <'.$email.'>');

		if (!empty($arnicaConfig['contact_form_email'])) {
			if (wp_mail($arnicaConfig['contact_form_email'], __( 'A new message from', 'arnica' ).' '.$name, $text, $headers)) {
				echo "ok";
			} else {
				$error .= __( 'Message sending error!', 'arnica' );
			}
		}
	} else {
		echo '<div class="notification_error">'.$error.'</div>';
	}
?>