<?php
	/*
	Plugin Name: Arnica - Creative Coming Soon WordPress Plugin
	Plugin URI: https://codecanyon.net/item/arnica-creative-coming-soon-wordpress-plugin/23093323
	Description: Creative and professional under construction / landing page / one page WordPress Plugin.
	Author: AthenaStudio
	Version: 1.0.2
	Author URI: https://themeforest.net/user/athenastudio
	*/
	
	add_action( 'plugins_loaded', 'arnica_textdomain' );
	function arnica_textdomain() {
		load_plugin_textdomain( 'arnica', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
	}
	
	if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/admin/redux-framework/ReduxCore/framework.php' ) ) {
		require_once( dirname( __FILE__ ) . '/admin/redux-framework/ReduxCore/framework.php' );
		require_once( plugin_dir_path(__FILE__) . '/admin/social/social.php' );
	}
	
	if ( !isset( $arnicaConfig ) && file_exists( dirname( __FILE__ ) . '/admin/config.php' ) ) {
		require_once( dirname( __FILE__ ) . '/admin/config.php' );
	}
	
	function arnica_coming_soon_wp() {
		global $arnicaConfig, $arnicaPluginConfig;
		
		if(!$arnicaConfig['plugin_mode']) {
			return;
		} else if($arnicaConfig['plugin_mode']) {
			if ( current_user_can( 'manage_options' ) || $arnicaPluginConfig->allowAccess() ) {
				return;
			} else {
				include( dirname( __FILE__ ) . '/template/functions.php');
				include( dirname( __FILE__ ) . '/template/index.php');
				die();
			}
		}
	}	
	add_action( 'template_redirect', 'arnica_coming_soon_wp' );
?>